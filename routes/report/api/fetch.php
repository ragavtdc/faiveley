<?php
$rId = 10;
require '../../includes/init.php';
require_once 'functions.php';
check('serial', 'Serial no required');

$db = db();

extract($_POST);


if (strlen($serial) < 5) {
    err('Insufficient Serial no');
}

$result = mysqli_query($db, "SELECT a.*,b.part_name FROM a_part_accessories_staging a INNER JOIN a_part_master b ON a.type = b.part_id WHERE (a.serial_no like '%$serial%' or a.serial_alpha like '%$serial%') AND a.parent = 0 LIMIT 1");

if (mysqli_num_rows($result) != 1) {
    mysqli_close($db);
    err('Part not found!');
}

$row = mysqli_fetch_assoc($result);

$row['base64'] = getBarcodeBase64($row['serial_no']);

$data = array();

$data['part'] = $row;

$data['access'] = getAccInfo($db, $row['id']);

$data['stages'] = getStageInfo($db, $row['id']);

mysqli_close($db);

complete($data);
