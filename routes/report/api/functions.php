<?php

require_once __DIR__ . '/../barcode/barcode.php';

function getAccInfo($db, $id)
{
    $res = array();
    $result = mysqli_query($db, "SELECT a.*,b.acc_name FROM `a_part_accessories_staging` a INNER JOIN `a_accessories_master` b ON a.type = b.acc_id WHERE a.parent ='$id' ORDER BY a.testing DESC,a.type ASC");
    while ($row = mysqli_fetch_assoc($result)) {
        $row['base64'] = getBarcodeBase64($row['serial_no']);
        $res[] = $row;
    }
    return $res;
}

function getStageInfo($db, $id)
{
    $res = array();
    $result = mysqli_query($db, "SELECT a.*,b.main_stage_description FROM `a_stage_updation` a INNER JOIN `main_stage_master` b ON a.stage = b.id WHERE a.part_id = '$id'");
    while ($row = mysqli_fetch_assoc($result)) {
        $res[] = $row;
    }
    return $res;
}
