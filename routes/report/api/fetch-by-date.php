<?php
$rId = 10;
require '../../includes/init.php';
require 'functions.php';

check('d1', 'From date required');
check('d2', 'To date required');

$db = db();

extract($_POST);

$result = mysqli_query($db, "SELECT a.*,b.part_name FROM a_part_accessories_staging a INNER JOIN a_part_master b ON a.type = b.part_id WHERE (DATE(a.created_at) BETWEEN '$d1' AND '$d2' ) AND a.parent = 0");

$datas = array();

while ($row = mysqli_fetch_assoc($result)) {

    $data = array();

    $row['base64'] = getBarcodeBase64($row['serial_no']);

    $data['part'] = $row;

    $data['access'] = getAccInfo($db, $row['id']);

    $data['stages'] = getStageInfo($db, $row['id']);

    $datas[] = $data;
}

mysqli_close($db);

complete($datas);
