<?php
$rId = 10;
require '../includes/init.php';
head();
?>

<div class="row">
    <div class="col-sm-12 col-md-6">
        <form id="partScan">
            <div class=" m-2 row tile p-3">
                <div class='form-group col-sm-12'>
                    <input name="serial" class='form-control' type='text' value="" placeholder='Enter serial no.' id="serialNumber">
                </div>
                <div class='offset-sm-4 col-sm-4'>
                    <input type="submit" class='btn btn-info d-block btn-primary text-white' value="GET REPORT">
                    <!-- <a id='getPartDetailsBtnM' onclick="fetch($('#serialNumber').val())" class='btn btn-info d-block btn-primary text-white'>GET REPORT</a> -->
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-12 col-md-6">
        <form id="dateScan">
            <div class="m-2 row tile p-3">
                <div class='form-group col-sm-12 col-md-6 '>
                    <input class='form-control' value="<?php echo date('Y-m-01'); ?>" type='date' name="d1" placeholder='Enter serial no.'>
                </div>
                <div class='form-group col-sm-12 col-md-6'>
                    <input class='form-control' value="<?php echo date('Y-m-d'); ?>" type='date' name="d2" placeholder='Enter serial no.'>
                </div>
                <div class='offset-sm-4 col-sm-4'>
                    <input type="submit" class='btn btn-info d-block btn-primary text-white' value="GET REPORT">
                </div>
            </div>
        </form>
    </div>

    <div class="col-sm-12 col-md-3"></div>

    <div class="col-sm-12 col-md-6 mt-2">
        <table class="table tile table-sm table-bordered text-center">
            <thead>
                <tr>
                    <th>Total Parts</th>
                    <th>Success Parts</th>
                    <th>Failure Parts</th>
                </tr>
            </thead>
            <tbody id="summer">

            </tbody>
        </table>
    </div>
</div>


<!--Accordion wrapper-->
<div class="accordion md-accordion mt-3 mx-2" id="accordionEx" aria-multiselectable="true">
    <!-- Accordion card -->

    <!-- Accordion card -->
</div>
<!-- Accordion wrapper -->



<?php foot();
?>
<script src="<?php echo $path ?>js/printThis.js"></script>
<script>
    // $(document).ready(function() {
    //     $('input[name=d2]').val(new Date());
    // });
    const accord = $('#accordionEx');

    function setTitle(part) {
        let html = '';

        html += '<div class="tile-title mb-0 text-center">';
        html += '<img src="<?php echo $path ?>images/logo-wabtec.png" class="print-loo" style="height:60px;display:none">'
        html += '<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne' + part.id + '">';
        html += '<h5 class="mb-0">';
        html += part.serial_no + ' - ' + part.part_name;
        html += '</h5>';
        html += '</a>';
        html += '</div>';

        return html;
    }

    function setPart(part) {
        let html = '';

        html += '<h5>Part info</h5>';

        html += '<table class="table table-bordered table-sm table-responsive-lg table-fixed">';

        html += '<thead>';
        html += '<tr>';
        html += '<td>Name</td>';
        html += '<td>Serial Alpha</td>';
        html += '<td>Serial No</td>';
        html += '<td>Updated Date</td>';
        html += '<td>Status</td>';
        html += '</tr>';
        html += '</thead>';

        html += '<tbody>';
        html += '<tr>';
        html += '<td>' + part.part_name + '</td>';
        // html += '<td>' + part.serial_alpha + '</td>';
        html += '<td class="text-center">' + '<img src="' + part.base64 + '" style="max-width:300px">' + '<p>' + part.serial_alpha + '</p>' + '</td>';
        html += '<td class="text-break">' + part.serial_no + '</td>';
        html += '<td>' + part.updated_at + '</td>';
        html += '<td>' + (part.testing == 0 ? 'Failure' : 'Ok') + '</td>';
        html += '</tr>';
        html += '</tbody>';

        html += '</table>';

        return html;
    }

    function setAccess(acc) {
        let html = '';

        html += '<h5>Accessories info</h5>';

        html += '<table class="table table-bordered table-sm table-responsive-lg table-fixed">';

        html += '<thead>';
        html += '<tr>';
        html += '<td>Name</td>';
        html += '<td>Serial Alpha</td>';
        html += '<td>Serial No</td>';
        html += '<td>Operator</td>';
        html += '<td>Updated Date</td>';
        html += '<td>Status</td>';
        html += '</tr>';
        html += '</thead>';

        html += '<tbody>';

        acc.forEach(a => {
            html += '<tr>';
            html += '<td>' + a.acc_name + '</td>';
            html += '<td class="text-center">' + '<img src="' + a.base64 + '" style="max-width:300px">' + '<p>' + a.serial_alpha + '</p>' + '</td>';
            html += '<td class="text-break" >' + a.serial_no + '</td>';
            html += '<td>' + (a.testing == 1 ? a.inserted_by : a.updated_by) + '</td>';
            html += '<td>' + a.updated_at + '</td>';
            html += '<td>' + (a.testing == 0 ? 'Failure' : 'Ok') + '</td>';
            html += '</tr>';
        });

        html += '</tbody>';

        html += '</table>';

        return html;
    }

    function setStages(stages) {
        let html = '';

        html += '<h5>Stage info</h5>';

        html += '<table class="table table-bordered table-sm" width="100%">';

        html += '<thead>';
        html += '<tr>';
        html += '<td>Stage Name</td>';
        html += '<td>Operator</td>';
        html += '<td>Updated Date</td>';
        html += '<td>Status</td>';
        html += '</tr>';
        html += '</thead>';

        html += '<tbody>';

        stages.forEach(s => {
            html += '<tr>';
            html += '<td>' + s.main_stage_description + '</td>';
            html += '<td>' + s.created_by + '</td>';
            html += '<td>' + s.created_at + '</td>';
            html += '<td>' + (s.status == 0 ? 'Failure' : s.status == 1 ? 'Ok' : '-') + '</td>';
            html += '</tr>';
        });

        html += '</tbody>';

        html += '</table>';

        return html;
    }

    function setPrint(part) {
        let html = '<div class="text-center print-buttons">';
        html += '<a class="btn btn-info text-white mx-2"  target="_blank" href="./print/?s=' + part.id + '">History</a>';
        html += '<a class="btn btn-info text-white mx-2" target="_blank" href="./bom/?s=' + part.id + '">Generate BOM</a>';
        html += '</div>';
        return html;
    }

    function createTile(data) {
        let html = '<div class="tile" id="print' + data.part.id + '">';
        html += setTitle(data.part);
        html += '<div id="collapseOne' + data.part.id + '" class="collapse tile-body pt-3" data-parent="#accordionEx">';
        html += setPart(data.part);
        html += setAccess(data.access);
        html += setStages(data.stages);
        html += setPrint(data.part);
        //** */
        // html += '<table class="table table-bordered table-sm print-table" style="display:none" width="100%"> <tbody><tr><td colspan="2" height="80px">p</td></tr><tr>    <td style="border: 0px;vertical-align: bottom;text-align:center" height="80px">        (QUALITY)    </td>    <td style="border: 0px;vertical-align: bottom;text-align:center">        (PRODUCTION)    </td></tr></tbody></table>';
        /** */
        html += '</div>'
        html += '</div>';
        accord.append(html);
    }

    function calcPart(total = 0, su = 0, fa = 0) {
        let html = '<tr>';
        html += '<td>' + total + '</td>';
        html += '<td>' + su + '</td>';
        html += '<td>' + fa + '</td>';
        $('#summer').html(html);
    }

    function clear() {
        accord.html('');
        calcPart(0, 0, 0);
    }
    $('#partScan').submit((e) => {
        e.preventDefault();
        const func = (data) => {
            console.log(data);
            createTile(data);
            $('#serialNumber').val(data.part.serial_no)
            $('#collapseOne' + data.part.id).addClass('show');
            if (data.part.testing == 1) calcPart(1, 1, 0);
            else calcPart(1, 0, 1);
        }
        clear();
        ajax('./api/fetch.php', $('#partScan').serialize(), func);
    });
    $('#dateScan').submit((e) => {
        e.preventDefault();
        const func = (datas) => {
            console.log(datas);

            if (datas.length == 0) {
                toast('no data found!');
                return;
            }
            let su = 0;
            let fa = 0;
            datas.forEach(data => {
                createTile(data);
                if (data.part.testing == 1) su++;
                else fa++;
            });
            calcPart(datas.length, su, fa);
        }
        clear();
        ajax('./api/fetch-by-date.php', $('#dateScan').serialize(), func);
    });
</script>