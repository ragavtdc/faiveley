<?php

require_once __DIR__ . '/../barcode/barcode.php';

function getAccInfo($db, $id)
{
    $res = array();
    $result = mysqli_query($db, "SELECT a.*,b.acc_name,b.acc_part_no FROM `a_part_accessories_staging` a INNER JOIN `a_accessories_master` b ON a.type = b.acc_id WHERE a.parent ='$id' and a.testing = 1 ORDER BY a.type ASC");
    while ($row = mysqli_fetch_assoc($result)) {
        $row['base64'] = getBarcodeBase64($row['serial_no']);
        $res[] = $row;
    }
    return $res;
}

function getPartInfo($db, $s)
{
    $result = mysqli_query($db, "SELECT a.*,b.part_name,b.part_no FROM a_part_accessories_staging a INNER JOIN a_part_master b ON a.type = b.part_id WHERE a.id = '$s' AND a.parent = 0 LIMIT 1");

    if (mysqli_num_rows($result) != 1) {
        mysqli_close($db);
        err('Part not found!');
    }

    $row = mysqli_fetch_assoc($result);

    $row['base64'] = getBarcodeBase64($row['serial_no']);
    return $row;
}

function getMaccInfo($db, $type)
{
    $result = mysqli_query($db, 'SELECT * FROM a_accessories_mechanical_master WHERE part_id = 1');
    $res = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $res[] = $row;
    }
    return $res;
}
