<?php

$rId = 10;

require_once '../../includes/init.php';

require_once __DIR__ . '/functions.php';

require_once '../../print/vendor/autoload.php';

check('s', 'required serial no');

$db = db();

extract($_GET);

$part = getPartInfo($db, $s);

$access = getAccInfo($db, $part['id']);

$mAccess = getMaccInfo($db, $part['type']);

mysqli_close($db);

// complete($part);

/**pdf start */

$pdfHeader = '
<table  width="100%" >
    <tr>
        <td colspan="3" rowspan="2">
            <img src="../../../images/logo-wabtec.png"  height="50px">
        </td>
        <td>
            PRODUCT
        </td>
        <td>' .
    $part['part_name']
    . '</td>
    </tr>
    <tr>
        <td>
            PART NO
        </td>
        <td>' .
    $part['part_no']
    . '</td>
    </tr>
    <tr>
        <td colspan="2">DATE</td>
        <td>' . viewDate($part['created_at']) . '</td>
        <td colspan="2" rowspan="2">
        <img src="' . $part['base64'] .  '" width="300px" alt="barcode">
        <p>' . $part['serial_alpha'] . '</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">DRAWN</td>
        <td>---</td>
    </tr>
    <tr>
        <td colspan="2">APPROVED</td>
        <td>---</td>
        <td colspan="2"> {PAGENO} / {nbpg} </td>
    </tr>
</table>';

$html = '
<style>
table,tr,th,td{
border: 0.5px solid black;
border-collapse: collapse;
text-align: center;
vertical-align: middle;
}
</style>';

$html .= '
<table width="100%">
    <thead>
        <tr>
            <th width="1.3cm" >#</th>
            <th width="1.3cm">QTY</th>
            <th>DESCRIPTION</th>
            <th>PART NO</th>
            <th>CODE</th>
        </tr>
    </thead>
    <tbody>';


foreach ($access as $key => $a) {
    $html .= '<tr>';
    $html .= '<td>' . ($key + 1) . '</td>';
    $html .= '<td>1</td>';
    $html .= '<td>' . $a['acc_name'] . '</td>';
    $html .= '<td>' . $a['acc_part_no'] . '</td>';
    $html .= '<td style="padding: 4px 10px;">';
    $html .= '<img width="40%" src="' . $a['base64'] . '"/>';
    $html .= '<p>' . $a['serial_alpha'] . '</p>';
    $html .= '</td>';
    $html .= '</tr>';
}

foreach ($mAccess as $key => $a) {
    $html .= '<tr>';
    $html .= '<td>' . ($key + 1 + count($access)) . '</td>';
    $html .= '<td>1</td>';
    $html .= '<td>' . $a['acc_name'] . '</td>';
    $html .= '<td>' . $a['acc_part_no'] . '</td>';
    $html .= '<td></td>';
    $html .= '</tr>';
}


$html .= (' 
<tr>
    <td colspan="5" height="80px"></td>
</tr>
<tr>
    <td colspan="3" style="border: 0px;vertical-align: bottom;" height="80px">
        (QUALITY)
    </td>
    <td colspan="2" style="border: 0px;vertical-align: bottom;">
        (PRODUCTION)
    </td>
</tr>
</tbody>
</table>');
$mpdf = new \Mpdf\Mpdf([
    'pagenumPrefix' => 'SHEET ',
    'setAutoTopMargin' => 'stretch',
    'autoMarginPadding' => 0,
    'showImageErrors' => true
]);
$mpdf->SetHTMLHeader($pdfHeader);
$mpdf->writeHTML($html);
$dir = '../../../backup/bom/' . $part['serial_no'];
if (!file_exists($dir)) {
    mkdir($dir, 0777, true);
}
$mpdf->Output($dir . '/' . $part['serial_no'] . '_' . date("h-iad-m-Y") . '.pdf', 'F');
$mpdf->Output($part['serial_no'] . '.pdf', 'I');
