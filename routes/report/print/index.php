<?php
$rId = 10;
require '../../includes/init.php';
require_once '../api/functions.php';
check('s', 'Serial no required');
require_once '../../print/vendor/autoload.php';

$db = db();

$mpdf = new \Mpdf\Mpdf([
    'pagenumPrefix' => 'SHEET ',
    'setAutoTopMargin' => 'stretch',
    'autoMarginPadding' => 0,
    'showImageErrors' => true
]);

extract($_GET);

$result = mysqli_query($db, "SELECT a.*,b.part_name FROM a_part_accessories_staging a INNER JOIN a_part_master b ON a.type = b.part_id WHERE a.id = '$s' AND a.parent = 0 LIMIT 1");

if (mysqli_num_rows($result) != 1) {
    mysqli_close($db);
    err('Part not found!');
}

$row = mysqli_fetch_assoc($result);

$row['base64'] = getBarcodeBase64($row['serial_no']);

$data = array();

$part = $row;

$access = getAccInfo($db, $row['id']);

$stages = getStageInfo($db, $row['id']);


// complete($part);
// <td colspan="2"> {PAGENO} / {nbpg} </td>

/**pdf start */

$pdfHeader = '
<table width="100%" style="border:none!important" >
<tr style="border:none!important">
<td style="border:none!important" >
<img src="../../../images/logo-wabtec.png"  height="60px">
</td>
</tr>
<tr style="border:none!important">
<td style="border:none!important">' . $part['serial_alpha'] . ' - ' . $part['part_name'] . '</td>

</tr>
</table>';

$foot = '<center> {PAGENO}  </center>'; // {nbpg}
$mpdf->SetHTMLHeader($pdfHeader);
$mpdf->SetHTMLFooter($foot);

$html = '
<style>
table,tr,th,td{
    border: 0.4px solid grey;
    border-collapse: collapse;
    text-align: center;
    vertical-align: middle;
}
</style>';


/** */


$html .= '<h5>Part info</h5>';

$html .= '<table width="100%">';
$html .= '<thead>';
$html .= '<tr>';
$html .= '<th>Name</th>';
$html .= '<th>Serial Alpha</th>';
$html .= '<th>Serial No</th>';
$html .= '<th>Updated Date</th>';
$html .= '<th>Status</th>';
$html .= '</tr>';
$html .= '</thead>';

$html .= '<tbody>';
$html .= '<tr>';
$html .= '<td>' . $part['part_name'] . '</td>';
$html .= '<td style="padding: 10px 5px">' . '<img src="' . $part['base64'] . '" style="width:30%">' . '<p>' . $part['serial_alpha'] . '</p>' . '</td>';
$html .= '<td>' . $part['serial_no'] . '</td>';
$html .= '<td>' . $part['updated_at'] . '</td>';
$html .= '<td>' . ($part['testing'] == 0 ? 'Failure' : 'Ok') . '</td>';
$html .= '</tr>';
$html .= '</tbody>';

$html .= '</table>';
/** */



$html .= '<h5>Accessories info</h5>';

$html .= '<table width="100%">';

$html .= '<thead>';
$html .= '<tr>';
$html .= '<th>Name</th>';
$html .= '<th>Serial Alpha</th>';
$html .= '<th>Serial No</th>';
$html .= '<th>Operator</th>';
$html .= '<th>Updated Date</th>';
$html .= '<th>Status</th>';
$html .= '</tr>';
$html .= '</thead>';

$html .= '<tbody>';

foreach ($access as $key => $a) {
    $html .= '<tr>';
    $html .= '<td>' . $a['acc_name'] . '</td>';
    $html .= '<td style="padding: 10px 5px">' . '<img src="' . $a['base64'] . '" style="width:30%">' . '<p>' . $a['serial_alpha'] . '</p>' . '</td>';
    $html .= '<td>' . $a['serial_no'] . '</td>';
    $html .= '<td>' . ($a['testing'] == '1' ? $a['inserted_by'] : $a['updated_by']) . '</td>';
    $html .= '<td>' . $a['updated_at'] . '</td>';
    $html .= '<td>' . ($a['testing'] == '0' ? 'Failure' : 'Ok') . '</td>';
    $html .= '</tr>';
}

$html .= '</tbody>';

$html .= '</table>';

//** */

$html .= '<h5>Stage info</h5>';

$html .= '<table class="table table-bordered table-sm" width="100%">';

$html .= '<thead>';
$html .= '<tr>';
$html .= '<th>Stage Name</th>';
$html .= '<th>Operator</th>';
$html .= '<th>Updated Date</th>';
$html .= '<th>Status</td>';
$html .= '</tr>';
$html .= '</thead>';

$html .= '<tbody>';

foreach ($stages as $key => $s) {

    $html .= '<tr>';
    $html .= '<td>' . $s['main_stage_description'] . '</td>';
    $html .= '<td>' . $s['created_by'] . '</td>';
    $html .= '<td>' . $s['created_at'] . '</td>';
    $html .= '<td>' . ($s['status'] == '0' ? 'Failure' : ($s['status'] == '1' ? 'Ok' : '-')) . '</td>';
    $html .= '</tr>';
}

$html .= '</tbody>';

$html .= '</table>';


$mpdf->writeHTML($html);
$html1 = '';
$mpdf->AddPage();

/** */

$result = mysqli_query($db, "SELECT * FROM a_hvtest_log where part_id= ${part['id']} ");
$tests = array();
$t0 = array();
$t1 = array();
while ($row = mysqli_fetch_assoc($result)) {
    $tests[] = $row;
    if ($row['state'] == '1')        $t1[] = $row;
    else        $t0[] = $row;
}
// if (empty($t1)) $tests = $t0;
// else $tests = $t1;

$html1 .= '<br><h5>HV & IR Test</h5>';
$html1 .= '<p>Configuration level : ' . $part['reading'] . '</p>';



$html1 .= '<table width="100%">
<thead>
<tr>
<th >
Results Dielectric Tests
</th>
<th >
Measured Resistor Value
</th>
<th >
OK / NOK
</th>

</tr>
</thead>
<tbody>';


for ($i = 0; $i < count($tests); $i++) {
    $row = $tests[$i];
    $html1 .= '<tr>';
    $html1 .= '<td>' . $row['test_name'] . '</td>';
    $html1 .= '<td>' . $row['reading'] . '</td>';
    $html1 .= '<td>' . ($row['test_state'] == 1 ? 'OK' : 'NOK') . '</td>';
    // $html1 .= '<td>' . $row['created_at'] . '</td>';
    $html1 .= '</tr>';
}
$html1 .= '</tbody></table>';


$html1 .= (' 
<br>
<table width="100%">
<tbody>
<tr>
<td colspan="5" height="80px"></td>
</tr>
<tr>
<td colspan="3" style="border: 0px;vertical-align: bottom;" height="80px">
(QUALITY)
</td>
<td colspan="2" style="border: 0px;vertical-align: bottom;">
(PRODUCTION)
</td>
</tr>
</tbody>
</table>');

$mpdf->writeHTML($html1);
mysqli_close($db);
$dir = '../../../backup/full/' . $part['serial_no'];
if (!file_exists($dir)) {
    mkdir($dir, 0777, true);
}
$mpdf->Output($dir . '/' . $part['serial_no'] . '_' . date("h-iad-m-Y") . '.pdf', 'F');
$mpdf->Output($part['serial_no'] . '.pdf', 'I');
