<?php

require 'vendor/autoload.php';

function getBarcodeBase64($serialNo)
{
    $redColor = [255, 0, 0];
    $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
    return 'data:image/png;base64,' . base64_encode($generator->getBarcode($serialNo, $generator::TYPE_CODE_128, 2, 50));
}
//$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
//file_put_contents('barcode.png', $generator->getBarcode('081231723897', $generator::TYPE_CODE_128, 3, 50, $redColor));
