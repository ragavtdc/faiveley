<?php

require_once '../../../print/vendor/autoload.php';
require_once '../../barcode/barcode.php';


$mpdf = new \Mpdf\Mpdf([
    'pagenumPrefix' => 'SHEET ',
    'setAutoTopMargin' => 'stretch',
    'autoMarginPadding' => 0,
    'showImageErrors' => true,
    'default_font_size' => 9,
    'default_font' => 'FreeSans'
]);

// complete($part);
// <td colspan="2"> {PAGENO} / {nbpg} </td>

/**pdf start */

$pdfHeader = '
<table width="100%" >
    <tr>
        <td><img src="../../../../images/logo-wabtec.png"  height="60px"></td>
        <td>DENOMINATION<br>WSP+BCU SLAVE - METRO MUMBAI L3</td>
        <td>CODE<br>FT0027800-016-E00BOM</td>
    </tr>
    <tr>
        <td>DATE 20/06/19</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>DRAWN Fassetta</td>
        <td></td>
        <td>{PAGENO} / {nbpg}</td>
    </tr>
    <tr>
        <td>APPROVED Murazzano</td>
        <td></td>
        <td></td>
    </tr>
</table>
<br>
';

$foot = '
<table width="100%" >
    <tr>
        <td style="height:15px"></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="height:15px"></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="height:15px"></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>ECO19PSC1037</td>
        <td>C00</td>
        <td>18/11/19</td>
        <td>FASSETTA</td>
        <td>MURAZZANO</td>
        <td>CHANGED CODE TO IDENTIFICATION LABEL REF.6</td>
    </tr>
    <tr>
        <td>ECO19PSC0779</td>
        <td>B00</td>
        <td>29/08/19</td>
        <td>FASSETTA</td>
        <td>MURAZZANO</td>
        <td>CHANGED WSP SIDE LAYOUT (MOVED ETH BOARD)</td>
    </tr>
    <tr>
        <td>--------</td>
        <td>A01</td>
        <td>27/06/19</td>
        <td>FASSETTA</td>
        <td>MURAZZANO</td>
        <td>ISSUE</td>
    </tr>
    <tr>
        <th style="text-align:center;">NUMBER</th>
        <th style="text-align:center;">REV</th>
        <th style="text-align:center;">DATE</th>
        <th style="text-align:center;">DRAWN</th>
        <th style="text-align:center;">APPROVED</th>
        <th style="text-align:center;">DESCRIPTION</th>
    </tr>
</table>
';

$mpdf->SetHTMLHeader($pdfHeader);
$mpdf->SetHTMLFooter($foot);

$html = '
<style>
table,tr,th,td{
    border: 0.4px solid grey;
    border-collapse: collapse;
    text-align: left;
    vertical-align: middle;
}
th,td{
    padding : 2px;
}

</style>';
/** */

$html .= '<table width="100%" >';
$html .= '<thead>';
$html .= '<tr>';
$html .= '<th style="text-align: center;">REF</th>';
$html .= '<th style="text-align: center;">QTY</th>';
$html .= '<th>DESCRIPTION</th>';
$html .= '<th>CODE</th>';
$html .= '<th>NOTE</th>';
$html .= '</tr>';
$html .= '</thead>';

$html .= '<tbody>';

$ar = array();

$ar[] = ["RACK 84 TE 3U PE LEFT", "FT0105118-100", "", ''];
$ar[] = ['CONDUCTIVE GASKET 84TE', '2/51515', '', ''];
$ar[] = ['REAR COVER 84TE', 'FT0026043-100', '', ''];
$ar[] = ['EARTH LABEL', '2/514855', '', ''];

$ar[] = ['IDENTIFICATION LABEL', 'FT0041631-000', '', ''];
$ar[] = ['IDENTIFICATION LABEL', 'FT0024892-000', '', ''];
$ar[] = ['INSULATION STRIP', 'N.A', '', ''];
$ar[] = ['MOTHERBOARD 48+36TE METRO MUMBAI L3', 'FT0120933-100', '0734173B00X0018', ''];

$ar[] = ['FRONTAL PLATE 4TE', '2/514068', '', ''];
$ar[] = ['POWER SUPPLY PSG2VDAT', 'FT0106838-100', '0734054A01P0102', ''];
$ar[] = ['ECHELON BOARD', 'FT0027241-100', '0522534B00T0510', ''];

$ar[] = ['IDB_1 BOARD', 'FT0115338-100', '0523380A01T0045', ''];
$ar[] = ['DI_1 BOARD', '1/456535', '1037788J00L3746', ''];
$ar[] = ['DO_1 BOARD', '1/456540', '1040665I00L4714', ''];
$ar[] = ['RL_1 BOARD', '1/456615', '0523552D02T0861', ''];
$ar[] = ['WBI_1 BOARD', 'FT0029675-100', '2287196D00Y0005', ''];
$ar[] = ['WBI_2 BOARD', 'FT0029675-101', '2287198D00Y0005', ''];

$ar[] = ['CPU_1 G2F BOARD WSP METRO MUMBAI L3', 'FT0120930-100', '1039565A00L0001', ''];
$ar[] = ['CPU_2 G2F LD BOARD BCU METRO MUMBAI L3', 'FT0120931-100', '0523380A00T0027', ''];
$ar[] = ['ECHELON BOARD', 'FT0027241-100', '0522885B00T0564', ''];
$ar[] = ['IDB_2 BOARD', 'FT0115338-101', '0523380A01T0008', ''];

$ar[] = ['DI_2 BOARD', '1/456536', '1039516J00L1391', ''];
$ar[] = ['DO_2 BOARD', '1/456541', '2287202I00Y0022', ''];
$ar[] = ['RL_2 BOARD', '1/456616', '0522598C02T0297', ''];
$ar[] = ['POWERSUPPLY PSG2AT', 'FT0039263', '0733805D00P0068', ''];
$ar[] = ['FT BAR CODE LABEL', 'FT0029110-000', '', ''];
$ar[] = ['FT TEST LABEL', 'FT0029111-000', '', ''];


foreach ($ar as $k => $v) {
    $html .= '
    <tr>
        <td style="text-align: center;">' . ($k + 1) . '</td>
        <td style="text-align: center;">1</td>
        <td>' . $v[0] . '</td>
        <td>' . $v[1] . '</td>';
    if ($v[2] == '') {
        $html .= '<td></td>';
    } else {
        $html .= '<td style="text-align: center;"><img style="max-height:16px" src="' . getBarcodeBase64($v[2]) . '"> <br><span style="font-size:8pt">' . $v[2] . '</span></td>';
    }

    $html .= '</tr>';
}

$html .= '</tbody>';
$html .= '</table>';
/** */


$mpdf->writeHTML($html);


$mpdf->Output('slave.pdf', 'I');
