<?php

$res = array();

function getPartDetails($db, $partcode)
{
    $stmt = $db->prepare("SELECT * FROM part_master WHERE part_code_id=? LIMIT 1");

    $stmt->bind_param('s', $partcode);

    /*  if ($stmt->execute()) {
        echo "function";
        $result = $stmt->get_result();
        echo mysqli_num_rows($result);
        $row = mysqli_fetch_assoc($result);
       // $row["part_code"]= base64_decode($row["part_code"]);
        $res = $row;
        echo json_encode($result);
        complete($res);
    } else {
        err('failed');
    }
}*/

    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows === 0) exit('No rows');
    while ($row = $result->fetch_assoc()) {
        $row["part_code"] = base64_encode($row["part_code"]);

        complete($row);
    }

    $stmt->close();
}

$rId = 3;
require '../../includes/init.php';

$db = db();

extract($_REQUEST);

getPartDetails($db, $partcode);
