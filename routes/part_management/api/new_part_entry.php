<?php

$rId = 3;
require '../../includes/init.php';

$res = array();

function addNewPart($db, $part_number, $part_name, $part_type, $pdate, $part_code)
{
    $fileName = basename($_FILES["fileUpload"]["name"]);
    $fileType = pathinfo($fileName, PATHINFO_EXTENSION);

    // Allow certain file formats 
    $allowTypes = array('jpg', 'png', 'jpeg', 'gif');

    $image = $_FILES['fileUpload']['tmp_name'];
    $imgContent = addslashes(file_get_contents($image));

    $stmt = $db->prepare("INSERT INTO part_master(part_number,part_name,part_type,part_date,part_code_id,part_code)VALUES('$part_number', '$part_name', '$part_type', '$pdate', '$part_code','$imgContent')");
    // $stmt = $db->prepare("INSERT INTO part_master(part_number,part_name,part_type,part_date,part_code_id,part_code)VALUES(?,?,?,?,?,?)");

    // $stmt->bind_param('sssssb', $part_number, $part_name, $part_type, $pdate, $part_code, $imgContent);

    if ($stmt->execute()) {
        $_SESSION['success'] = "Part Added Successfully";
    } else {
        $_SESSION['error'] = mysqli_error($db) . "Unable to add part try again later";
    }
    header('Location: ../');
}

function updatePart($db, $part_number, $part_name, $part_type, $pdate, $part_code)
{
    $fileName = basename($_FILES["fileUpload"]["name"]);
    $fileType = pathinfo($fileName, PATHINFO_EXTENSION);

    // Allow certain file formats 
    $allowTypes = array('jpg', 'png', 'jpeg', 'gif');

    $image = $_FILES['fileUpload']['tmp_name'];
    $imgContent = addslashes(file_get_contents($image));
    $stmt = $db->prepare("UPDATE part_master SET part_number ='$part_number' ,part_name='$part_name',part_type='$part_type',part_date='$pdate',part_code_id='$part_code',part_code='$imgContent' WHERE part_code_id= '$part_code'");


    if ($stmt->execute()) {
        $_SESSION['success'] = "Part Updated Successfully";
    } else {
        $_SESSION['error'] = "Unable to update... try again later" . mysqli_error($db);
    }
    header('Location: ../');
}

extract($_POST);

$db = db();

if (json_encode($_FILES['fileUpload']['name']) != null && json_encode($_FILES['fileUpload']['name'] != '') && strlen(json_encode($_FILES['fileUpload']['name']) > 0)) {

    if (isset($updatePart)) {
        updatePart($db, $part_number, $part_name, $part_type, $pdate, $part_code);
    } else {
        addNewPart($db, $part_number, $part_name, $part_type, $pdate, $part_code);
    }
    header('Location: ../');
} else {
    if (isset($updatePart)) {
        updatePart($db, $part_number, $part_name, $part_type, $pdate, $part_code);
    } else {
        addNewPart($db, $part_number, $part_name, $part_type, $pdate, $part_code);
    }
}
