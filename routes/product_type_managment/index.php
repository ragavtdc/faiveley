<?php

$rId = 22;
require '../includes/init.php';

$db = db();

$res = array();

$stmt = $db->prepare('SELECT username FROM user_master WHERE state=1');

if ($stmt->execute()) {
    $result = $stmt->get_result();
    while ($rows = mysqli_fetch_assoc($result)) {
        $res[] = $rows;
    }
}
function getParts(): array
{
    $db = db();
    $rows = array();
    $result = mysqli_query($db, "SELECT * FROM a_part_master");
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    mysqli_close($db);
    return $rows;
}
head();
?>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-users'></i> Product Type Management </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-product-hunt fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>Product Type Management</a></li>
    </ul>
</div>
<?php if (isset($_SESSION['error'])) {
?>
    <h2 class='tile red text-center text-white'>
        <?php echo $_SESSION['error'];
        unset($_SESSION['error']);
        ?>
    </h2>
<?php }
?>

<?php if (isset($_SESSION['success'])) {
?>
    <h2 class='tile green text-center text-white'>
        <?php echo $_SESSION['success'];
        unset($_SESSION['success']);
        ?>
    </h2>
<?php }
?>

<div class='container'>
    <!--Accordion wrapper-->
    <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

        <!-- Accordion card -->
        <div class="tile">
            <!-- Card header -->
            <div class="tile-header" role="tab">
                <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne2">
                    <h5 class="mb-0">
                        Add New Product
                    </h5>
                </a>
            </div>
            <!-- Card body -->
            <div id="collapseOne2" class="collapse mt-3" role="tabpanel" data-parent="#accordionEx">
                <div class="tile-body">
                    <form id="addNew">
                        <div class='row'>
                            <div class='form-group col-sm-12'>
                                <label class='control-label'>Part Name</label>
                                <input class='form-control' type='text' placeholder='Enter Part Name' name='partName' id='partName' required>
                            </div>
                            <div class='form-group col-sm-12'>
                                <label class='control-label'>Part No</label>
                                <input class='form-control' type='text' placeholder='Enter part No' name='partNo' id='partNo' required>
                            </div>
                            <div class='form-group col-sm-12'>
                                <label class='control-label'>Product Description</label>
                                <input class='form-control' type='text' placeholder='Enter product description' name='des' id='des' required>
                            </div>

                            <div class='form-group btn-container col-lg-12 col-md-12 col-sm-12 text-center'>
                                <button type="submit" class='btn btn-primary'>ADD</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Accordion card -->
        <!-- Accordion card -->
        <div class="tile">
            <!-- Card header -->
            <div class="tile-header" role="tab">
                <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1">
                    <h5 class="mb-0">
                        Edit Product
                    </h5>
                </a>
            </div>
            <!-- Card body -->
            <div id="collapseOne1" class="collapse show mt-3" role="tabpanel" data-parent="#accordionEx">
                <div class="tile-body">
                    <div class='row'>
                        <div class='form-group col-md-6 col-sm-10 offset-sm-1 offset-md-3 text-center'>
                            <select class='form-control' onchange="getPartDetails()" id='selectedPart' name='selectedPart'>
                                <option disabled selected>Select Part</option>

                                <?php
                                $parts = getParts();
                                foreach ($parts as $part) {
                                    echo ' <option value="' . $part['part_id'] . '">' . $part['part_name'] . '</option>';
                                }
                                ?>

                            </select>
                        </div>
                        <div class='form-group col-lg-12 col-md-12 col-sm-12 text-center'>
                            <div id='progressDiv' style='display: none;' class='col-lg-2 offset-lg-5 col-md-2 offset-md-5 col-sm-12'>
                                <div class='progress'>
                                    <div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='75' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form id="updatePart" style="display: none;">
                        <div class='row'>
                            <div class='form-group col-sm-12'>
                                <label class='control-label'>Part Name</label>
                                <input class='form-control' type='text' placeholder='Enter Part Name' name='partName' id='uPartName' required>
                            </div>
                            <div class='form-group col-sm-6'>
                                <label class='control-label'>Part No</label>
                                <input class='form-control' type='text' placeholder='Enter part No' name='partNo' id='uPartNo' required>
                            </div>
                            <div class='form-group col-sm-6'>
                                <label class='control-label'>Part Status</label>
                                <select class='form-control' id='uPartState' name='partState' required>
                                    <option value="0">Disabled</option>
                                    <option value="1">Enabled</option>
                                </select>
                            </div>
                            <div class='form-group col-sm-12'>
                                <label class='control-label'>Product Description</label>
                                <input class='form-control' type='text' placeholder='Enter product description' name='des' id='uDes' required>
                            </div>

                            <div class='form-group btn-container col-lg-12 col-md-12 col-sm-12 text-center'>
                                <input class='form-control' type='hidden' name='partId' id='uPartId' required>
                                <button type="submit" class='btn btn-primary'>UPDATE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Accordion card -->

    </div>
    <!-- Accordion wrapper -->

</div>
<!-- </div> -->

<?php foot();
?>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script>
    const progressDiv = $('#progressDiv');
    const addForm = $('#addNew');
    const updateForm = $('#updatePart');

    function getPartDetails() {

        var selectedPart = $('#selectedPart').val();

        if (selectedPart == undefined || selectedPart == null) {
            return;
        }

        progressDiv.show();

        let func = (data) => {

            updateForm.show();
            progressDiv.hide();
            $('#uPartName').val(data.part_name);

            $('#uPartNo').val(data.part_no);
            $('#uPartState').val(data.state);
            $('#uDes').val(data.part_description);
            $('#uPartId').val(selectedPart);

        }

        ajax('api/product_management_details.php', {
                'partcode': selectedPart,
            },
            func
        );
    }


    addForm.submit((e) => {
        e.preventDefault();
        let func = (data) => {
            if (data === true) {
                successToast('added successfully');
                addForm.trigger("reset");
            }
        };
        ajax('./api/add.php', addForm.serialize(), func);
    });

    updateForm.submit((e) => {
        e.preventDefault();
        let func = (data) => {
            if (data === true) {
                successToast('updated successfully');
                $('#selectedPart').prop('selectedIndex', 0);
                updateForm.hide();
            }
        };
        ajax('./api/update.php', updateForm.serialize(), func);

    });
</script>
</body>

</html>