<?php

$rId = 3;

require '../../includes/init.php';

check('partId', 'part id required');
check('partName', 'part name required');
check('partNo', 'part number required');
check('partState', 'part state required');
check('des', 'part description required');

extract($_POST);

$db = db();

$stmt = $db->prepare("UPDATE a_part_master SET part_name = '$partName' ,part_no = '$partNo', part_description = '$des' ,state = '$partState' where part_id = '$partId'");
if ($stmt->execute()) {
    mysqli_close($db);
    complete(true);
}
mysqli_close($db);
err(mysqli_error($db) . "Unable to add part try again later");
