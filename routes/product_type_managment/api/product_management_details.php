<?php

$rId = 3;

require '../../includes/init.php';

$db = db();

extract($_REQUEST);

$res = array();


$stmt = $db->prepare("SELECT * FROM a_part_master WHERE part_id=? LIMIT 1");

$stmt->bind_param('d', $partcode);

$stmt->execute();
$result = $stmt->get_result();
$stmt->close();
$db->close();
if ($result->num_rows === 0) err('No part found');
$row = $result->fetch_assoc();
complete($row);
