<?php

$rId = 3;

require '../../includes/init.php';

check('partName', 'part name required');
check('partNo', 'part number required');
check('des', 'part description required');

extract($_POST);

$db = db();

$stmt = $db->prepare("INSERT INTO a_part_master(part_name,part_no,part_description)VALUES('$partName', '$partNo', '$des')");
if ($stmt->execute()) {
    mysqli_close($db);
    complete(true);
}
mysqli_close($db);
err(mysqli_error($db) . "Unable to add part try again later");
