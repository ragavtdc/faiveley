<?php

require_once __DIR__ . '/db.php';

function path(): string
{
    $path = '/';
    if ($_SERVER['HTTP_HOST'] == 'adc.bitsathy.ac.in') {
        $path = 'https://adc.bitsathy.ac.in/projects/faiveley/';
    }
    return $path;
}

$path = path();

function head()
{
    $path = path();
    require_once 'header.php';
    // require_once 'menu.php';
}
function foot()
{
    $path = path();
    require_once 'footer.php';
}

function err($err)
{
    $db = db();
    $user = $_SESSION['user']['username'];
    mysqli_query($db, "INSERT INTO err_log (err_msg,username) values('$err','$user')");
    mysqli_close($db);
    $res['state'] = false;
    $res['err'] = $err;
    // header('Content-Type: application/json');
    echo json_encode($res, JSON_PRETTY_PRINT);
    die();
}

function check($query, $err)
{
    if (!isset($_REQUEST[$query])) {
        err($err);
    }
}

function complete($data)
{
    $res['state'] = true;
    $res['data'] = $data;
    // header('Content-Type: application/json');
    echo json_encode($res, JSON_PRETTY_PRINT);
    die();
}

function viewDate($dbDate)
{
    try {
        $date = date_create($dbDate);
        return date_format($date, "d-M-Y");
    } catch (\Throwable $th) {
        return $dbDate;
    }
}

function viewDateTime($dbTimeStamp)
{
    try {
        $date = date_create($dbTimeStamp);
        return date_format($date, "d-M-Y h:i:s a");
    } catch (\Throwable $th) {
        return $dbTimeStamp;
    }
}
