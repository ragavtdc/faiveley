<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

function db()
{
    $config = parse_ini_file('db.ini');
    // $dbContent = file_get_contents( 'http://apps.bittechpro.in/db/rstahl_db.ini' );
    // $config = parse_ini_file( $dbContent, true );

    // $host = $config['server'];
    $host = 'adc.bitsathy.ac.in';
    $dbname = 'faiveley';
    $username = $config['username'];
    $password = $config['password'];

    $db = mysqli_connect($host, $username, $password, $dbname) or die('Connection Failed');

    if ($db) {
        return $db;
    } else {
        die(" error in creating connection ");
    }
}
