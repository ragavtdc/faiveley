<!DOCTYPE html>
<html lang='en'>

<head>
    <title>Faiveley</title>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="shortcut icon" href="<?php echo $path ?>images/favicon-wabtec.png" type="image/png">
    <!-- Main CSS-->
    <link rel='stylesheet' type='text/css' href='<?php echo $path ?>css/main.css'>
    <!-- Font-icon css-->
    <link rel='stylesheet' type='text/css' href='<?php echo $path ?>js/font-awesome/4.7.0/css/font-awesome.min.css'>
    <!--     Fonts and icons     -->
    <link rel='stylesheet' type='text/css' href='<?php echo $path ?>css/material.css' />
    <link href='<?php echo $path ?>js/mdb-css/bootstrap.min.css' rel='stylesheet'>
    <link rel='stylesheet' type='text/css' href='<?php echo $path ?>css/style.css'>
    <link href='https://fonts.googleapis.com/css?family=Libre Barcode 39' rel='stylesheet'>
</head>

<body class='app sidebar-mini'>

    <header class='app-header'><a class='app-header__logo' style="background-color: white;" href='#'><img class="img-fluid" style="vertical-align: top;max-height: 50px;" src="<?php echo $path ?>images/faiveley.jpg" alt="LOGO"></a>
        <!-- Sidebar toggle button-->
        <a class='app-sidebar__toggle' href='#' data-toggle='sidebar' aria-label='Hide Sidebar'></a>
        <!-- Navbar Right Menu-->
        <ul class='app-nav'>
            <!-- User Menu-->
            <li class='dropdown'><a class='app-nav__item' href='#' data-toggle='dropdown' aria-label='Open Profile Menu'><i class='fa fa-user fa-lg'></i></a>
                <ul class='dropdown-menu settings-menu dropdown-menu-right'>
                    <?php if ($_SESSION['role'] != 'Guest') {
                    ?>
                        <!-- <li><a class='dropdown-item' href='page-user.php'><i class='fa fa-user fa-lg'></i> Profile</a></li> -->

                        <li><a class='dropdown-item' href='<?php echo $path ?>page-lockscreen.php'><i class='fa fa-lock fa-lg'></i>
                                Lock</a>
                        </li>
                    <?php }
                    ?>
                    <li><a class='dropdown-item' href='<?php echo $path ?>page-login.php'><i class='fa fa-sign-out fa-lg'></i> Logout</a>
                    </li>
                </ul>
            </li>
        </ul>
    </header>
    <?php require_once 'menu.php'; ?>
    <!-- start your code here -->
    <main class='app-content'>
        <?php
        if (empty($isAuthorized) && $rId != 0) {
            echo "<h3>Unauthorized page!</h3>";
            foot();
            die();
        }
        ?>
        <script>
            document.title = '<?php if (empty($isAuthorized)) {
                                    echo $isAuthorized['resource_name'];
                                } else echo 'Faiveley'; ?>';
        </script>