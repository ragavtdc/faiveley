<!-- Sidebar menu-->
<div class='app-sidebar__overlay' data-toggle='sidebar'></div>
<aside class='app-sidebar'>
    <div class='app-sidebar__user'><img class='app-sidebar__user-avatar' src='<?php echo $path; ?>images/emp.png' alt='User Image'>
        <div>
            <p class='app-sidebar__user-name'>
                <?php
                $user = $_SESSION['user'];
                echo strtoupper($user['emp_name']);
                ?></p>
        </div>
    </div>
    <ul class='app-menu'>

        <?php

        $db = db();


        $uid = $user['username'];

        $sql = "SELECT rm.* FROM resource_master rm
        WHERE STATUS = 1 AND 
        FIND_IN_SET(
            rm.resource_id,
                (SELECT GROUP_CONCAT(rrm.resource_id) FROM role_resource_mapping rrm 
                    WHERE FIND_IN_SET(
                            rrm.role_id ,
                            (SELECT um.role FROM user_master um WHERE um.username = '$uid')
                        )>0
                )
            )>0 
        ORDER BY sort_order";

        $result = mysqli_query($db, $sql);

        mysqli_close($db);

        $menus = array();

        $isAuthorized = array();


        while ($row = mysqli_fetch_assoc($result)) {
            $row['id'] = (int) $row['resource_id'];
            $row['parent_id'] = (int) $row['parent_id'];
            if ($GLOBALS['rId'] == $row['id']) {
                $isAuthorized = $row;
            }
            $menus[$row['id']] = $row;
        }

        function detectSelectedMenu($sMenu, $menus, $stack = []): array
        {

            if ($sMenu['parent_id'] == 0) {
                $stack[] = $sMenu['id'];
            } else {
                foreach ($menus as $menu) {
                    if ($menu['id'] == $sMenu['parent_id']) {
                        $stack = detectSelectedMenu($menu, $menus, $stack);
                        break;
                    }
                }
            }
            return $stack;
        }
        $stack = array();
        if (!empty($isAuthorized)) {
            $stack = detectSelectedMenu($isAuthorized, $menus, [$isAuthorized['id']]);
            // die(json_encode($stack));
        }

        function generateMenu($pId, $menus, $stack)
        {

            $path = path();

            $subMenus = array();

            foreach ($menus as $menu) {
                if ($menu['parent_id'] == $pId) {
                    $subMenus[] = $menu;
                }
            }

            foreach ($subMenus as $subMenu) {
                $count = 0;
                $id = $subMenu['id'];

                foreach ($menus as $menu) {
                    if ($menu['parent_id'] == $id) {
                        $count += 1;
                    }
                }
                $selected = in_array($id, $stack);
                if ($count == 0) {

                    $url = $path . $subMenu['resource_link'];

                    echo '<li>
                        <a class="app-menu__item ' . ($selected ? 'active' : '') . ' " href="' . $url . '">
                            <i class="app-menu__icon fa ' . $subMenu['icon'] . '"></i>
                            <span class="app-menu__label">' . $subMenu['resource_name'] . '</span>
                        </a>
                        </li>';

                    continue;
                }
                echo '<li class="treeview ' . ($selected ? 'is-expanded' : '') . '">
                        <a class="app-menu__item" href="javascript:void(0);" data-toggle="treeview">
                            <i class="app-menu__icon fa ' . $subMenu['icon'] . '"></i>
                            <span class="app-menu__label">' . $subMenu['resource_name'] . '</span>
                            <i class="treeview-indicator fa fa-angle-right"></i>
                        </a>
                        <ul class="treeview-menu">';

                generateMenu($id, $menus, $stack);

                echo ' </ul> </li>';
            }
        }

        generateMenu(0, $menus, $stack);

        ?>
        <!-- <li>
            <a class="app-menu__item" href="/routes/file-manager/">
                <i class="app-menu__icon fa fa-file"></i>
                <span class="app-menu__label">Files</span>
            </a>
        </li> -->

    </ul>
</aside>