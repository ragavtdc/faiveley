</main>

<script src="<?php echo $path ?>js/jquery-3.3.1.min.js"></script>
<script src="<?php echo $path ?>js/popper.min.js"></script>
<script src="<?php echo $path ?>js/bootstrap.min.js"></script>
<script src="<?php echo $path ?>js/main.js"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="<?php echo $path ?>js/plugins/pace.min.js"></script>
<script src='<?php echo $path ?>js/plugins/bootstrap-notify.min.js'></script>
<script src="<?php echo $path ?>js/toast.js"></script>

<script>
    function ajax(url, data = {}, func = (data) => {}) {
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function(result) {
                $('#progressDiv').hide();
                try {
                    console.log(result);
                    var res = $.parseJSON(result);
                    if (res.state === true) {
                        func(res.data);
                        return;
                    }
                    toast('[error]', res.err);
                } catch (error) {
                    console.error('[ajax call]', error);
                    toast('', 'Error!');
                }
            },
        });
    }

    <?php
    if (isset($_SESSION['error'])) {
    ?>
        toast(`<?php echo $_SESSION['error'];
                unset($_SESSION['error']); ?>`);

    <?php
    }
    if (isset($_SESSION['success'])) {
    ?>
        successToast(`<?php echo $_SESSION['success'];
                        unset($_SESSION['success']); ?>`)
    <?php }
    ?>
</script>