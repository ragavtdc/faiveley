<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

require_once __DIR__ . '/db.php';
require_once __DIR__ . '/functions.php';

if (!isset($rId)) {
    die('Resource not found');
}

session_start();

if (isset($_SESSION['lock']) && $_SESSION['lock'] == true) {
    header("Location: {$path}page-lockscreen.php");
    die();
}

if (!isset($_SESSION['role'])) {
    $_SESSION['close'] = 'close';
    header("Location: {$path}page-login.php");
}

$user = $_SESSION['user'];
