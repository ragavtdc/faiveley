<?php

$rId = 25;

require '../../includes/init.php';

check('newFname', 'Failure name required');
check('newFdes', 'Failure Description required');

extract($_POST);

$db = db();

$stmt = $db->prepare("INSERT INTO failure_master(failure_name,failure_description)VALUES('$newFname','$newFdes')");
if ($stmt->execute()) {
    mysqli_close($db);
    complete(true);
}
mysqli_close($db);
err(mysqli_error($db) . "Unable to add part try again later");
