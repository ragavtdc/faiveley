<?php

$rId = 2;
require '../includes/init.php';

$db = db();

$res = array();

$stmt = $db->prepare("SELECT * FROM user_master WHERE state=1 and username <> 'admin'");

if ($stmt->execute()) {
    $result = $stmt->get_result();
    while ($rows = mysqli_fetch_assoc($result)) {
        $res[] = $rows;
    }
}

$roles = array();
$opRoles = array();
$result = mysqli_query($db, "SELECT * from role_master where status = 1 ");
while ($row = mysqli_fetch_assoc($result)) {
    if ($row['operator'] == '1')
        $opRoles[] = $row;
    else
        $roles[] = $row;
}
head();
?>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-users'></i> User Management </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-users fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>User Management</a></li>
    </ul>
</div>

<div class='container'>
    <div class='row tile'>
        <!-- <div class='form-group col-lg-6 col-md-6 col-sm-12'>
            <h4>Select an User</h4>
        </div> -->
        <div class='form-group col-lg-6 col-md-6 col-sm-12'>
            <select class='form-control' name='selectedUser' id='selectedUser'>
                <option disabled selected>Select employee</option>
                <?php foreach ($res as $item) {
                ?>
                    <option value="<?php echo $item['username'] ?>"><?php echo $item['emp_name'] ?></option>
                <?php }
                ?>
            </select>
        </div>
        <div class='form-group col-lg-6 col-md-6 col-sm-12 text-center'>
            <button id='getDetailsBtn' data-toggle="tooltip" title="View / Edit selected employee" onclick='getDetails()' class='btn btn-purple btn-sm'><i class="material-icons text-info ">info</i></button>
            <button id='removeUserBtn' data-toggle="tooltip" title="Delete selected employee" onclick='removeUser()' class='btn btn-red btn-sm'><i class="material-icons text-danger ">delete</i></button>
            <button id='addUserBtn' data-toggle="tooltip" title="Add New Employee" onclick='window.location.reload()' class='btn btn-purple btn-sm'><i class="material-icons text-success ">add</i></button>
        </div>
        <div class='col-12 text-center'>
        </div>
    </div>
</div>
<div class='container'>
    <form action='api/new-user.php' method='POST' enctype='multipart/form-data'>
        <div class='row tile'>
            <div class='form-group text-center col-sm-12'>
                <h5 id="formTitle">Add New Employee</h5>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Employee ID</label>
                <input class='form-control' type='text' placeholder='Enter employee id' name='username' id='username' required autofocus>
                <h6 id="userNameText"></h6>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Employee Name</label>
                <input class='form-control' type='text' placeholder='Enter name' name='comp_id' id='comp_id' required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Designation</label>
                <input class='form-control' type='text' placeholder='Enter Job Designation' name='designation' id='designation' required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Date of Birth</label>
                <input class='form-control' type='date' placeholder='Select Date of Birth' name='dob' id='dob' required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Date of Joining</label>
                <input class='form-control' type='date' placeholder='Select Date of Joining' name='doj' id='doj' required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Address</label>
                <textarea class='form-control' rows="3" placeholder='Enter Address' minlength="5" name='address' id='address' required></textarea>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>

                <lable disabled>Select Roles</lable>
                <select class='form-control' multiple title="CTRL + Click to select multiple" name='roles[]' onchange="toggleOpRoles()" id='roles' required>
                    <?php
                    foreach ($roles as $role) {
                        echo "<option value='" . $role['role_id'] . "'>" . $role['role_name'] . "</option>";
                    }
                    ?>
                </select>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>

                <lable disabled>If Operator, select operator roles</lable>
                <select class='form-control' disabled multiple title="CTRL + Click to select multiple" name='opRoles[]' id='opRoles' required>
                    <?php
                    foreach ($opRoles as $role) {
                        echo "<option value='" . $role['role_id'] . "'>" . $role['role_name'] . "</option>";
                    }
                    ?>
                </select>
            </div>

            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Contact Number</label>
                <input class='form-control' type='tel' maxlength='10' minlength="10" placeholder='Enter Mobile Number' name='contact' id='contact' required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Email ID</label>
                <input class='form-control' type='email' placeholder='Enter Email ID' name='email' id='email' required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Select Shift</label>
                <select class='form-control' name='shift' id='shift' required>
                    <option disabled selected>Select a Shift...</option>
                    <option value='1'>1</option>
                    <option value='2'>2</option>
                    <option value='3'>3</option>
                </select>
            </div>

            <div class='input-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label' style='width: 100%;margin: unset;'>Upload</label>
                <div class='custom-file' style='margin-top: -8px;'>
                    <input type='file' name='fileUpload' class='custom-file-input' accept='.png,.jpg' id='fileUploader'>
                    <label class='custom-file-label' for='fileUploader'>Upload User Photo</label>
                </div>
            </div>
            <div class='form-group btn-container col-lg-12 col-md-12 col-sm-12 text-center'>
                <button id='saveUser' name='saveUser' class='btn btn-primary'><i class='fa fa-save'></i>SAVE</button>
                <button id='updateUser' style='display: none;' name='updateUser' class='btn btn-primary'><i class='fa fa-save'></i>UPDATE</button>
            </div>
    </form>
</div>
<!-- </div> -->
<!-- </div> -->
<link href='../../js/mdb-css/bootstrap.min.css' rel='stylesheet'>
<?php foot();
?>



<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script>
    function getDetails() {

        var getDetailsBtn = $('#getDetailsBtn');
        var selectedUser = $('#selectedUser').val();
        var updateUser = $('#updateUser');
        var saveUser = $('#saveUser');

        if (selectedUser == null || selectedUser == '') {
            toast('Invalid Attempt : ', 'Select An Employee');
            return;
        }

        $('#formTitle').html('Edit Employee');

        getDetailsBtn.hide();
        updateUser.show();
        saveUser.hide();

        $.ajax({
            url: 'api/manage-user-details.php',
            data: {
                'username': selectedUser,
            },
            success: function(result) {
                var res = $.parseJSON(result);
                console.log(res);
                if (res.state == true) {
                    $('#username').hide();

                    var data = res.data;
                    $('#userNameText').html(data.username);
                    $('#username').val(data.username);
                    $('#comp_id').val(data.emp_name);
                    $('#designation').val(data.designation);
                    $('#dob').val(data.dob);
                    $('#doj').val(data.doj);

                    // $('#role').val(data.role);
                    $('#address').val(data.address);
                    $('#contact').val(data.contact);
                    $('#email').val(data.email);
                    $('#shift').val(data.shift);

                    let roles = data.role.split(',');

                    roles.forEach(r => {
                        $('#roles option[value=' + r + ']').attr('selected', 'selected');
                        $('#opRoles option[value=' + r + ']').attr('selected', 'selected');

                    });
                    toggleOpRoles();



                } else {
                    toast('Invalid Attempt : ', 'Server Error... Try Again Later');
                }

                getDetailsBtn.show();
            }
        });
    }

    function removeUser() {


        var getDetailsBtn = $('#getDetailsBtn');
        var removeUserBtn = $('#removeUserBtn');
        var addUserBtn = $('#addUserBtn');
        var selectedUser = $('#selectedUser').val();
        var updateUser = $('#updateUser');
        var saveUser = $('#saveUser');

        if (selectedUser == null || selectedUser == '') {
            toast('Invalid Attempt : ', 'Select An User');
            return;
        }

        if (!confirm('Remove employee ?')) {
            return;
        }
        getDetailsBtn.attr('disabled', 'disabled');
        removeUserBtn.attr('disabled', 'disabled');
        addUserBtn.attr('disabled', 'disabled');
        updateUser.attr('disabled', 'disabled');
        saveUser.attr('disabled', 'disabled');

        $.ajax({
            url: 'api/manage-user-details.php',
            method: 'POST',
            data: {
                'remove': true,
                'username': selectedUser,
            },
            success: function(result) {
                var res = $.parseJSON(result);
                if (res.state == true) {
                    successToast('Success : ', 'Removed Successfully');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                } else {
                    toast('Failed : ', 'Try again Later');
                }
            }
        });

    }

    function formatDate(date) {
        var d = new Date(date);
        var hh = d.getHours();
        var m = d.getMinutes();
        var s = d.getSeconds();
        var dd = 'AM';
        var h = hh;
        if (h >= 12) {
            h = hh - 12;
            dd = 'PM';
        }
        if (h == 0) {
            h = 12;
        }
        m = m < 10 ? '0' + m : m;

        s = s < 10 ? '0' + s : s;

        /* if you want 2 digit hours:
        h = h<10?'0'+h:h;
        */

        var pattern = new RegExp('0?' + hh + ':' + m + ':' + s);

        var replacement = h + ':' + m;
        /* if you want to add seconds
        replacement += ':'+s;
        */
        replacement += ' ' + dd;

        return date.replace(pattern, replacement);
    }
</script>

<script>
    function toggleOpRoles() {
        var foo = $('#roles').val();
        console.log(foo);
        if (foo.includes('3'))
            $('#opRoles').attr('disabled', false);
        else $('#opRoles').attr('disabled', true);
    }
</script>

</body>

</html>