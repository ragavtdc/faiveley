<?php

$rId = 2;
require '../../includes/init.php';

$res = array();

function addNewUser($db, $username, $comp_id, $designation, $dob, $doj, $role, $address, $contact, $email, $photo, $shift)
{

    $password = date_format(date_create($dob), 'd-m-Y');
    $stmt = $db->prepare("INSERT INTO user_master(username,password,role,emp_name,designation,dob,doj,address,contact,email,photo,shift)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");

    $stmt->bind_param('ssssssssssss', $username, $password, $role, $comp_id, $designation, $dob, $doj, $address, $contact, $email, $photo, $shift);

    if ($stmt->execute()) {
        $_SESSION['success'] = "USER ADDED SUCCESSFULLY";
    } else {
        $_SESSION['error'] = mysqli_error($db); //"Unable to add user try again later";
    }
    header('Location: ../');
}

function updateUser($db, $username, $comp_id, $designation, $dob, $doj, $role, $address, $contact, $email, $photo, $shift)
{

    $password = date_format(date_create($dob), 'd-m-Y');

    if ($photo == '**--p') {
        $stmt = $db->prepare("UPDATE user_master SET password=?,role=?,emp_name=?,designation=?,dob=?,doj=?,address=?,contact=?,email=?,shift=? WHERE username=?");
        $stmt->bind_param('sssssssssss', $password, $role, $comp_id, $designation, $dob, $doj, $address, $contact, $email, $shift, $username);
    } else {
        $stmt = $db->prepare("UPDATE user_master SET password=?,role=?,emp_name=?,designation=?,dob=?,doj=?,address=?,contact=?,email=?,photo=?,shift=? WHERE username=?");
        $stmt->bind_param('ssssssssssss', $password, $role, $comp_id, $designation, $dob, $doj, $address, $contact, $email, $photo, $shift, $username);
    }

    if ($stmt->execute()) {
        $_SESSION['success'] = "USER UPDATED SUCCESSFULLY";
    } else {
        $_SESSION['error'] = "Unable to update... try again later" . mysqli_error($db);
    }
    header('Location: ../');
}

extract($_POST);

$role = join(',', $roles);
if (in_array('3', $roles)) {
    $role .= ',' . join(',', $opRoles);
}
$db = db();

$file = explode('.', json_encode($_FILES['fileUpload']['name']));

$ext = explode('"', $file[count($file) - 1])[0];

$t = round(microtime(true) * 1000) . '.' . $ext;
$target_file = '../../../uploads/users/' . $t;
if (json_encode($_FILES['fileUpload']['name']) != null && json_encode($_FILES['fileUpload']['name'] != '') && strlen(json_encode($_FILES['fileUpload']['name']) > 0)) {
    if (move_uploaded_file($_FILES["fileUpload"]["tmp_name"], $target_file)) {
        if (isset($updateUser)) {
            updateUser($db, $username, $comp_id, $designation, $dob, $doj, $role, $address, $contact, $email, $t, $shift);
        } else {
            addNewUser($db, $username, $comp_id, $designation, $dob, $doj, $role, $address, $contact, $email, $t, $shift);
        }
    } else {
        $_SESSION['error'] = "Unable to upload file";
        header('Location: ../');
    }
} else {
    if (isset($updateUser)) {


        updateUser($db, $username, $comp_id, $designation, $dob, $doj, $role, $address, $contact, $email, '**--p', $shift);
    } else {
        addNewUser($db, $username, $comp_id, $designation, $dob, $doj, $role, $address, $contact, $email, null, $shift);
    }
}
