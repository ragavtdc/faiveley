<?php

$res = array();

function getUserDetails($db, $username)
{
    $stmt = $db->prepare("SELECT * FROM user_master WHERE username=? AND state=1 LIMIT 1");

    $stmt->bind_param('s', $username);

    if ($stmt->execute()) {
        $result = $stmt->get_result();
        $row = mysqli_fetch_assoc($result);
        $res = $row;
        complete($res);
    } else {
        err(['status' => 'failed']);
    }
}

function removeUserDetails($db, $username)
{
    $stmt = $db->prepare("UPDATE user_master SET state=0 WHERE username='$username'");

    if ($stmt->execute()) {
        complete(['status' => 'success']);
    } else {
        err(['status' => 'failed']);
    }
}
$rId = 2;
require '../../includes/init.php';

$db = db();

extract($_REQUEST);

if (isset($remove)) {
    removeUserDetails($db, $username);
} else {
    getUserDetails($db, $username);
}
