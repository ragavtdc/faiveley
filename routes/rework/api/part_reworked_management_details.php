<?php

$res = array();

function getPartDetails($db, $partcode)
{
    // $partcode="2966199002780000336000412259";
    $stmt = $db->prepare("SELECT * FROM a_rework_master WHERE reworked_part_serial_no=? ORDER by id DESC ");

    $stmt->bind_param('s', $partcode);

    $data = array();
    $stmt->execute();
    $result = $stmt->get_result();
    $count = $result->num_rows;
    if ($count === 0) err('Not found');
    else {
        $row = $result->fetch_assoc();
        $data['count'] = $count - 1;
        $data['row'] = $row;
        complete($data);
    }

    $stmt->close();
}

$rId = 23;
require '../../includes/init.php';

$db = db();

extract($_REQUEST);
getPartDetails($db, $partcode);
