<?php

$rId = 23;
require '../includes/init.php';

$db = db();

$res = array();

$stmt = $db->prepare('SELECT username FROM user_master WHERE state=1');

if ($stmt->execute()) {
    $result = $stmt->get_result();
    while ($rows = mysqli_fetch_assoc($result)) {
        $res[] = $rows;
    }
}
head();
?>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-users'></i> PCB Rework Management </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-product-hunt fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>PCB Rework Management</a></li>
    </ul>
</div>
<?php if (isset($_SESSION['error'])) {
?>
    <h1 class='tile red text-center text-white'>
        <?php echo $_SESSION['error'];
        unset($_SESSION['error']);
        ?>
    </h1>
<?php }
?>

<?php if (isset($_SESSION['success'])) {
?>
    <h1 class='tile green text-center text-white'>
        <?php echo $_SESSION['success'];
        unset($_SESSION['success']);
        ?>
    </h1>
<?php }
?>
<div class='container'>
    <div class='row tile'>
        <div class='form-group col-lg-8 col-md-8 col-sm-12'>
            <label class='control-label'></label>
            <input class='form-control' type='text' placeholder='Scan Part' name='partcode' id='partcode' required autofocus>

        </div>
        <div class='form-group col-lg-4 col-md-4 col-sm-12'>
            <label class='control-label'></label>
            <a id='getPartDetailsBtn' onclick='getPartDetails()' class='btn btn-purple btn-sm'>GET DETAILS</a>
        </div>

        <div class='form-group col-lg-12 col-md-12 col-sm-12 text-center'>
            <div id='progressDiv' style='display: none;' class='col-lg-2 offset-lg-5 col-md-2 offset-md-5 col-sm-12'>
                <div class='progress'>
                    <div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='75' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class='container'>
    <form action='api/new_reworked_part_entry.php' method='POST' enctype='multipart/form-data'>
        <div class='row tile'>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Part_number</label>
                <input class='form-control' type='text' placeholder='Enter part_Numer' name='part_number' id='part_number' required autofocus>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Part_Name</label>
                <input class='form-control' type='text' placeholder='Enter part_name' name='part_name' id='part_name' required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Part_Rework</label>
                <input class='form-control' type='text' placeholder='Enter part type' name='part_type' id='part_type' required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Reworked_No_of_Times</label>
                <input class='form-control' type='text' placeholder='Reworked_no_of_Times' name='Reworked' id='rework_no_of_times' required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Rework_Date</label>
                <input class='form-control' type='date' placeholder='Select Rework_date' name='rework_date' id='rework_date' required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>Rework_Comments</label>
                <input class='form-control' type='text' placeholder='Type your Comments' name='comments' id='comments' required>
            </div>

            <div class='form-group btn-container col-lg-12 col-md-12 col-sm-12 text-center'>
                <button id='savePart' name='savePart' class='btn btn-primary'><i class='fa fa-save'></i>Rework</button>
                <button id='updatePart' style='display: none;' name='updatePart' class='btn btn-primary'><i class='fa fa-save'></i>UPDATE</button>
            </div>
    </form>
</div>
<!-- </div> -->
<!-- </div> -->

<?php foot();
?>
<!-- Page specific javascripts-->
<!-- Bootstrap core CSS -->
<link href='../../js/mdb-css/bootstrap.min.css' rel='stylesheet'>
<!-- Material Design Bootstrap -->
<link href='../../js/mdb-css/mdb.min.css' rel='stylesheet'>
<!-- Your custom styles ( optional ) -->
<link href='../../css/style.css' rel='stylesheet'>
<!-- JQuery -->
<script type='text/javascript' src='../../js/mdb-js/mdb.min.js'></script>
<script type='text/javascript' src='../../js/mdb-js/addons/progressBar.js'></script>
<script type='text/javascript' src='../../js/mdb-js/addons/progressBar.min.js'></script>
<script src='../../js/plugins/plotly-latest.min.js' type='text/javascript'></script>
<script src='../../js/plugins/bootstrap-notify.min.js'></script>
<script src='../../js/toast.js'></script>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script>
    function getPartDetails() {

        var progressDiv = $('#progressDiv');
        var getDetailsBtn = $('#getDetailsBtn');
        var selectedPart = $('#partcode').val();
        var updateUser = $('#updatePart');
        var saveUser = $('#savePart');

        progressDiv.show();
        getDetailsBtn.hide();
        updateUser.show();
        saveUser.hide();

        $.ajax({
            url: 'api/part_reworked_management_details.php',
            data: {
                'partcode': selectedPart,
            },
            success: function(result) {
                console.log(result);

                var res = $.parseJSON(result);
                if (res.state == true) {
                    let row = res.data.row;
                    $('#part_number').val(row.reworked_part_serial_no);
                    $('#part_name').val(row.reworked_part_acc_name);
                    if (row.state == 0)
                        $('#part_type').val("Failure Part");
                    else
                        $('#part_type').val("Reworked part");
                    $('#rework_no_of_times').val(res.data.count);
                    $('#comments').val(row.comments);
                    $('#rework_date').val(row.reworked_date.split(' ')[0]);


                } else {
                    toast(res.err);
                }
                progressDiv.hide();
                getDetailsBtn.show();
            }
        });
    }
</script>
</body>

</html>