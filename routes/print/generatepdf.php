<?php
$rId = 12;
require '../includes/init.php';
require_once 'vendor/autoload.php';

function getPart($part)
{
    $db = db();
    $stmt = $db->prepare("SELECT * FROM part_master WHERE part_code_id = ? LIMIT 1");
    $stmt->bind_param('s', $part);
    $stmt->execute();
    $result = $stmt->get_result();
    $db->close();
    if (mysqli_num_rows($result) == 1) {
        return mysqli_fetch_assoc($result);
    }
    return null;
}

function getPartCodes($part)
{
    $db = db();
    $stmt = $db->prepare("SELECT accessories_part_name,accessories_quantity,accessories_part_no,accessories_part_code FROM part_accessories_master WHERE part_id = ? and state =1");
    $stmt->bind_param('s', $part);
    $stmt->execute();
    $result = $stmt->get_result();
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    $db->close();
    return $rows;
}
if (!isset($_GET['part-code'])) {
    header("location: index.php", true);
    die();
}
$row = getPart($_GET['part-code']);

if ($row == null) {
    die("report not found");
}

function dataImage($img)
{
    $type = '';
    switch (substr($img, 0, 1)) {
        case '/':
            $type = 'jpg';
            break;
        case 'i':
            $type = 'png';
            break;
        case 'R':
            $type = 'gif';
            break;
        default:
            $type = '*';
            break;
    }
    return 'data:image/' . $type . ';base64,' . $img;
}

$mpdf = new \Mpdf\Mpdf([
    'pagenumPrefix' => 'SHEET ', 'setAutoTopMargin' => 'stretch',
    'autoMarginPadding' => 0
]);
// $mpdf->showImageErrors = true;
$mpdf->SetHTMLHeader(' 

<table  width="100%" >

    <tr>
        <td colspan="3" rowspan="2">
            <img src="../../images/logo-wabtec.png" class="img-fluid" width="250px">
        </td>
        <td>
            PRODUCT
        </td>
        <td>' .
    $row['part_name']
    . '</td>
    </tr>
    <tr>
        <td>
            PART NO
        </td>
        <td>' .
    $row['part_number']
    . '</td>
    </tr>
    <tr>
        <td colspan="2">DATE</td>
        <td>' . viewDate($row['part_date']) . '</td>
        <td colspan="2" rowspan="2">
        <img src="' . dataImage(base64_encode($row['part_code'])) .  '" width="300px" alt="sample barcode">
        </td>
    </tr>
    <tr>
        <td colspan="2">DRAWN</td>
        <td>TAMAGNONE</td>
    </tr>
    <tr>
        <td colspan="2">APPROVED</td>
        <td>MURANZZANO</td>
        <td colspan="2"> {PAGENO} / {nbpg} </td>
    </tr>
    

</table>
');
$WriteHTML = '<body>
<style>
table,tr,th,td{
border: 1px solid black;
border-collapse: collapse;
text-align: center;
vertical-align: middle;
}
</style>
<div><table width="100%">
<thead>

<tr>
        <th width="1.5cm" >#</th>
        <th>QTY</th>
        <th>DESCRIPTION</th>
        <th>PART NO</th>
        <th>CODE</th>
    </tr>
</thead>
<tbody>

';


$rows = getPartCodes($row['id']);
foreach ($rows as $key => $row) {
    $WriteHTML .= "<tr>";
    $WriteHTML .= "<td>" . ($key + 1) . "</td>";
    $WriteHTML .= "<td>" . $row['accessories_quantity'] . "</td>";
    $WriteHTML .= "<td>" . $row['accessories_part_name'] . "</td>";
    $WriteHTML .= "<td>" . $row['accessories_part_no'] . "</td>";
    $WriteHTML .= "<td>";
    if ($row['accessories_part_code'] != null) {
        $WriteHTML .= "<img src='" . dataImage(base64_encode($row['accessories_part_code'])) . "'/>";
    }
    $WriteHTML .= "</td>";
    $WriteHTML .= "</tr>";
}

// for ($i = 0; $i < 8; $i++) {
//     $WriteHTML .= ('
//     <tr>
//     <td>1</td>
//     <td>1</td>
//     <td>RACK 84TE 3U G2S</td>
//     <td>FT0039310-100</td>
//     <td><img src="../../images/bs-2.jpg"  width="6.5cm" alt=""></td>
// </tr>

// ');
// }


$WriteHTML .= (' 
<tr>
    <td colspan="5" height="125px"></td>
</tr>
<tr>
    <td colspan="3" style="border: 0px;vertical-align: bottom;" height="125px">
        (QUALITY)
    </td>
    <td colspan="2" style="border: 0px;vertical-align: bottom;" height="125px">
        (PRODUCTION)
    </td>
</tr>
</tbody>

</table></div></body>');
$mpdf->writeHTML($WriteHTML);
$mpdf->Output();
