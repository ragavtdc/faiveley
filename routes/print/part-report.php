<?php
$rId = 11;
require '../includes/init.php';

extract($_GET);

function getReport()
{
    extract($_GET);
    if (isset($from_date) && !empty($from_date)) {
        $fromDate = $from_date;
    } else {

        $fromDate = "2020-06-04";
    }
    if (isset($to_date) && !empty($to_date)) {
        $toDate = $to_date;
    } else {
        $toDate = date("Y-m-d");
    }


    $db = db();
    $stmt = $db->prepare("SELECT
	pm.part_number,pm.part_name,
	(SELECT msm.main_stage_description FROM main_stage_master msm WHERE msm.id = pss.current_stage_no)AS main_stage,
	(SELECT ssm.sub_stage_description FROM sub_stage_master ssm WHERE ssm.sub_stage_no = pss.current_sub_stage_no)AS sub_stage,
	(CASE WHEN pss.last_status_condition=1 THEN 'Ongoing' ELSE 'Completed' END)AS status,
	pss.created_at 
    FROM part_stage_status pss INNER JOIN part_master pm ON pss.part_code_id = pm.id WHERE DATE(pss.created_at) BETWEEN DATE(?) AND DATE(?)");
    $stmt->bind_param('ss', $fromDate, $toDate);
    $stmt->execute();
    $result = $stmt->get_result();
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    $db->close();
    return $rows;
}

head();

?>

<style>
.barcode {
    font-family: 'Libre Barcode 39';
    font-size: 22px;
}

table,
th,
td {
    border: 1.5px solid black;
    border-collapse: collapse;
    text-align: center;
    vertical-align: middle;
}
</style>


<div class='app-title'>
    <div>
        <h1><i class='fa fa-users'></i> Print Management </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-home fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>Print Management</a></li>
    </ul>
</div>

<div class='container'>

    <form class="">
        <div class="row tile">
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>From Date</label>
                <input class='form-control' type='date' name='from_date' required
                    value="<?php echo isset($from_date) ?  $from_date : '' ?>" id="from_date">
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>To Date</label>
                <input class='form-control' type='date' name='to_date' required
                    value="<?php echo isset($to_date) ?  $to_date : '' ?>" id="to_date">
            </div>
            <div class='form-group col-sm-12 text-center'>
                <button type="submit" value="date" name="data" class="btn btn-purple mt-2">view</button>
            </div>
            <div class='form-group col-sm-12 text-center'>
                <?php
                if (count($_GET) > 0) {
                    echo '<a href="./part-report.php">View All</a>';
                }
                ?>
            </div>
            <!-- <div class='form-group col-lg-6 col-md-6 col-sm-12 mt-3 text-center'>
                        <form class="">
                            <label class='control-label'>From Date</label>
                            <input class='form-control' type='text' name='from-date' id="from-date" required> 
                            <button type="submit" value="all" name="data" class="btn btn-purple">View All</button>
                        </form>
                    </div> -->
        </div>
    </form>

    <?php

    // if (isset($_GET['data'])) {
    ?>


    <div class="row tile">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="sampleTable">
                            <thead>
                                <tr>
                                    <th>Part Number</th>
                                    <th>Part Name</th>
                                    <th>Main Stage</th>
                                    <th>Sub Stage</th>
                                    <th>Status</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                $rows = getReport();

                                foreach ($rows as $row) {
                                    echo '<tr>';
                                    echo '<td>' . $row['part_number'] . '</td>';
                                    echo '<td>' . $row['part_name'] . '</td>';
                                    echo '<td>' . $row['main_stage'] . '</td>';
                                    echo '<td>' . $row['sub_stage'] . '</td>';
                                    echo '<td>' . $row['status'] . '</td>';
                                    echo '<td>' . viewDateTime($row['created_at']) . '</td>';
                                    echo '</tr>';
                                }

                                ?>





                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php //} 
    ?>
</div>



<?php foot(); ?>

<!-- Page specific javascripts-->
<!-- Bootstrap core CSS -->
<link href='../../js/mdb-css/bootstrap.min.css' rel='stylesheet'>
<!-- Material Design Bootstrap -->
<link href='../../js/mdb-css/mdb.min.css' rel='stylesheet'>
<!-- Your custom styles ( optional ) -->
<link href='../../css/style.css' rel='stylesheet'>
<!-- JQuery -->
<!-- <script type='text/javascript' src='../../js/mdb-js/mdb.min.js'></script> -->
<!-- <script src='../../js/plugins/plotly-latest.min.js' type='text/javascript'></script> -->
<script type="text/javascript" src="../../js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/plugins/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
    $('#sampleTable').DataTable();
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
</body>

</html>