<?php

$rId = 12;

require '../includes/init.php';

function getPart($part)
{
    $db = db();
    // $part = "29554190027800003D00I1970";
    $stmt = $db->prepare("SELECT * FROM part_master WHERE part_code_id = ? LIMIT 1");
    $stmt->bind_param('s', $part);
    $stmt->execute();
    $result = $stmt->get_result();
    $db->close();
    if (mysqli_num_rows($result) == 1) {
        return mysqli_fetch_assoc($result);
    }
    return null;
}

function getPartCodes($part)
{
    $db = db();
    // $product = "29554190027800003D00I1970";
    // $stmt = $db->prepare("SELECT accessories_part_name,accessories_quantity,accessories_part_no,accessories_part_code FROM part_accessories_master WHERE part_id = IFNULL((SELECT id FROM part_master WHERE part_code_id = ? LIMIT 1),0)");
    $stmt = $db->prepare("SELECT accessories_part_name,accessories_quantity,accessories_part_no,accessories_part_code FROM part_accessories_master WHERE part_id = ?");
    $stmt->bind_param('s', $part);
    $stmt->execute();
    $result = $stmt->get_result();
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    $db->close();
    return $rows;
}

head();

?>

<style>
.barcode {
    font-family: 'Libre Barcode 39';
    font-size: 22px;
}

table,
th,
td {
    border: 1.5px solid black;
    border-collapse: collapse;
    text-align: center;
    vertical-align: middle;
}
</style>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-users'></i> Print Management </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-home fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>Print Management</a></li>
    </ul>
</div>

<div class='container'>



    <form>
        <div class="row tile">
            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                <label class='control-label'>QR Scan</label>
                <input class='form-control' type='text' placeholder='Scanned Part Code' name='part-code' id="barcode"
                    value="29554190027800003D00I1970" required>
            </div>
            <div class='form-group col-lg-6 col-md-6 col-sm-12 mt-3 text-center'>
                <button type="submit" class="btn btn-purple btn-lg pl-4 pr-4"><i class="fa fa-qrcode"
                        style="font-size: 30px;"></i></button>
                <!-- <button type="submit" value="GET DATA" class="btn btn-purple btn-block ">GET DATA</button> -->
            </div>
        </div>
    </form>
    <?php if (isset($_GET['part-code'])) {
    ?>


    <div class='row tile'>
        <div class="col-6">
            <h3 class='tile-title'>Report</h3>
        </div>

        <?php $row = getPart($_GET['part-code']);
            if ($row != null) {
                echo '<div class="col-6"><a href="./generatepdf.php?part-code=' . $_GET['part-code'] . '" target="_blank" class="btn btn-info float-right ">Print</a>
                        </div><div class="col-12">';
                echo '<table width="100%">
                                        <thead>';
                echo '<tr>
                                        <td colspan="3" rowspan="2">
                                            <img src="../../images/logo-wabtec.png" class="img-fluid" width="250px">
                                        </td>
                                        <td>
                                            PRODUCT
                                        </td>
                                        <td>' .
                    $row['part_name']
                    . '</td>
                                    </tr>';
                echo '<tr>
                                        <td>
                                            PART NO
                                        </td>
                                        <td>' .
                    $row['part_number']
                    . '</td>
                                    </tr>';
                echo '<tr>
                                        <td colspan="2">DATE</td>
                                        <td>' . viewDate($row['part_date']) . '</td>
                                        <td colspan="2" rowspan="2">
                                            <img src="data:image/jpeg;base64,' . base64_encode($row['part_code']) .  '"class="img-fluid" width="300px" alt="sample barcode">
                                        </td>
                                    </tr>';
                echo '<tr>
                                        <td colspan="2">DRAWN</td>
                                        <td>TAMAGNONE</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">APPROVED</td>
                                        <td>MURANZZANO</td>
                                        <td colspan="2">(SHEET NO)</td>
                                    </tr>';
                echo '<tr>
                                            <th>#</th>
                                            <th>QTY</th>
                                            <th>DESCRIPTION</th>
                                            <th>PART NO</th>
                                            <th>CODE</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            } else {
                echo "<br><br><br><p class='center-text float-none tile-body'>Part not found!</p>";
            }

            ?>
        <?php

            if ($row != null) {
                $rows = getPartCodes($row['id']);
                foreach ($rows as $key => $row) {
                    echo "<tr>";
                    echo "<td>" . ($key + 1) . "</td>";
                    echo "<td>" . $row['accessories_quantity'] . "</td>";
                    echo "<td>" . $row['accessories_part_name'] . "</td>";
                    echo "<td>" . $row['accessories_part_no'] . "</td>";
                    echo "<td>";
                    if ($row['accessories_part_code'] != null) {
                        echo "<img src='data:image/jpeg;base64," . base64_encode($row['accessories_part_code']) . "'/>";
                    }
                    echo "</td>";
                    echo "</tr>";
                }
                echo '<tr>
                                <td colspan="5" height="125px"></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="border: 0px;vertical-align: bottom;" height="125px">
                                    ( QUALITY )
                                </td>
                                <td colspan="2" style="border: 0px;vertical-align: bottom;" height="125px">
                                    ( PRODUCTION )
                                </td>
                            </tr>
                            </tbody>
    
                            </table></div>';
            }
            ?>

    </div>
    <?php
    } ?>
</div>



<?php foot(); ?>
<!-- Page specific javascripts-->
<!-- Bootstrap core CSS -->
<link href='../../js/mdb-css/bootstrap.min.css' rel='stylesheet'>
<!-- Material Design Bootstrap -->
<link href='../../js/mdb-css/mdb.min.css' rel='stylesheet'>
<!-- Your custom styles ( optional ) -->
<link href='../../css/style.css' rel='stylesheet'>
<!-- JQuery -->
<script type='text/javascript' src='../../js/mdb-js/mdb.min.js'></script>
<!-- <script src='../../js/plugins/plotly-latest.min.js' type='text/javascript'></script> -->
<script type="text/javascript" src="../../js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/plugins/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
    $('#sampleTable').DataTable();
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
</body>

</html>