<?php

$rId = 6;
require '../../includes/init.php';

$db = db();

extract($_POST);

check('partId', 'part id required');

$result = mysqli_query($db, "SELECT * FROM a_accessories_master where part_id = $partId and state = 1");

$pcb = array();

while ($row = mysqli_fetch_assoc($result)) {
    $pcb[] = $row;
}

$data['pcb'] = $pcb;

$result = mysqli_query($db, "SELECT * FROM a_accessories_mechanical_master where part_id = $partId and state = 1");

$mec = array();

while ($row = mysqli_fetch_assoc($result)) {
    $mec[] = $row;
}

$data['mec'] = $mec;

mysqli_close($db);

complete($data);
