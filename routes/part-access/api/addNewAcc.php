<?php

$rId = 6;
require '../../includes/init.php';

$db = db();

extract($_POST);

check('partId', 'part id required');
check('newAcc', 'require accessory name');
check('type', 'Accessory type required');
check('newAccPart', 'Accessory type required');


if ($type == '1') {
    $table = "a_accessories_master";
} elseif ($type == '2') {
    $table = "a_accessories_mechanical_master";
} else {
    err('unknown type');
}

if (mysqli_query($db, "INSERT INTO $table (acc_name,part_id,acc_part_no) values('$newAcc',$partId,'$newAccPart')")) {
    mysqli_close($db);
    complete(true);
}
err('Unable to create!' . mysqli_error($db));
mysqli_close($db);
