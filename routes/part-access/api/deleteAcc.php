<?php

$rId = 6;
require '../../includes/init.php';

$db = db();

extract($_POST);

check('accId', 'Accessory id required');
check('type', 'Accessory type required');


if ($type == '1') {
    $table = "a_accessories_master";
} elseif ($type == '2') {
    $table = "a_accessories_mechanical_master";
} else {
    err('unknown type');
}

if (mysqli_query($db, "UPDATE $table SET state = 0 where acc_id = $accId")) {
    mysqli_close($db);
    complete(true);
}
mysqli_close($db);
err('Unable to delete!');
