<?php

$rId = 6;
require '../includes/init.php';

function getParts(): array
{
    $db = db();
    $rows = array();
    $result = mysqli_query($db, "SELECT * FROM a_part_master where state = 1");
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    mysqli_close($db);
    return $rows;
}

head();
?>
<div class='app-title'>
    <div>
        <h1><i class='fa fa-users'></i> Part Accessories Management </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-product-hunt fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>Part Accessories Management</a></li>
    </ul>
</div>

<div class='container'>
    <div class='row tile'>
        <div class='form-group col-md-6 col-sm-12 text-center pt-1 '>
            <h5>Choose Part</h5>
        </div>
        <div class='form-group col-md-6 col-sm-12'>
            <select class='form-control' onchange="fetchAcc()" id='selectedPart' name='selectedPart'>
                <option disabled selected>Select Part</option>

                <?php

                $parts = getParts();

                foreach ($parts as $part) {
                    echo ' <option value="' . $part['part_id'] . '">' . $part['part_name'] . '</option>';
                }

                ?>

            </select>
        </div>
        <div class=' col-sm-12 text-center'>
            <div id='progressDiv' style='display: none;'>
                <div class='progress'>
                    <div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='75' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container tile" id="accDiv" style="display: none;">
    <h5>Board Accessories</h5>
    <ul class="list-group" id="accList">

    </ul>
    <div class="row mt-3">
        <div class='form-group  col-md-6 col-sm-12'>
            <input class='form-control' type='text' placeholder='Add PCB Accessories' id='newAcc'>
        </div>
        <div class='form-group  col-md-6 col-sm-12'>
            <input class='form-control' type='text' placeholder='Enter Part Number' id='newAccPart'>
        </div>
        <div class='form-group text-center offset-md-4 col-md-4 col-sm-12'>
            <label class='control-label'></label>
            <button id='getPartDetailsBtn' onclick="addNewAcc(1,$('#newAcc').val().trim(),$('#newAccPart').val().trim())" class='btn btn-primary text-white'>Add</button>
        </div>
    </div>
</div>

<div class="container tile" id="accDivM" style="display: none;">
    <h5>Mechanical Accessories</h5>

    <ul class="list-group" id="accListM">

    </ul>
    <div class="row mt-3">
        <div class='form-group  col-md-6 col-sm-12'>
            <input class='form-control' type='text' placeholder='Add Mechanical Accessories' id='newAccM'>
        </div>
        <div class='form-group  col-md-6 col-sm-12'>
            <input class='form-control' type='text' placeholder='Enter Part Number' id='newAccPartM'>
        </div>
        <div class='form-group text-center offset-md-4 col-md-4 col-sm-12'>
            <label class='control-label'></label>
            <button id='getPartDetailsBtnM' onclick="addNewAcc(2,$('#newAccM').val().trim(),$('#newAccPartM').val().trim())" class='btn btn-primary text-white'>Add</button>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Accessory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="mForm">
                    <div class='form-group'>
                        <label class='control-label'>Accessory Name &nbsp;</label>
                        <input class='form-control' type="text" name="accName" id="mAccName" required>
                    </div>
                    <div class='form-group'>
                        <label class='control-label'>Accessory Part No</label>
                        <input class='form-control' type="text" name="accPartNo" id="mAccPartNo" required>
                    </div>
                    <input type="hidden" name="accId" id="mAccId">
                    <input type="hidden" name="accType" id="mAccType">
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary ">Update</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<?php foot();
?>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    let selectedPartId;
    var progress = $('#progressDiv');
    var accDiv = $('#accDiv');
    var accDivM = $('#accDivM');

    function fetchAcc() {
        selectedPartId = $('#selectedPart').val();
        accDiv.hide();
        accDivM.hide();
        progress.show();
        var data = {
            'partId': selectedPartId
        }
        var func = (data) => {
            console.log(data);
            progress.hide();
            accDiv.show();
            let html = '';
            data.pcb.forEach(acc => {
                html += '<li class="list-group-item d-flex justify-content-between align-items-center">';
                html += '<div>';
                html += '<p class="mb-0">';
                html += acc.acc_name;
                html += '</p><p class="mb-0"> ';
                html += acc.acc_part_no;
                html += '</p>';
                html += '</div>';
                html += '<div>';
                html += '<button  data-toggle="modal" data-target="#basicExampleModal" title="Edit Accessory Part No" class="btn btn-flat btn-sm p-1 mx-2" onclick="editAcc(1,' + acc.acc_id + ',\'' + acc.acc_name + '\',\'' + acc.acc_part_no + '\')"><i class="material-icons red" style="font-size: 20px">edit</i></button>';
                html += '<button class="btn btn-flat btn-sm p-1 " onclick="deleteAcc(1,' + acc.acc_id + ')"><i class="material-icons red">delete</i>  </button></div>';
                html += '</li>';
            });
            $('#accList').html(html);

            accDivM.show();
            let htmlM = '';
            data.mec.forEach(acc => {
                htmlM += '<li class="list-group-item d-flex justify-content-between align-items-center">';
                htmlM += '<div>';
                htmlM += '<p class="mb-0">';
                htmlM += acc.acc_name;
                htmlM += '</p><p class="mb-1"> ';
                htmlM += (acc.acc_part_no == '' ? '[ ]' : acc.acc_part_no);
                htmlM += '</p>';
                htmlM += '</div>';
                htmlM += '<div>';
                htmlM += '<button  data-toggle="modal" data-target="#basicExampleModal" title="Edit Accessory Part No" class="btn btn-flat btn-sm p-1 mx-2" onclick="editAcc(2,' + acc.acc_id + ',\'' + acc.acc_name + '\',\'' + acc.acc_part_no + '\')"><i class="material-icons red" style="font-size: 20px">edit</i></button>';
                htmlM += '<button class="btn btn-flat btn-sm p-1 " onclick="deleteAcc(2,' + acc.acc_id + ')"><i class="material-icons red">delete</i>  </button></div>';
                htmlM += '</li>';
            });
            $('#accListM').html(htmlM);
            $('[data-toggle="tooltip"]').tooltip();

        };
        ajax('api/fetchAcc.php', data, func);
    }

    function addNewAcc(type, newAcc, newAccPart) {

        if (newAcc === '') {
            toast('', 'Name Field empty!');
            return;
        }
        if (newAccPart === '') {
            toast('', 'Part No Field empty!');
            return;
        }
        if (selectedPartId === undefined) {
            toast('', 'Part not selected!');
            return;
        }
        progress.show();
        var data = {
            'type': type,
            'partId': selectedPartId,
            'newAcc': newAcc,
            'newAccPart': newAccPart,
        }

        var func = (data) => {
            progress.hide();
            console.log(data);
            successToast('added successfully');
            $('#newAcc').val('');
            $('#newAccM').val('');
            $('#newAccPart').val('');
            $('#newAccPartM').val('');
            setTimeout(fetchAcc, 500);
        };

        ajax('api/addNewAcc.php', data, func);
    }

    function deleteAcc(type, accId) {

        if (!confirm('Delete accessory ?')) {
            return;
        }
        progress.show();

        var data = {
            'type': type,
            'accId': accId,
        }

        var func = (data) => {
            progress.hide();
            console.log(data);
            successToast('Deleted successfully');
            setTimeout(() => {
                fetchAcc();
            }, 500);
        };

        ajax('api/deleteAcc.php', data, func);
    }


    function editAcc(type, accId, accName, accPartNo) {
        $('#mAccType').val(type);
        $('#mAccId').val(accId);
        $('#mAccName').val(accName);
        $('#mAccPartNo').val(accPartNo);
    }

    let mForm = $('#mForm');

    mForm.submit((e) => {
        e.preventDefault();
        $('#basicExampleModal').modal('hide');
        var func = (data) => {
            if (data === true) {
                console.log(data);
                successToast('Updated successfully');
                setTimeout(() => {
                    fetchAcc();
                }, 500);
            }
        };
        ajax('api/update.php', mForm.serialize(), func);
    });
</script>