<?php

$rId = 27;
require '../../includes/init.php';

check('part_id', 'Part ID is required');

$data = array();

function getUnitFailedStages($db, $part_id)
{
    $result = mysqli_query($db, "SELECT msm.main_stage_description as stage,su.created_at FROM a_stage_updation su INNER JOIN main_stage_master msm ON msm.main_stage_no=su.stage WHERE part_id = $part_id AND STATUS=0");

    $stages = array();

    while ($r = mysqli_fetch_assoc($result)) {
        $r['created_at'] = date_format(date_create($r['created_at']), 'd-M-yy h:i a');
        $stages[] = $r;
    }
    return $stages;
}

function getAccsFailedStages($db, $parent)
{
    $result = mysqli_query($db, "SELECT pas.id,pas.serial_no,pas.serial_alpha,pas.type,msm.main_stage_description AS failed_stage,am.acc_name,am.acc_part_no,pas.updated_by,pas.updated_at FROM a_part_accessories_staging pas INNER JOIN a_accessories_master am ON am.acc_id = pas.type INNER JOIN main_stage_master msm ON msm.main_stage_no = pas.failed_stage WHERE pas.parent=$parent AND pas.testing=0");

    $accsStages = array();

    while ($r = mysqli_fetch_assoc($result)) {
        $r['updated_at'] = date_format(date_create($r['updated_at']), 'd-M-yy h:i a');
        $accsStages[] = $r;
    }
    return $accsStages;
}

extract($_REQUEST);

$db = db();

$data['failedStage'] = getUnitFailedStages($db, $part_id);

$data['failedAccs'] = getAccsFailedStages($db, $part_id);

complete($data);
