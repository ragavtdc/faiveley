<?php

$rId = 27;
require '../../includes/init.php';

check('year', 'Year is required');

$data = array();

function getTotal($db, $year)
{
    $result = mysqli_query($db, "SELECT COUNT(*) FROM a_stage_updation WHERE YEAR(created_at) = $year GROUP BY part_id");

    $rowCount = mysqli_num_rows($result);

    return $rowCount;
}

function getFailure($db, $year)
{
    $result = mysqli_query($db, "SELECT COUNT(*) FROM a_stage_updation WHERE YEAR(created_at) = $year AND STATUS=0 GROUP BY part_id");

    $rowCount = mysqli_num_rows($result);

    return $rowCount;
}
function getMonthWise($db, $year)
{
    $data = array();
    $result = mysqli_query($db, "SELECT su.part_id,pas.serial_no,pas.serial_alpha,pas.created_at,pm.part_name,MONTHNAME(pas.created_at) AS mon FROM a_stage_updation su INNER JOIN a_part_accessories_staging pas ON pas.id=su.part_id INNER JOIN a_part_master pm ON pm.part_id = pas.type WHERE YEAR(su.created_at) = $year AND su.status=0 GROUP BY su.part_id");

    while ($r = mysqli_fetch_assoc($result)) {
        $r['created_at'] = date_format(date_create($r['created_at']), 'd-M-yy');
        $data[$r['mon']][] = $r;
    }
    return $data;
}

extract($_REQUEST);

$db = db();

$res = array();

$res['total'] = getTotal($db, $year);

$res['fail'] = getFailure($db, $year);

$data['count'] = $res;

$data['parts'] = getMonthWise($db, $year);

complete($data);
