const yearForm = $('#yearForm');
const countTable = $('#countTable');
const accordionEx = $('#accordionEx');
const failureBody = $('#failureBody');
const failureHeader = $('#failureHeader');

yearForm.on('submit', (e) => {
    e.preventDefault();
    const func = (data) => {
        setCount(data.count);
        setMonthParts(data.parts);
    };
    ajax('api/fetch.php', yearForm.serialize(), func);
});

function setCount(count) {
    let html = '<tr>';
    html += '<td>' + count.total + '</td>'
    html += '<td>' + (count.total - count.fail) + '</td>'
    html += '<td>' + count.fail + '</td>'
    countTable.html(html);
}

let allParts;
function setMonthParts(part) {
    allParts = part;
    let html = '';
    Object.keys(part).forEach((k) => {
        const parts = part[k];
        html += '<div class="tile">';
        html += '<div class="tile-title mb-0">';
        html += '<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne' + k + '" aria-expanded="false" class="collapsed">';
        html += '<h5 class="mb-0">' + k + '<span class="float-right">' + parts.length + '</span> </h5>';
        html += '</a>';
        html += '</div>';
        html += '<div id="collapseOne' + k + '" class="tile-body pt-3 collapse" data-parent="#accordionEx">';
        html += setPart(parts);
        html += '</div>';
        html += '</div>';
    });
    accordionEx.html(html);
}


function setPart(parts) {

    let html = '';
    html += '<table class="table table-bordered table-sm table-responsive-lg table-fixed">';

    html += '<thead>';
    html += '<tr>';
    html += '<td>Name</td>';
    html += '<td>Serial Alpha</td>';
    html += '<td>Serial No</td>';
    html += '<td>Created At</td>';
    html += '<td>Stages</td>';
    html += '</tr>';
    html += '</thead>';
    html += '<tbody>';

    parts.forEach((p) => {
        html += '<tr>';
        html += '<td>' + p.part_name + '</td>';
        html += '<td class="text-center"><p>' + p.serial_alpha + '</p>' + '</td>';
        html += '<td class="text-break">' + p.serial_no + '</td>';
        html += '<td>' + p.created_at + '</td>';
        html += '<td><a href="#" data-toggle="modal" onclick="loadFailureView(' + p.part_id + ',)" data-target="#failureViewModal" class="btn btn-info">View</a></td>';
        html += '</tr>';
    });
    html += '</tbody>';

    html += '</table>';

    return html;
}

function loadFailureView(partId) {
    let part;
    Object.keys(allParts).forEach((k) => {
        if (part == undefined) {
            part = allParts[k].find(p => {
                return p.part_id == partId;
            });
        }
    });
    failureHeader.html('');
    failureBody.html('');
    const func = (data) => {
        failureHeader.html(part.part_name + '<br><code>' + part.serial_no + '</code>');
        let html = 'Failed Stages<ul class="list-group">';
        data.failedStage.forEach(i => {
            html += '<li class="list-group-item">' + i.stage + '<span class="float-right">' + i.created_at + '</span></li>';
        });
        html += '';
        html += '</ul><br>';
        html += 'Failed Accessories<ul class="list-group">';
        data.failedAccs.forEach(i => {
            html += '<li class="list-group-item d-flex justify-content-between align-items-center"><div><p class="mb-0">' + i.acc_name + '</p><p class="mb-0">' + i.serial_no + '</p></div><div><p class="mb-0">' + i.failed_stage + '</p><p class="mb-0">' + i.updated_at + '</p></div></li>';
        });
        html += '';
        html += '</ul>';
        failureBody.html(html);
    };
    ajax('api/failures.php', { part_id: partId }, func);
}