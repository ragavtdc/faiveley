<?php
$rId = 27;
require '../includes/init.php';
head();
$db = db();
$result = mysqli_query($db, "SELECT YEAR(created_at)AS ye FROM a_stage_updation GROUP BY YEAR(created_at)");

?>

<form id="yearForm">
    <div class="row ">
        <div class="col-sm-12 col-md-4"></div>
        <div class=' col-sm-6 col-md-3'>
            <div class="form-group">
                <select class="form-control" name="year" id="">
                    <?php
                    while ($row = mysqli_fetch_assoc($result)) {
                        $ye = $row['ye'];
                        echo "<option value='$ye'>$ye</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class='col-sm-4 col-md-3 center'>
            <input type="submit" class='btn btn-info d-block text-white' value="View">
        </div>
    </div>
</form>

<div class="row">
    <div class="col-sm-12 col-md-3"></div>
    <div class="col-sm-12 col-md-6 mt-2">
        <table class="table tile table-sm table-bordered text-center">
            <thead>
                <tr>
                    <th>Total Parts</th>
                    <th>Success Parts</th>
                    <th>Failure Parts</th>
                </tr>
            </thead>
            <tbody id="countTable">

            </tbody>
        </table>
    </div>
</div>

<div class="accordion md-accordion mt-3 mx-2" id="accordionEx" aria-multiselectable="true">

</div>

<div class="modal fade" id="failureViewModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="failureHeader">STAGE SELECTION<br>
                    <code id="failureSubTitle">STAGE SELECTION</code></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="failureBody">
                <ul class="list-group">
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">
                        <ul class="list-group">
                            <li class="list-group-item">Cras justo odio</li>
                            <li class="list-group-item">Dapibus ac facilisis in</li>
                            <li class="list-group-item">Morbi leo risus</li>
                            <li class="list-group-item">Porta ac consectetur ac</li>
                            <li class="list-group-item">Vestibulum at eros</li>
                        </ul>
                    </li>
                    <li class="list-group-item">Porta ac consectetur ac</li>
                    <li class="list-group-item">Vestibulum at eros</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php foot();
?>

<script src="script.js"></script>