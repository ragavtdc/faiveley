<?php
$rId = 1;
require '../../includes/init.php';

$db = db();

$data = array();
$result = array();

if (!isset($_REQUEST['fromDate']) && !isset($_REQUEST['toDate'])) {
    die('Insufficient parameters');
}
extract($_REQUEST);
$queryTotal = "SELECT * FROM a_stage_updation su INNER JOIN a_part_accessories_staging pas ON pas.id = su.part_id
                WHERE DATE(su.created_at) >= '$fromDate' AND DATE(su.created_at) <= '$toDate' 
                AND su.status = 1 GROUP BY su.part_id ORDER BY su.id DESC";

$res = mysqli_query($db, $queryTotal);

$data['status'] = '';
$data['error'] = 'No records';

while ($row = mysqli_fetch_assoc($res)) {

    $stageId = $row['part_id'];
    $queryGetStage = "SELECT *, DATE(created_at) AS created_date FROM a_stage_updation WHERE part_id = '$stageId' AND STATUS = 1 GROUP BY stage";

    $res1 = mysqli_query($db, $queryGetStage);
    $result1 = array();
    while ($row1 = mysqli_fetch_assoc($res1)) {
        $result1[] = $row1;
    }
    $row['stageDetails'] = $result1;

    $result[] = $row;
    $data['status'] = 'ok';
    $data['error'] = '';
}

$data['result'] = $result;

echo json_encode($data);

mysqli_close($db);