function dashBoardData() {
    var prodCode = document.getElementById('prodCode');
    $.ajax({
        type: 'post',
        url: 'apis/dashboardData.php',
        data: {
            'partCode': prodCode.value
        },
        dataType: 'json',
        success: function(data) {
            if (data.status == 'ok') {
                $('#partNameArea').html(
                    '<ul><li>PART NAME : ' + data.result.part_name + '</li><li>PART NO :  ' +
                    data.result.part_number + '</li></ul>'
                );
                //Referring main satge one
                if (data.result.main_stage_no == '1') {
                    $('#stageTwoTile').removeClass('animated pulse infinite');
                    $('#stageThreeTile').removeClass('animated pulse infinite');
                    $('#stageFourTile').removeClass('animated pulse infinite');
                    $('#stageOneTile').addClass('animated pulse infinite');
                    $('#stageOneTile').css('box-shadow', '10px 10px 5px #e69900');
                    $('#stageTwoTile').css('box-shadow', 'none');
                    $('#stageThreeTile').css('box-shadow', 'none');
                    $('#stageFourTile').css('box-shadow', 'none');
                    $('#rmv').removeClass('col-sm-2');
                    $('#rmv').addClass('col-sm-1');

                    //Referring sub stage one
                    if (data.result.sub_stage_no == '1') {
                        $('#btnStageOneSubTwo').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageTwoSubOne').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageTwoSubTwo').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageThreeSubOne').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageThreeSubTwo').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageThreeSubThree').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageThreeSubFour').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageThreeSubFive').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageFourSubOne').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        if (data.result.status == '0') {
                            $('#btnStageOneSubOne').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#btnStageOneSubOne').html(
                                '<button class="btn sunny-morning-gradient btn-sm btnState">ongoing</button>'
                            );
                        } else if (data.result.status == '2') {
                            $('#btnStageOneSubOne').html(
                                '<button class="btn blue-gradient btn-sm btnState">completed</button>');
                            // $('.clsComments').html(data.result1.failure_comments[9]);
                        }
                    }
                    //Referring sub stage two
                    if (data.result.sub_stage_no == '2') {
                        $('#btnStageOneSubOne').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        $('#btnStageTwoSubOne').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageTwoSubTwo').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageThreeSubOne').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageThreeSubTwo').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageThreeSubThree').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageThreeSubFour').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageThreeSubFive').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );
                        $('#btnStageFourSubOne').html(
                            '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                        );

                        if (data.result.status == '0') {
                            $('#btnStageOneSubTwo').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#btnStageOneSubTwo').html(
                                '<button class="btn sunny-morning-gradient btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#btnStageOneSubTwo').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        }
                    }
                }
                //Referring main stage two
                if (data.result.main_stage_no == '2') {
                    $('#stageOneTile').removeClass('animated pulse infinite');
                    $('#stageThreeTile').removeClass('animated pulse infinite');
                    $('#stageFourTile').removeClass('animated pulse infinite');
                    $('#stageTwoTile').addClass('animated pulse infinite');
                    $('#stageOneTile').css('box-shadow', '10px 10px 5px #888');
                    $('#stageTwoTile').css('box-shadow', '10px 10px 5px #e69900');
                    $('#stageThreeTile').css('box-shadow', 'none');
                    $('#stageFourTile').css('box-shadow', 'none');
                    $('#rmv').removeClass('col-sm-2');
                    $('#rmv').addClass('col-sm-1');

                    $('#btnStageOneSubOne').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageOneSubTwo').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageTwoSubTwo').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageThreeSubOne').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageThreeSubTwo').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageThreeSubThree').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageThreeSubFour').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageThreeSubFive').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageFourSubOne').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );

                    //Referring sub stage one
                    if (data.result.sub_stage_no == '3') {
                        if (data.result.status == '0') {
                            $('#btnStageTwoSubOne').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#btnStageTwoSubOne').html(
                                '<button class="btn sunny-morning-gradient btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#btnStageTwoSubOne').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        }
                    }
                    //Referring sub stage two
                    if (data.result.sub_stage_no == '4') {
                        $('#btnStageTwoSubOne').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>');
                        if (data.result.status == '0') {
                            $('#btnStageTwoSubTwo').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#btnStageTwoSubTwo').html(
                                '<button class="btn sunny-morning-gradient btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#btnStageTwoSubTwo').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        }
                    }
                }

                //Referring main stage three
                if (data.result.main_stage_no == '3') {
                    $('#stageTwoTile').removeClass('animated pulse infinite');
                    $('#stageOneTile').removeClass('animated pulse infinite');
                    $('#stageFourTile').removeClass('animated pulse infinite');
                    $('#stageThreeTile').addClass('animated pulse infinite');
                    $('#stageOneTile').css('box-shadow', '10px 10px 5px #888');
                    $('#stageTwoTile').css('box-shadow', '10px 10px 5px #888');
                    $('#stageThreeTile').css('box-shadow', '10px 10px 5px #e69900');
                    $('#stageFourTile').css('box-shadow', 'none');

                    $('#btnStageOneSubOne').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageOneSubTwo').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageTwoSubOne').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageTwoSubTwo').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubOne').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageThreeSubTwo').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageThreeSubThree').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageThreeSubFour').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageThreeSubFive').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );
                    $('#btnStageFourSubOne').html(
                        '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                    );

                    //Referring sub stage one
                    if (data.result.sub_stage_no == '5') {
                        if (data.result.status == '0') {
                            $('#btnStageThreeSubOne').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#btnStageThreeSubOne').html(
                                '<button class="btn sunny-morning-gradient btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#btnStageThreeSubOne').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        }
                    }

                    //Referring sub stage two
                    if (data.result.sub_stage_no == '6') {
                        $('#btnStageThreeSubOne').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        $('#rmv').removeClass('col-sm-2');
                        $('#rmv').addClass('col-sm-1');
                        if (data.result.status == '0') {
                            $('#btnStageThreeSubTwo').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#btnStageThreeSubTwo').html(
                                '<button class="btn sunny-morning-gradient btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#btnStageThreeSubTwo').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        }
                    }
                    //Referring sub stage three
                    if (data.result.sub_stage_no == '7') {
                        $('#btnStageThreeSubOne').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        $('#btnStageThreeSubTwo').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        if (data.result.status == '0') {
                            $('#btnStageThreeSubThree').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#btnStageThreeSubThree').html(
                                '<button class="btn sunny-morning-gradient btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#btnStageThreeSubThree').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        }
                    }
                    //Referring sub stage four
                    if (data.result.sub_stage_no == '8') {
                        $('#btnStageThreeSubOne').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        $('#btnStageThreeSubTwo').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        $('#btnStageThreeSubThree').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        if (data.result.status == '0') {
                            $('#btnStageThreeSubFour').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#btnStageThreeSubFour').html(
                                '<button class="btn sunny-morning-gradient btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#btnStageThreeSubFour').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        }
                    }

                    //Referring sub stage five
                    if (data.result.sub_stage_no == '9') {
                        $('#btnStageThreeSubOne').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        $('#btnStageThreeSubTwo').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        $('#btnStageThreeSubThree').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        $('#btnStageThreeSubFour').html(
                            '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                        );
                        if (data.result.status == '0') {
                            $('#btnStageThreeSubFive').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#btnStageThreeSubFive').html(
                                '<button class="btn sunny-morning-gradient btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#btnStageThreeSubFive').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        }
                    }
                }

                //Referring main stage four
                if (data.result.main_stage_no == '4') {
                    $('#stageTwoTile').removeClass('animated infinite pulse');
                    $('#stageThreeTile').removeClass('animated infinite pulse');
                    $('#stageOneTile').removeClass('animated pulse infinite');
                    $('#stageFourTile').addClass('animated infinite pulse');
                    $('#stageOneTile').css('box-shadow', '10px 10px 5px #888');
                    $('#stageTwoTile').css('box-shadow', '10px 10px 5px #888');
                    $('#stageThreeTile').css('box-shadow', '10px 10px 5px #888');
                    $('#stageFourTile').css('box-shadow', '10px 10px 5px #e69900');

                    $('#rmv').removeClass('col-sm-2');
                    $('#rmv').addClass('col-sm-1');

                    $('#btnStageOneSubOne').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageOneSubTwo').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageTwoSubOne').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageTwoSubTwo').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubOne').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubTwo').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubThree').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubFour').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubFive').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );

                    //Referring sub stage one
                    if (data.result.sub_stage_no == '10') {
                        if (data.result.status == '0') {
                            $('#btnStageFourSubOne').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#btnStageFourSubOne').html(
                                '<button class="btn sunny-morning-gradient btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#btnStageFourSubOne').html(
                                '<button class="btn to-start-gradient btn-sm btnState">to_start</button>'
                            );
                        }
                    }
                }

                //Referring main stage five
                if (data.result.main_stage_no == '5' && data.result.sub_stage_no == '11') {
                    $('#stageTwoTile').removeClass('animated infinite pulse');
                    $('#stageThreeTile').removeClass('animated infinite pulse');
                    $('#stageOneTile').removeClass('animated pulse infinite');
                    $('#stageFourTile').removeClass('animated pulse infinite');
                    $('#stageOneTile').css('box-shadow', '10px 10px 5px #888');
                    $('#stageTwoTile').css('box-shadow', '10px 10px 5px #888');
                    $('#stageThreeTile').css('box-shadow', '10px 10px 5px #888');
                    $('#stageFourTile').css('box-shadow', '10px 10px 5px #888');

                    $('#btnStageOneSubOne').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageOneSubTwo').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageTwoSubOne').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageTwoSubTwo').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubOne').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubTwo').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubThree').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubFour').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageThreeSubFive').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                    $('#btnStageFourSubOne').html(
                        '<button class="btn blue-gradient btn-sm btnState">completed</button>'
                    );
                }

                $('#com11').html('');
                $('#com12').html('');
                $('#com23').html('');
                $('#com24').html('');
                $('#com35').html('');
                $('#com36').html('');
                $('#com37').html('');
                $('#com38').html('');
                $('#com39').html('');
                $('#com41').html('');

                for (i = 0; i < data.result1.length; i++) {

                    //commments for main 1 sub 1
                    if (data.result1[i].current_stage_no == 1 && data.result1[i].current_sub_stage_no == 1) {
                        if (data.result1[i].failure == 0) {
                            $('#com11').html(data.result1[i].comments);
                        } else if (data.result1[i].failure == 1) {
                            $('#com11').html('<span class="failAlrt">.</span>&nbsp;' + data.result1[i].failure_comments);
                        } else {
                            $('#com11').html('');
                        }
                    }

                    //comments for main 1 sub 2
                    if (data.result1[i].current_stage_no == 1 && data.result1[i].current_sub_stage_no == 2) {
                        if (data.result1[i].failure == 0) {
                            $('#com12').html(data.result1[i].comments);
                        } else if (data.result1[i].failure == 1) {
                            $('#com12').html('<span class="failAlrt">.</span>&nbsp;' + data.result1[i].failure_comments);
                        } else {
                            $('#com12').html('');
                        }
                    }

                    //commments for main 2 sub 3
                    if (data.result1[i].current_stage_no == 2 && data.result1[i].current_sub_stage_no == 3) {
                        if (data.result1[i].failure == 0) {
                            $('#com23').html(data.result1[i].comments);
                        } else if (data.result1[i].failure == 1) {
                            $('#com23').html('<span class="failAlrt">.</span>&nbsp;' + data.result1[i].failure_comments);
                        } else {
                            $('#com23').html('');
                        }
                    }

                    //commments for main 2 sub 4
                    if (data.result1[i].current_stage_no == 2 && data.result1[i].current_sub_stage_no == 4) {
                        if (data.result1[i].failure == 0) {
                            $('#com24').html(data.result1[i].comments);
                        } else if (data.result1[i].failure == 1) {
                            $('#com24').html('<span class="failAlrt">.</span>&nbsp;' + data.result1[i].failure_comments);
                        } else {
                            $('#com24').html('');
                        }
                    }

                    //commments for main 3 sub 5
                    if (data.result1[i].current_stage_no == 3 && data.result1[i].current_sub_stage_no == 5) {
                        if (data.result1[i].failure == 0) {
                            $('#com35').html(data.result1[i].comments);
                        } else if (data.result1[i].failure == 1) {
                            $('#com35').html('<span class="failAlrt">.</span>&nbsp;' + data.result1[i].failure_comments);
                        } else {
                            $('#com35').html('');
                        }
                    }

                    //commments for main 3 sub 6
                    if (data.result1[i].current_stage_no == 3 && data.result1[i].current_sub_stage_no == 6) {
                        if (data.result1[i].failure == 0) {
                            $('#com36').html(data.result1[i].comments);
                        } else if (data.result1[i].failure == 1) {
                            $('#com36').html('<span class="failAlrt">.</span>&nbsp;' + data.result1[i].failure_comments);
                        } else {
                            $('#com36').html('');
                        }
                    }

                    //commments for main 3 sub 7
                    if (data.result1[i].current_stage_no == 3 && data.result1[i].current_sub_stage_no == 7) {
                        if (data.result1[i].failure == 0) {
                            $('#com37').html(data.result1[i].comments);
                        } else if (data.result1[i].failure == 1) {
                            $('#com37').html('<span class="failAlrt">.</span>&nbsp;' + data.result1[i].failure_comments);
                        } else {
                            $('#com37').html('');
                        }
                    }

                    //commments for main 3 sub 8
                    if (data.result1[i].current_stage_no == 3 && data.result1[i].current_sub_stage_no == 8) {
                        if (data.result1[i].failure == 0) {
                            $('#com38').html(data.result1[i].comments);
                        } else if (data.result1[i].failure == 1) {
                            $('#com38').html('<span class="failAlrt">.</span>&nbsp;' + data.result1[i].failure_comments);
                        } else {
                            $('#com38').html('');
                        }
                    }

                    //commments for main 3 sub 9
                    if (data.result1[i].current_stage_no == 3 && data.result1[i].current_sub_stage_no == 9) {
                        if (data.result1[i].failure == 0) {
                            $('#com39').html(data.result1[i].comments);
                        } else if (data.result1[i].failure == 1) {
                            $('#com39').html('<span class="failAlrt">.</span>&nbsp;' + data.result1[i].failure_comments);
                        } else {
                            $('#com39').html('');
                        }
                    }

                    //commments for main 4 sub 10
                    if (data.result1[i].current_stage_no == 4 && data.result1[i].current_sub_stage_no == 10) {
                        if (data.result1[i].failure == 0) {
                            $('#com41').html(data.result1[i].comments);
                        } else if (data.result1[i].failure == 1) {
                            $('#com41').html('<span class="failAlrt">.</span>&nbsp;' + data.result1[i].failure_comments);
                        } else {
                            $('#com41').html('');
                        }
                    }

                }

            } else {
                prodCode.value = '';
                prodCode.focus();
                $.notify({
                    title: ' Unknown : ',
                    message: data.result.error,
                    icon: 'fa fa-exclamation-triangle'
                }, {
                    type: 'danger'
                });
            }
        }
    });
}