<?php
$rId = 1;
require '../includes/init.php';
$db = db();
head();
?>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-dashboard'></i> Dashboard </h1>
    </div>
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-home fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>Dashboard</a></li>
    </ul>
</div>

<div class='row align-items-center'>
    <!-- <div class='col-md-1'></div> -->
    <div class='col-md-6'>
        <div class='input-group'>
            <input type='text' class='form-control' placeholder='Part code' name='productId' id='prodCode'
                autocomplete='off' required autofocus>
            <div class='input-group-append'>
                <button class='input-group-text' id="chkPart">Check</button>
            </div>
        </div>
    </div>

    <div class='col-sm-3'>
        <div class="md-form">
            <input placeholder="Select date" type="date" id="singleDatepicker" class="form-control datepicker"
                data-date="" onchange="">
        </div>
    </div>
    <div class='col-sm-3'>
        <div class="md-form">
            <input placeholder="Select date" type="date" id="secondDatepicker" class="form-control datepicker"
                data-date="">
        </div>
    </div>
</div>

<br>

<!-- Dashboard card values starts here -->

<div id="tableDashboard"></div>

<?php foot(); ?>

<!-- Page specific javascripts-->
<!-- Bootstrap core CSS -->
<link href='../../js/mdb-css/bootstrap.min.css' rel='stylesheet'>
<!-- Material Design Bootstrap -->
<link href='../../js/mdb-css/mdb.min.css' rel='stylesheet'>
<script type='text/javascript' src='../../js/mdb-js/mdb.min.js'></script>
<script src='../../js/plugins/plotly-latest.min.js' type='text/javascript'></script>
<script src='../../js/plugins/bootstrap-notify.min.js'></script>
<script src='../../js/plugins/bootstrap-datepicker.min.js'></script>
<script type='text/javascript' src='../../js/plugins/sweetalert.min.js'></script>
<script type="text/javascript" src="../../js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/plugins/dataTables.bootstrap.min.js"></script>
<!-- <script type='text/javascript' src='dashboardData.js'></script> -->
<script type="text/javascript" src="dash.js"></script>
<!-- <script type="text/javascript" src="dashDatePick.js"></script> -->
<link href='../../css/style.css' rel='stylesheet'>

<script>
$(document).ready(function() {
    $('#chkPart').click(function(){
        
    });
    $('[data-toggle="tooltip"]').tooltip();

    // onLoadDashBoard();
    datepickChange();

    $('#singleDatepicker').on('change', function() {
        var toDate = document.getElementById('secondDatepicker').value;
        datepickChange(this.value, toDate);
    });

    $('#secondDatepicker').on('change', function() {
        var fromDate = document.getElementById('singleDatepicker').value;
        datepickChange(fromDate, this.value);
    });
});
</script>
</body>

</html>