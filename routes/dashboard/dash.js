var date = new Date();
var dd = String(date.getDate()).padStart(2, '0');
var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = date.getFullYear();

var today = yyyy + '-' + mm + '-' + dd;

var prodCode = document.getElementById('prodCode');

var selDate = '';
if (sDate = undefined) {
    selDate = today;
} else {
    // selDate = today;
}

// function onLoadDashBoard() {
//     $.ajax({
//         url: 'apis/dashGetByToday.php',
//         data: {},
//         statusCode: {
//             404: function() {
//                 alert('Page not found');
//             }
//         },
//         dataType: 'json',
//         success: function(data) {
//             loadToday(data);
//         }
//     });
// }

function datepickChange(fromDate, toDate) {

    if (fromDate == '' || toDate == '' || fromDate == undefined || toDate == undefined) {
        fromDate = today;
        toDate = today;
    }
    if (toDate < fromDate) {
        $.notify({
            title: ' Invalid : ',
            message: 'Select date in correct format',
            icon: 'fa fa-exclamation-triangle'
        }, {
            type: 'danger'
        });
        return
    }

    $.ajax({
        url: 'apis/dashGetByDate.php',
        data: {
            fromDate: fromDate,
            toDate: toDate
        },
        statusCode: {
            404: function() {
                alert('Page not found');
            }
        },
        dataType: 'json',
        success: function(data) {
            prodCode.value = '';
            prodCode.focus();
            ajaxResult(data);
        }
    });
}

$('#chkPart').on('click', function() {
    $.ajax({
        type: 'post',
        url: 'apis/dashGetBySerialNo.php',
        data: {
            'partCode': prodCode.value
        },
        statusCode: {
            404: function() {
                alert('Page not found');
            }
        },
        dataType: 'json',
        success: function(data) {
            ajaxResult(data);
        }
    });
});

function ajaxResult(data) {
    var sno = 0;
    var output = '';
    var headCell = '';
    var stage = '';
    var table = '';
    var header = "<table id='dtBasicTable' class='table table-bordered table-striped white' cellspacing='0' width='100%'>" +
        "<thead><tr><th class='th-sm'>SNO</th><th class='th-sm'>SERIAL NO</th><th class='th-sm'>PCB ASSEMBLY STAGE</th>" +
        "<th class='th-sm'>HV & IR TESTING</th><th class='th-sm'>FUNCTIONAL TESTING</th><th class='th-sm'>ESS TESTING" +
        "</th><th class='th-sm'>APPLICATION SOFTWARE LOADING</th><th class='th-sm'>PRE-DISPATCH INSPECTION</th>" +
        "<th class='th-sm'>QUALITY CHECKING</th></tr></thead><tbody>";
    var footer = "</tbody><tfoot><tr><th>SNO</th><th>SERIAL NO</th><th>PCB ASSEMBLY STAGE</th><th>HV & IR TESTING</th>" +
        "<th>FUNCTIONAL TESTING</th><th>ESS TESTING</th><th>APPLICATION SOFTWARE LOADING</th><th>PRE-DISPATCH INSPECTION</th>" +
        "<th>QUALITY CHECKING</th></tr></tfoot></table>";

    for (var i = 0; i < data.result.length; i++) {
        sno++;
        headCell = '';
        stage = '';
        headCell += "<td><div>" + sno + "</div></td>" +
            "<td><div>" + data.result[i].serial_no + "</div></td>";

        c1 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c2 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c3 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c4 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c5 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c6 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c7 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";

        for (var j = 0; j < data.result[i].stageDetails.length; j++) {

            // stage 1
            if (data.result[i].stageDetails[j].stage == 1) {
                c1 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                    data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
            }
            // stage 2
            if (data.result[i].stageDetails[j].stage == 2) {
                c2 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                    data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
            }
            // stage 3
            if (data.result[i].stageDetails[j].stage == 3) {
                c3 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                    data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
            }
            // stage 4
            if (data.result[i].stageDetails[j].stage == 4) {
                c4 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                    data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
            }
            // stage 5
            if (data.result[i].stageDetails[j].stage == 5) {
                c5 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                    data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
            }
            // stage 6
            if (data.result[i].stageDetails[j].stage == 6) {
                c6 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                    data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
            }
            // stage 7
            if (data.result[i].stageDetails[j].stage == 7) {
                c7 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                    data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
            }
        }
        stage = "<tr>" + headCell + c1 + c2 + c3 + c4 + c5 + c6 + c7 + "</tr>";
        output += stage;
    }

    table = header + output + footer;
    sno++;

    $('#tableDashboard').html('');
    $('#tableDashboard').html(table);

    $('th').addClass('text-center');
    $('td').addClass('text-center');

    $("#dtBasicTable").DataTable();
    $('.dataTables_length').addClass('bs-select');
}


function loadToday(data) {
    var sno = 0;
    var output = '';
    var headCell = '';
    var stage = '';
    var table = '';
    var header = "<table id='dtBasicTable' class='table table-bordered table-striped white' cellspacing='0' width='100%'>" +
        "<thead><tr><th class='th-sm'>SNO</th><th class='th-sm'>SERIAL NO</th><th class='th-sm'>PCB ASSEMBLY STAGE</th>" +
        "<th class='th-sm'>HV & IR TESTING</th><th class='th-sm'>FUNCTIONAL TESTING</th><th class='th-sm'>ESS TESTING" +
        "</th><th class='th-sm'>APPLICATION SOFTWARE LOADING</th><th class='th-sm'>PRE-DISPATCH INSPECTION</th>" +
        "<th class='th-sm'>QUALITY CHECKING</th></tr></thead><tbody>";
    var footer = "</tbody><tfoot><tr><th>SNO</th><th>SERIAL NO</th><th>PCB ASSEMBLY STAGE</th><th>HV & IR TESTING</th>" +
        "<th>FUNCTIONAL TESTING</th><th>ESS TESTING</th><th>APPLICATION SOFTWARE LOADING</th><th>PRE-DISPATCH INSPECTION</th>" +
        "<th>QUALITY CHECKING</th></tr></tfoot></table>";

    for (var i = 0; i < data.result.length; i++) {
        sno++;
        headCell = '';
        stage = '';
        sno++;
        headCell += "<td><div>" + sno + "</div></td>" +
            "<td><div>" + data.result[i].serial_no + "</div></td>";

        c1 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c2 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c3 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c4 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c5 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c6 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";
        c7 = "<td><div class='badge grey'><i class='fa fa-hourglass-start'></i></div><div><span>-</span></div></td>";

        for (var j = 0; j < data.result[i].stageDetails.length; j++) {

            // stage 1
            if (data.result[i].stageDetails[j].stage == 1) {
                if (data.result[i].stageDetails[j].created_date < today) {
                    c1 = "<td><div class='badge green lighten-3'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                } else {
                    c1 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                }
            }
            // stage 2
            if (data.result[i].stageDetails[j].stage == 2) {
                if (data.result[i].stageDetails[j].created_date < today) {
                    c2 = "<td><div class='badge green lighten-3'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                } else {
                    c2 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                }
            }
            // stage 3
            if (data.result[i].stageDetails[j].stage == 3) {
                if (data.result[i].stageDetails[j].created_date < today) {
                    c3 = "<td><div class='badge green lighten-3'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                } else {
                    c3 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                }
            }
            // stage 4
            if (data.result[i].stageDetails[j].stage == 4) {
                if (data.result[i].stageDetails[j].created_date < today) {
                    c4 = "<td><div class='badge green lighten-3'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                } else {
                    c4 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                }
            }
            // stage 5
            if (data.result[i].stageDetails[j].stage == 5) {
                if (data.result[i].stageDetails[j].created_date < today) {
                    c5 = "<td><div class='badge green lighten-3'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                } else {
                    c5 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                }
            }
            // stage 6
            if (data.result[i].stageDetails[j].stage == 6) {
                if (data.result[i].stageDetails[j].created_date < today) {
                    c6 = "<td><div class='badge green lighten-3'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                } else {
                    c6 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                }
            }
            // stage 7
            if (data.result[i].stageDetails[j].stage == 7) {
                if (data.result[i].stageDetails[j].created_date < today) {
                    c7 = "<td><div class='badge green lighten-3'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                } else {
                    c7 = "<td><div class='badge green'><i class='fa fa-check'></i></div><div><span>" +
                        data.result[i].stageDetails[j].created_by.toUpperCase() + "</span></div></td>";
                }
            }
        }
        stage = "<tr>" + headCell + c1 + c2 + c3 + c4 + c5 + c6 + c7 + "</tr>";
        output += stage;
      
    }

    table = header + output + footer;
    sno++;

    $('#tableDashboard').html('');
    $('#tableDashboard').html(table);

    $('th').addClass('text-center');
    $('td').addClass('text-center');

    $("#dtBasicTable").DataTable();
    $('.dataTables_length').addClass('bs-select');
}