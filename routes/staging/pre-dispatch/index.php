<?php

$rId = 20;
require '../../includes/init.php';

$db = db();

head();
?>

<style>
    table,
    th,
    td {
        border: 1.5px solid black;
        border-collapse: collapse;
        text-align: center;
        vertical-align: middle;
    }
</style>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-database'></i> Pre-dispatch Inspection Staging </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-database fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>Pre-dispatch Inspection Staging</a></li>
    </ul>
</div>
<div class="container">

    <div class="row tile">
        <div class='form-group col-lg-6 col-md-6 col-sm-12'>
            <!-- <label class='control-label'>Barcode Scan</label> -->
            <!-- <input class='form-control' type='text' placeholder='Scan Barcode' name='serial' id="serial" required autofocus onkeyup="if (event.keyCode === 13) {fetchAcc();}"> -->
            <input class='form-control' type='text' placeholder='Scan Barcode' name='serial' id="serial" required autofocus onkeyup="if (event.keyCode === 13) {getCheckList();}">
        </div>
        <div class="form-group col-lg-6 col-md-6 col-sm-12">
            <!-- <button id="getInfoBtn" onclick="fetchAcc()" class="btn waves-effect waves-light btn-sm btn-info" data-toggle="tooltip" data-original-title="Get Unit Info">Get Unit Info</button> -->
            <button id="getInfoBtn" onclick="getCheckList()" class="btn waves-effect waves-light btn-sm btn-info" data-toggle="tooltip" data-original-title="Get Unit Info">Get Unit Info</button>
        </div>
    </div>
</div>

<div class=' col-sm-12 text-center'>
    <div id='progressDiv' style='display: none;'>
        <div class='progress'>
            <div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='75' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div>
        </div>
    </div>
</div>

<div class="container tile" id="accDiv" style="display: none;">
    <!-- <ul class="list-group" id="accList">

    </ul> -->

    <div class="row" style="display:none">
        <div class="form-group col-8 text-center">
            Checked by

            <input type="text" class="form-control" placeholder="Checked by" id="cb" value="Dinesh">
        </div>
        <div class="form-group col-4 text-center">
            <br>
            <!-- <button id="getInfoBtn" onclick="fetchAcc()" class="btn waves-effect waves-light btn-sm btn-info" data-toggle="tooltip" data-original-title="Get Unit Info">Get Unit Info</button> -->
            <button id="printBtn" onclick="printPDF()" class="btn waves-effect waves-light btn-md btn-primary" data-toggle="tooltip" data-original-title="Print the Checklist">PRINT</button>
        </div>
    </div>

    <form id=tempFrm>
        <table width="100%">
            <thead>
                <tr class="text-center">
                    <td colspan="10" style="font-weight: bold;font-size: 24px;">
                        PRE DISPATCH INSPECTION CHECK LIST
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        Part Name
                    </td>
                    <td style="text-align: start;" colspan="4" id="partName">

                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        Description
                    </td>
                    <td style="text-align: start;" colspan="4" id="desc">

                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        Date
                    </td>
                    <td style="text-align: start;" colspan="4" id="curdate">

                    </td>
                </tr>
                <tr>
                    <td>
                        SL No.
                    </td>
                    <td>
                        Description
                    </td>
                    <td>
                        FPD
                    </td>
                    <!-- <td>
                        QAD
                    </td> -->
                    <td>
                        Remarks
                    </td>
                </tr>
            </thead>
            <tbody id="checklistBody">

            </tbody>
        </table>
    </form>

    <div class="text-center mt-4">
        <button class="btn waves-effect waves-light btn-sm ml-3 btn-danger" data-toggle="tooltip" data-original-title="Mark as Failed" id="failBtn" onclick="updateStage(0)"><i class="material-icons text-danger"></i>SAVE STAGE AS FAILED</button>
        <button class="btn waves-effect waves-light btn-sm ml-3 btn-success" data-toggle="tooltip" data-original-title="Mark as Passed" id="passBtn" onclick="updateStage(1)"><i class="material-icons text-success"></i>SAVE STAGE AS PASSED</button>
    </div>
</div>

<?php foot();
?>

<link href='<?php echo $path ?>js/mdb-css/bootstrap.min.css' rel='stylesheet'>

<script type='text/javascript' src='<?php echo $path ?>js/mdb-js/addons/progressBar.js'></script>
<script type='text/javascript' src='<?php echo $path ?>js/mdb-js/addons/progressBar.min.js'></script>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    let selectedPartVal;
    var progress = $('#progressDiv');
    var accDiv = $('#accDiv');

    // function fetchAcc() {
    //     selectedPartVal = $('#serial').val();
    //     accDiv.hide();
    //     progress.show();
    //     var data = {
    //         'partId': selectedPartVal
    //     }
    //     var func = (data) => {
    //         progress.hide();
    //         accDiv.show();
    //         console.log(data);
    //         let html = '';
    //         html += '<li class="list-group-item d-flex justify-content-between align-items-center">' + data.unit.part_name + '</li>';

    //         data.acc.forEach(ac => {
    //             html += '<li id="list' + ac.acc_id + '" class="list-group-item d-flex justify-content-between align-items-center">';
    //             html += ac.serial_no + ' - ' + ac.acc_name;
    //             html += '<div><button class="btn btn-flat btn-sm p-1 waves-effect waves-light" onclick="markAsFailed(' + ac.id + ',' + ac.acc_id + ')">Mark As Failure</button></div>';
    //             html += '</li>';
    //         });
    //         $('#accList').html(html);

    //     };
    //     ajax('api/fetchAcc.php', data, func);
    // }

    // function updateStage(status) {
    //     progress.show();
    //     selectedPartVal = $('#serial').val();
    //     let data = {
    //         'status': status,
    //         'serial': selectedPartVal
    //     }
    //     let func = (data) => {
    //         if (data) {
    //             successToast('Stage Updated');
    //             progress.hide();
    //             setTimeout(() => {
    //                 window.location.reload();
    //             }, 1500);
    //         } else {
    //             toast("Try again later...");
    //             progress.hide();
    //         }
    //     }
    //     ajax('api/updateStage.php', data, func);
    // }

    let ids = new Array;

    function printPDF() {

        $('select').removeAttr('disabled');
        $('input[name="remarks[]"]').removeAttr('disabled');
        selectedPartVal = $('#serial').val();
        // let data = new FormData;
        let te = $('#tempFrm').serializeArray();
        // data = te;
        // data.push({
        //     name: 'serial',
        //     value: selectedPartVal
        // });
        // data.push({
        //     name: 'ids',
        //     value: ids
        // });
        // data.push({
        //     name: 'partName',
        //     value: partName
        // });
        // data.push({
        //     name: 'partDesc',
        //     value: partDesc
        // });



        te = JSON.stringify(te);

        checks = JSON.stringify(checklists);

        // $('select').attr('disabled', 'true');
        // $('input[name="remarks[]"]').attr('disabled', 'true');

        window.open('generatePDF.php?serial=' + selectedPartVal + '&ids=' + ids + '&partName=' + partName + '&partDesc=' + partDesc + '&fd=' + te + '&checklists=' + checks + '&cb=' + $('#cb').val());

    }

    let partDesc;
    let partName;

    function getCheckList() {
        selectedPartVal = $('#serial').val();
        $('#printBtn').hide();
        $('select').removeAttr('disabled');
        accDiv.hide();
        progress.show();

        let data = {
            'partId': selectedPartVal
        }

        let func = (data) => {
            if (data.status == 1) {
                $('#failBtn').hide();
                $('#passBtn').hide();
                // $('#printBtn').show();

                // $('select').attr('disabled');
            }
            progress.hide();
            accDiv.show();
            console.log(data);
            partDesc = data.unit.part_description;
            partName = data.unit.part_name;
            checklists = data.cm;
            $('#desc').html(data.unit.part_description);
            $('#partName').html(data.unit.part_name);
            $('#curdate').html(new Date().toDateString());
            let html = '';

            // <td><select name="qad[]" class="form-control"><option value="1">Yes</option><option value="0">No</option></select></td>

            let count = 0;

            data.cm.forEach(c => {
                let selected1 = '';
                let selected0 = '';
                let disabled = '';
                let remarks = '';
                if (data.cv.length != 0) {
                    v = data.cv[count];
                    if (v.fpd == '1') {
                        selected1 = 'selected';
                        // disabled = ' disabled '
                    } else {
                        selected0 = 'selected';
                        // disabled = ' disabled ';
                    }
                    remarks = v.remarks;
                }
                ids.push(c.id);
                html += '<tr id="' + c.id + '">';
                html += '<td>' + c.id + '</td><td style="text-align:start">' + c.checklist + '</td><td><select ' + disabled + ' name="fpd[]" class="form-control"><option ' + selected1 + ' value="1">Yes</option><option ' + selected0 + ' value="0">No</option></select></td><td><input class="form-control" type="text" placeholder="If Any..." name="remarks[]" value="' + remarks + '"></td>';
                html += '</tr>';
                count++;
            });
            html += '<tr><td height="130px" colspan="3"><table width="100%"><tr><td height="130px" style="text-align:center;vertical-align:bottom;border:none !important"><?php echo $user['username'] ?><br>(Tested by)</td><td style="text-align:center;vertical-align:bottom">(Checked by)</td></tr></table></td><td style="text-align:start;">Rev. 0</td></tr>';

            $('#checklistBody').html(html);

            // if (data.status == 1)
            //     $('input[name="remarks[]"]').attr('disabled', 'true');
        }
        ajax('api/fetchChecklist.php', data, func);
    }

    let checklists;

    function updateStage(status) {
        progress.show();
        selectedPartVal = $('#serial').val();
        let data = new FormData;
        let te = $('#tempFrm').serializeArray();
        data = te;
        data.push({
            name: 'status',
            value: status
        });
        data.push({
            name: 'serial',
            value: selectedPartVal
        });
        data.push({
            name: 'ids',
            value: ids
        });
        let func = (data) => {
            if (data) {
                successToast('Stage Updated');
                progress.hide();
                setTimeout(() => {
                    window.location.reload();
                }, 1500);
            } else {
                toast("Try again later...");
                progress.hide();
            }
        }
        ajax('api/updateStage.php', data, func);
    }

    // function markAsFailed(id, accId) {
    //     progress.show();
    //     let data = {
    //         'id': id,
    //         'accId': accId
    //     }
    //     let func = (data) => {
    //         if (data) {
    //             successToast('Marked As Failure');
    //             $('#passBtn').hide();
    //             $('#list' + accId).removeClass('d-flex');
    //             $('#list' + accId).hide();
    //             progress.hide();
    //         } else {
    //             toast('Try Again Later...');
    //             progress.hide();
    //         }
    //     }
    //     ajax('api/markFailure.php', data, func);
    // }
</script>
</body>

</html>