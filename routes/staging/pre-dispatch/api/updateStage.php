<?php

$rId = 20;
require '../../../includes/init.php';

$db = db();

extract($_POST);

check('status', 'Status is required');
check('serial', 'Serial Number is required');

$result = mysqli_query($db, "SELECT id FROM a_part_accessories_staging WHERE serial_no = '$serial' AND parent=0 ORDER BY created_at DESC LIMIT 1");

$row = mysqli_fetch_assoc($result);

$partId = $row['id'];

// $createdBy = $user['username'];

// $stmt = $db->prepare("INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES(?,6,?,?)");

// $stmt->bind_param('sss', $partId, $status, $createdBy);

// if (!$stmt->execute()) {
//     err(mysqli_error($db));
// }

$userBy = $user['username'];

if (mysqli_query($db, "UPDATE a_stage_updation SET status='$status',updated_by='$userBy' WHERE part_id='$partId' AND stage=6 AND status = -1")) {
    $nextStage = $status == '1' ? 7 : 1;
    if (mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES('$partId','$nextStage',-1,'$userBy')")) {
        // mysqli_close($db);
        // complete(true);
    }
}
// err('Unable to update');

$query = '';

$id = explode(',', $ids);

for ($i = 0; $i < count($id); $i++) {
    $query .= "INSERT INTO a_checklist_log(part_id,checklist_id,fpd,remarks)VALUES($partId,$id[$i],$fpd[$i],'$remarks[$i]');";
}

mysqli_multi_query($db, $query);

complete(true);
