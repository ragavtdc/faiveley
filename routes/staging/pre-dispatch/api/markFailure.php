<?php

$rId = 20;
require '../../../includes/init.php';

$db = db();

extract($_POST);

check('id', 'ID is required');
check('accId', 'Account ID is required');

// $result = mysqli_query($db, "SELECT id FROM a_part_accessories_staging WHERE serial_no = '$serial' AND parent=0 ORDER BY created_at DESC LIMIT 1");

// $row = mysqli_fetch_assoc($result);

// $partId = $row['id'];

$updatedBy = $user['username'];

$stmt = $db->prepare("UPDATE a_part_accessories_staging SET testing=0,updated_by=? WHERE id=? AND type=?");

$stmt->bind_param('sss', $updatedBy, $id, $accId);

if (!$stmt->execute()) {
    err(mysqli_error($db));
}

complete(true);
