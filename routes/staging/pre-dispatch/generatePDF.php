<?php
$rId = 20;
require '../../includes/init.php';
require_once '../../print/vendor/autoload.php';

// if (!isset($_GET['partId'])) {
//     header("location: index.php", true);
//     die();
// }

// $table = base64_decode(urldecode($_GET['token']));

extract($_GET);

$fd = json_decode($fd);
$checklists = json_decode($checklists);

// echo json_encode($_GET);
// die();

$mpdf = new \Mpdf\Mpdf();
// $mpdf->showImageErrors = true;
$mpdf->SetHTMLHeader(' 
');
$WriteHTML = '
<style>
    table,
    th,
    td {
        border: 1px solid grey;
        border-collapse: collapse;
        text-align: center;
        vertical-align: middle;
    }
</style>
<header style="text-align:center; margin-bottom:20px">
        <img height="100" src="../../../images/logo-wabtec.png" />
</header>
<table width="100%">
<thead>
    <tr class="text-center">
        <td colspan="10" style="font-weight: bold;font-size: 24px;">
            PRE DISPATCH INSPECTION CHECK LIST
        </td>
    </tr>
    <tr>
        <td colspan="1">
            Serial
        </td>
        <td style="text-align: start;" colspan="4" id="partName">
        ' . $serial . '
        </td>
    </tr>
    <tr>
        <td colspan="1">
            Part Name
        </td>
        <td style="text-align: start;" colspan="4" id="partName">
        ' . $partName . '
        </td>
    </tr>
    <tr>
        <td colspan="1">
            Description
        </td>
        <td style="text-align: start;" colspan="4" id="desc">
        ' . $partDesc . '
        </td>
    </tr>
    <tr>
        <td colspan="1">
            Date
        </td>
        <td style="text-align: start;" colspan="4" id="curdate">
        ' . date("d-m-Y") . '
        </td>
    </tr>
    <tr>
        <td>
            SL No.
        </td>
        <td>
            Description
        </td>
        <td>
            FPD
        </td>
        <td>
            Remarks
        </td>
    </tr>
</thead>
<tbody id="checklistBody">';
$id = explode(',', $ids);

// <td>
//             QAD
//         </td>

$fpds = array();
// $qads = array();
$remarkss = array();

$cl = array();

foreach ($fd as $key => $value) {
    foreach ($value as $k => $v) {
        if ($v == 'fpd[]') {
            $fpds[] = $value;
        } elseif ($v == 'qad[]') {
            // $qads[] = $value;
        } elseif ($v == 'remarks[]') {
            $remarkss[] = $value;
        }
    }
}

$f = array();
// $q=array();
$r = array();

foreach ($fpds as $value) {
    foreach ($value as $k => $v) {
        if ($k == 'value') {
            $f[] = $v == '1' ? 'Yes' : 'No';
        }
    }
}
// foreach ($qads as $value) {
//     foreach ($value as $k => $v) {
//         if ($k=='value') {
//             $q[]= $v=='1' ? 'Yes' : 'No';
//         }
//     }
// }
foreach ($remarkss as $value) {
    foreach ($value as $k => $v) {
        if ($k == 'value') {
            $r[] = $v;
        }
    }
}

foreach ($checklists as $key => $value) {
    foreach ($value as $k => $v) {
        if ($k == 'checklist') {
            $cl[] = $v;
        }
    }
}

// echo json_encode($_GET);
// echo json_encode($cl);
// die();

for ($i = 0; $i < count($id); $i++) {
    $WriteHTML .= '<tr>';
    $WriteHTML .= '<td>' . $id[$i] . '</td>';
    $WriteHTML .= '<td style="text-align:start">' . $cl[$i] . '</td>';
    $WriteHTML .= '<td>' . $f[$i] . '</td><td>' . $r[$i] . '</td>';
    $WriteHTML .= '</tr>';
}
$WriteHTML .= '<tr><td height="130px" colspan="2"><table width="100%"><tr><td height="130px" style="text-align:center;vertical-align:bottom;border:none !important">' . $user['username'] . '<br>(Tested by)</td><td style="text-align:center;vertical-align:bottom">' . $cb . '<br>(Checked by)</td></tr></table></td><td style="text-align:start;border:none;">Rev. 0</td></tr>';
$WriteHTML .= '</tbody></table>';

// for ($i = 0; $i < 8; $i++) {
//     $WriteHTML .= ('
//     <tr>
//     <td>1</td>
//     <td>1</td>
//     <td>RACK 84TE 3U G2S</td>
//     <td>FT0039310-100</td>
//     <td><img src="../../images/bs-2.jpg"  width="6.5cm" alt=""></td>
// </tr>

// ');
// }
$foot = '<center> SHEET {PAGENO} / {nbpg} </center>';
$mpdf->SetHTMLFooter($foot);
$mpdf->writeHTML($WriteHTML);
$mpdf->Output();
