<?php

$rId = 4;
require '../../includes/init.php';

$db = db();

extract($_POST);

check('cStage', 'Current Stage required');
check('nextStage', 'Next Stage required');
check('cStageState', 'Current Stage State required');
check('serialss', 'Serial Number Required');

$userBy = $user['username'];

function getPartId($db, $serialss)
{
    $result = mysqli_query($db, "SELECT id FROM a_part_accessories_staging WHERE serial_no='$serialss' AND parent=0");

    if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_assoc($result);
        return $row['id'];
    }
    err('Part not found');
}

$partId = getPartId($db, $serialss);

if ($cStage == '7' && $cStageState == '1') {

    if (mysqli_query($db, "UPDATE a_stage_updation SET status='$cStageState',updated_by='$userBy' WHERE part_id='$partId' AND stage='$cStage'")) {

        mysqli_close($db);
        complete(true);
    }
    err('Unable to update');
}

if ($cStage == '1') {
    if (isset($insertion) && $insertion == 'true') {
        if (mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES('$partId',1,1,'$userBy')")) {
            if (mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES('$partId','$nextStage',-1,'$userBy')")) {
                mysqli_close($db);
                complete(true);
            }
        }
        err('Unable to update');
    }
}

if (mysqli_query($db, "UPDATE a_stage_updation SET status='$cStageState',updated_by='$userBy' WHERE part_id='$partId' AND stage='$cStage' AND status = -1")) {
    if (mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES('$partId','$nextStage',-1,'$userBy')")) {
        mysqli_close($db);
        complete(true);
    }
}

mysqli_close($db);
err('Unable to update!');
