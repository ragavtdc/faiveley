<?php

$res = array();

function getStage($db, $code)
{
    $stmt = $db->prepare("SELECT *,(SELECT main_stage_description FROM main_stage_master WHERE main_stage_no = pss.main_stage_no) AS main,(SELECT sub_stage_description FROM sub_stage_master WHERE sub_stage_no=pss.sub_stage_no) AS sub FROM part_stage_status pss WHERE part_code_id = (SELECT id FROM part_master WHERE part_code_id='$code') AND state=1 ORDER BY id DESC LIMIT 1");

    if ($stmt->execute()) {
        $result = $stmt->get_result();
        $rows = mysqli_fetch_assoc($result);
        $res = $rows;
        if ($res == null) {
            $stmt2 = $db->prepare("SELECT * FROM part_master WHERE part_code_id='$code'");

            if ($stmt2->execute()) {
                $result2 = $stmt2->get_result();
                $count = mysqli_num_rows($result2);
                if ($count > 0) {
                    $_SESSION['mainStage'] = 1;
                    $_SESSION['subStage'] = 1;
                } else {
                    $res = 0;
                }
            }
        } else {
            $_SESSION['mainStage'] = $res['main_stage_no'];
            $_SESSION['subStage'] = $res['sub_stage_no'];

            $stmt = $db->prepare("SELECT *,(SELECT main_stage_description FROM main_stage_master WHERE main_stage_no=pss.main_stage_no) AS main,(SELECT sub_stage_description FROM sub_stage_master WHERE sub_stage_no=pss.sub_stage_no) AS sub,(SELECT accessories_part_name FROM part_accessories_master WHERE id=pss.failure_accessory) AS accessory FROM part_stage_status pss WHERE part_code_id=(SELECT id FROM part_master WHERE part_code_id='$code') AND state=1 AND failure=1");

            if ($stmt->execute()) {
                $r = array();
                $result = $stmt->get_result();
                while ($rows = mysqli_fetch_assoc($result)) {
                    $r[] = $rows;
                }
                $res['fails'] = $r;
            }
        }
        complete($res);
    } else {
        err(['status' => 'failed']);
    }
}

function updateStageStatus($db, $partCode, $mainStage, $subStage, $statusCondition, $comments, $fromMain, $fromSub)
{
    if ($fromMain == $_SESSION['mainStage'] && $fromSub == $_SESSION['subStage']) {
        unset($_SESSION['mainStage']);
        unset($_SESSION['subStage']);

        if ($statusCondition == '2' && $subStage != '1') {
            $currentSubStage = ((int) $subStage) - 1;
        } else {
            $currentSubStage = $subStage;
        }
        if ($mainStage == '1') {
            $currentMainStage = '1';
        } elseif ($mainStage == '2' && ((int) $subStage) < 5 && ((int) $subStage) > 3) {
            $currentMainStage = '2';
        } elseif ($mainStage == '3' && ((int) $subStage) < 10 && ((int) $subStage) > 5) {
            $currentMainStage = '3';
        } elseif ($mainStage == '4' && ((int) $subStage) < 12 && ((int) $subStage) > 9) {
            $currentMainStage = '4';
        }

        if ($mainStage == '2' && $subStage == '3' && $statusCondition == '2') {
            $currentMainStage = '1';
        } elseif ($mainStage == '3' && $subStage == '5' && $statusCondition == '2') {
            $currentMainStage = '2';
        } elseif ($mainStage == '4' && $subStage == '10' && $statusCondition == '2') {
            $currentMainStage = '3';
        } elseif ($mainStage == '5' && $subStage == '11' && $statusCondition == '2') {
            $currentMainStage = '4';
        }
        $stmt = $db->prepare("INSERT INTO part_stage_status(part_code_id,main_stage_no,sub_stage_no,last_status_condition,current_stage_no,current_sub_stage_no,comments)VALUES((SELECT id FROM part_master WHERE part_code_id=?),?,?,?,?,?,?)");

        $stmt->bind_param('sssssss', $partCode, $mainStage, $subStage, $statusCondition, $currentMainStage, $currentSubStage, $comments);

        if ($stmt->execute()) {
            $stmt2 = $db->prepare("INSERT INTO Faiveley_product_status(part_code_id,main_stage_no,sub_stage_no,status,comments)VALUES((SELECT id FROM part_master WHERE part_code_id=?),?,?,?,?) ON DUPLICATE KEY UPDATE main_stage_no=?,sub_stage_no=?,status=?,comments=?");

            $stmt2->bind_param('sssssssss', $partCode, $mainStage, $subStage, $statusCondition, $comments, $mainStage, $subStage, $statusCondition, $comments);

            if ($stmt2->execute()) {
                complete(['status' => 'success']);
            } else {
                err(['status' => 'failed']);
            }
        } else {
            err(['status' => 'failed']);
        }
    } else {
        err(['invalid' => 'stage mismatch']);
    }
}

function addFailure($db, $partCode, $mainStage, $subStage, $fComments, $fAccessory)
{
    if ($mainStage == $_SESSION['mainStage'] && $subStage == $_SESSION['subStage']) {
        unset($_SESSION['mainStage']);
        unset($_SESSION['subStage']);

        $stmt = $db->prepare("INSERT INTO part_stage_status(part_code_id,main_stage_no,sub_stage_no,last_status_condition,current_stage_no,current_sub_stage_no,failure,failure_accessory,failure_comments)VALUES((SELECT id FROM part_master WHERE part_code_id=?),?,?,1,?,?,1,?,?)");

        $stmt->bind_param('sssssss', $partCode, $mainStage, $subStage, $mainStage, $subStage, $fAccessory, $fComments);

        if ($stmt->execute()) {
            complete(['status' => 'success']);
        } else {
            err(['status' => 'failed']);
        }
    } else {
        err(['invalid' => 'stage mismatch']);
    }
}

$rId = 4;
require '../../includes/init.php';

$db = db();

extract($_REQUEST);

if (isset($type)) {
    updateStageStatus($db, $partCode, $mainStage, $subStage, $statusCondition, $comments, $fromMain, $fromSub);
} elseif (isset($failure)) {
    addFailure($db, $partCode, $mainStage, $subStage, $fComments, $fAccessory);
} else {
    getStage($db, $code);
}
