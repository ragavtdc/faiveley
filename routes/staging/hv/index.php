<?php

$rId = 16;
require '../../includes/init.php';

$db = db();

function getHvTestList(): array
{
    $db = db();
    $rows = array();
    $result = mysqli_query($db, "SELECT * FROM a_hvtest_master");
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    mysqli_close($db);
    return $rows;
}
$hvList = getHvTestList();
head();
?>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-database'></i> HV & IR TESTING </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-database fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>HV & IR TESTING</a></li>
    </ul>
</div>
<div class="container">

    <div class="row tile">
        <div class='form-group col-lg-6 col-md-6 col-sm-12'>
            <!-- <label class='control-label'>Barcode Scan</label> -->
            <input class='form-control' type='text' placeholder='Scan Barcode' name='serial' value="" id="serial" required autofocus onkeyup="if (event.keyCode === 13) {fetchAcc();}">
        </div>
        <div class="form-group col-lg-6 col-md-6 col-sm-12">
            <button id="getInfoBtn" onclick="fetchAcc()" class="btn waves-effect waves-light btn-sm btn-info" data-toggle="tooltip" data-original-title="Get Unit Info">Get Unit Info</button>
        </div>
        <div class="col-sm-12">
            <ul class="list-group" style="display: none;">
                <li id="unitInfo" class="list-group-item d-flex justify-content-between align-items-center"></li>
            </ul>
            <p class="text-center"></p>
        </div>


    </div>
</div>

<div class=' col-sm-12 text-center'>
    <div id='progressDiv' style='display: none;'>
        <div class='progress'>
            <div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='75' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div>
        </div>
    </div>
</div>

<div class="container tile" id="accDiv" style="display: none;">
    <ul class="list-group" id="accList">

    </ul>

    <form id="hvReport">
        <div class='row m-3'>
            <div class='form-group col-sm-12'>
                <label class='control-label'><strong>Configuration Level</strong></label>
                <input class='form-control' type='text' placeholder='Enter Configuration Level' name='cLevel' required>
            </div>
            <div class='form-group col-sm-12'>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Dielectric Tests</th>
                            <th>Measured Resistor value</th>
                            <th>OK / NOK</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($hvList as $value) {
                            echo '<tr>';
                            echo '<td>';
                            echo '<input class="form-control" type="text" name="tests[]" required value="' . $value['test_name'] . '">';
                            echo '</td>';
                            echo '<td>';
                            echo '<input class="form-control" type="';
                            echo ($value['reading_required'] == 1 ? 'text' : 'hidden');
                            echo '" name="readings[]"';
                            echo ($value['reading_required'] == 1 ? ' required' : '');
                            echo '></td>';
                            echo '<td>';
                            echo '<select class="form-control" name="testState[]" required>';
                            echo '<option value="">select an option</option>';
                            echo '<option value="1">OK</option>';
                            echo '<option value="0">NOK</option>';
                            echo '</select>';
                            echo '</td>';
                            echo '<tr>';
                        }
                        ?>
                    </tbody>
                </table>


            </div>
            <div class='form-group btn-container col-lg-12 col-md-12 col-sm-12 text-center'>
                <input type="hidden" name="stageState" id="stageState" value="">
                <button type="button" class="btn waves-effect waves-light btn-sm ml-3 btn-danger" onclick="update(0)">SAVE STAGE AS FAILED</button>
                <button type="button" id="passBtn" class="btn waves-effect waves-light btn-sm ml-3 btn-success" onclick="update(1)"></i>SAVE STAGE AS PASSED</button>
                <button type="submit" id="theSubmit" style="display: none;"></button>
            </div>
        </div>
    </form>
</div>


<div class="container tile text-center" id="testsDiv" style="display: none;">

    <ul class="list-group">
        <li id="stageStatus" class="list-group-item d-flex justify-content-between align-items-center"></li>
        <li id="configLevel" class="list-group-item d-flex justify-content-between align-items-center"></li>
    </ul>
    <table class="table table-bordered table-sm">
        <thead>
            <tr>
                <th>Dielectric Tests</th>
                <th>Measured Resistor value</th>
                <th>OK / NOK</th>
            </tr>
        </thead>
        <tbody id="testsBody">

        </tbody>
    </table>

    <button class="btn waves-effect waves-light btn-sm m-3 p-2 btn-warning " onclick="printer()">PRINT</button>
</div>

<!-- Modal -->
<!-- <div class="modal fade" id="stageSelectionModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">STAGE SUBMISSION</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="stageUpdateFrm">

                    <input type="hidden" id="serialss" name="serialss" value="">

                    <input type="hidden" name="cStage" value="2">

                    <select name="cStageState" required>
                        <option selected disabled>Select Status</option>
                        <option value="1">Pass</option>
                        <option value="0">Fail</option>
                    </select>

                    <select name="nextStage" class="browser-default custom-select" required>
                        <option selected>Select Next Stage</option>
                        <option value="1">ASSEMBLING</option>
                        <option value="2">HV & IR TESTING</option>
                        <option value="3">FUNCTIONAL TESTING</option>
                    </select>

                    <button type="submit" class="btn btn-primary">Submit</button>

                </form> -->

<!-- Group of default radios - option 2 -->
<!-- <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="failRadio" name="passFailRadio" checked>
                        <label class="custom-control-label" for="failRadio">FAIL</label>
                    </div> -->

<!-- Basic dropdown -->

<!-- Basic dropdown -->
<!-- </div>
        </div>
    </div>
</div> -->

<?php foot();
?>

<link href='<?php echo $path ?>js/mdb-css/bootstrap.min.css' rel='stylesheet'>

<script type='text/javascript' src='<?php echo $path ?>js/mdb-js/addons/progressBar.js'></script>
<script type='text/javascript' src='<?php echo $path ?>js/mdb-js/addons/progressBar.min.js'></script>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    let selectedPartVal;
    let selectedPartId;
    var progress = $('#progressDiv');
    var accDiv = $('#accDiv');
    var testsDiv = $('#testsDiv');

    function fetchAcc() {
        selectedPartVal = $('#serial').val();
        $('#serialss').val(selectedPartVal);
        accDiv.hide();
        testsDiv.hide();
        progress.show();
        $('#unitInfo').html('').parent().hide();
        var data = {
            'serial': selectedPartVal
        }
        var func = (data) => {

            console.log(data);
            progress.hide();
            $('#unitInfo').html('<h6>' + data.unit.part_name + '</h6><code>' + data.unit.part_no + '</code>').parent().show();
            $('#serial').val(data.unit.serial_no);
            selectedPartId = data.unit.id;
            if (data.status == -1) {
                accDiv.show();
                testsDiv.hide();
                let html = '';
                // html += '<li class="list-group-item d-flex justify-content-between align-items-center">' + data.unit.part_name + '<code> ' + data.unit.part_no + '</code></li>';
                data.acc.forEach(ac => {
                    html += '<li id="list' + ac.acc_id + '" class="list-group-item d-flex justify-content-between align-items-center">';
                    html += ac.serial_no + ' - ' + ac.acc_name;
                    html += '<div><button class="btn btn-flat btn-sm p-1 waves-effect waves-light" onclick="markAsFailed(' + ac.id + ',' + ac.acc_id + ',\'' + ac.serial_no + '\',\'' + ac.acc_name + '\')">Mark As Failure</button></div>';
                    html += '</li>';
                });
                $('#accList').html(html);
            } else {
                accDiv.hide();
                let html = '';
                data.tests.forEach(t => {
                    html += '<tr>';
                    html += '<td>' + t.test_name + '</td>';
                    html += '<td>' + t.reading + '</td>';
                    html += '<td>' + (t.test_state == 1 ? 'OK' : 'NOK') + '</td>';
                    html += '</tr>';
                });
                $('#testsBody').html(html);
                $('#stageStatus').html('<h6>STAGE</h6><code>' + (data.status == 1 ? ' PASSED' : 'STAGE FAILED') + '</code>');
                $('#configLevel').html('<h6>Configuration Level</h6><code>' + data.unit.reading + '</code>');
                testsDiv.show();
            }

        };
        ajax('api/fetchAcc.php', data, func);
    }

    // function updateStage(status) {
    //     selectedPartVal = $('#serial').val();
    //     let r = $('#reading').val().trim();
    //     if (r == '') {
    //         toast('Enter Reading');
    //         return;
    //     }
    //     let data = {
    //         'status': status,
    //         'serial': selectedPartVal,
    //         'reading': r
    //     }
    //     let func = (data) => {
    //         if (data) {
    //             successToast('Stage Updated');
    //             progress.hide();
    //             setTimeout(() => {
    //                 window.location.reload();
    //             }, 1500);
    //         } else {
    //             toast("Try again later...");
    //             progress.hide();
    //         }
    //     }
    //     progress.show();
    //     ajax('api/updateStage.php', data, func);
    // }

    // $('#stageUpdateFrm').submit((e) => {
    //     e.preventDefault();
    //     const func = (data) => {
    //         successToast('Updated Successfully');
    //         setTimeout(() => {
    //             window.location.reload();
    //         }, 1500);
    //     }
    //     ajax('../api/stage-update.php', $('#stageUpdateFrm').serialize(), func);
    // });
    const hvForm = $('#hvReport');

    hvForm.submit((e) => {
        e.preventDefault();
        if (selectedPartId == null || selectedPartId == undefined) {
            toast('Please refresh and try again');
        }

        const func = (data) => {
            successToast('Updated Successfully');
            setTimeout(() => {
                window.location.reload();
                // console.log(data);
            }, 1800);
        }
        if ($('#stageState').val() == '') {
            toast('stage not set');
            return;
        }
        let data = hvForm.serialize() + '&partID=' + selectedPartId;
        ajax('api/updateStage.php', data, func);
    });

    function update(state) {
        $('#stageState').val(state);
        $('#theSubmit').trigger('click');
    }

    function printer() {
        window.open('./api/generate-pdf.php?partId=' + selectedPartId, '_blank');
    }

    function markAsFailed(id, accId, serial, name) {
        let data = {
            'id': id,
            'serial': serial,
            'name': name
        }
        let func = (data) => {
            if (data) {
                successToast('Marked As Failure');
                $('#passBtn').hide();
                $('#list' + accId).removeClass('d-flex');
                $('#list' + accId).hide();
                progress.hide();
            } else {
                toast('Try Again Later...');
                progress.hide();
            }
        }
        progress.show();
        ajax('api/markFailure.php', data, func);
    }
</script>
</body>

</html>