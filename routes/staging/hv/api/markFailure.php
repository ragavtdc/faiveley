<?php

$rId = 16;
require '../../../includes/init.php';

$db = db();

extract($_POST);

check('id', 'ID is required');
check('serial', 'serial no required');
check('name', 'accessory name required');

// die();
// $result = mysqli_query($db, "SELECT id FROM a_part_accessories_staging WHERE serial_no = '$serial' AND parent=0 ORDER BY created_at DESC LIMIT 1");

// $row = mysqli_fetch_assoc($result);

// $partId = $row['id'];

$updatedBy = $user['username'];

$stmt = $db->prepare("UPDATE a_part_accessories_staging SET testing=0,failed_stage=2,updated_by=? WHERE id=?");

$stmt->bind_param('ss', $updatedBy, $id);

if (!$stmt->execute()) {
    err(mysqli_error($db));
}

$stmt->close();

$stmt = $db->prepare("INSERT INTO a_rework_master(reworked_part_serial_no,reworked_part_acc_name,reworked_date,state,comments)VALUES(?, ?, ?,0,'nil')");
$d = date("Y-m-d");
$stmt->bind_param('sss', $serial, $name, $d);

if (!$stmt->execute()) {
    err(mysqli_error($db));
}

$stmt->close();

complete(true);
