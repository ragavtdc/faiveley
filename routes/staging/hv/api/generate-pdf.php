<?php
$rId = 0;
require '../../../includes/init.php';
require_once '../../../print/vendor/autoload.php';

check('partId', 'PartId Required');

$db = db();

extract($_GET);

$result = mysqli_query($db, "SELECT a.id,a.type,b.part_name,b.part_description,a.serial_no,a.reading,b.part_no FROM a_part_accessories_staging a INNER JOIN a_part_master b ON a.type = b.part_id WHERE a.id = '$partId' AND a.parent=0 ORDER BY a.created_at DESC LIMIT 1");
if (mysqli_num_rows($result) != 1) {
    err('No part found');
}
$row = mysqli_fetch_assoc($result);

$serial = $row['serial_no'];
$partName = $row['part_name'];
$partDesc = $row['part_description'];
$cLevel = $row['reading'];
$partNo = $row['part_no'];

$result = mysqli_query($db, "SELECT * FROM a_stage_updation WHERE part_id=$partId AND stage = 2 ORDER BY id DESC LIMIT 1");

if (mysqli_num_rows($result) != 1) {
    err('Unit Not Assembled');
}

$row = mysqli_fetch_assoc($result);

$status =  $row['status'];

if ($status == '-1') {
    complete('This is the Current stage.');
}

$result = mysqli_query($db, "SELECT * FROM a_hvtest_log where part_id=$partId and state=$status");
$tests = array();
while ($row = mysqli_fetch_assoc($result)) {
    $tests[] = $row;
}


// complete($rows);

$mpdf = new \Mpdf\Mpdf([
    'setAutoTopMargin' => 'stretch',
    'autoMarginPadding' => 0,
    'mode' => 'utf-8',
    'format' => 'A4-P',
    'orientation' => 'P'
]);

$mpdf->SetHTMLHeader(' 
');
$WriteHTML = '
<style>
    table,
    th,
    td {
        border: 1px solid grey;
        border-collapse: collapse;
        text-align: center;
        vertical-align: middle;
    }
</style>
<header style="text-align:center; margin-bottom:10px">
        <img height="80" src="../../../../images/logo-wabtec.png" />
</header>
<table width="100%">
<tbody>
    <tr class="text-center">
        <th colspan="3" style="font-weight: bold;font-size: 24px;">
            HV IV STAGE REPORT
        </th>
    </tr>
    <tr>
        <th >
            Serial
        </th>
        <td style="text-align: start;" colspan="2" >
        ' . $serial . '
        </td>
    </tr>
    <tr>
        <th>
            Part Name
        </th>
        <td style="text-align: start;" colspan="2">
        ' . $partName . '
        </td>
    </tr>
    <tr>
        <th>
            PART NO
        </th>
        <td style="text-align: start;" colspan="2">
        ' . $partNo . '
        </td>
    </tr>
   
    <tr>
        <th>
            Configuration Level
        </th>
        <td style="text-align: start;" colspan="2">
        ' . $cLevel . '
        </td>
    </tr>
    <tr>
        <th>
            Date
        </th>
        <td style="text-align: start;" colspan="2">
        ' . date("d-m-Y") . '
        </td>
    </tr>
</tbody>

</table>
<br>
<table width="100%">
<thead>
    <tr>
        <th >
            Results Dielectric Tests
        </th>
        <th >
            Measured Resistor Value
        </th>
        <th >
            OK / NOK
        </th>
    </tr>
</thead>
<tbody>';


for ($i = 0; $i < count($tests); $i++) {
    $row = $tests[$i];
    $WriteHTML .= '<tr>';
    $WriteHTML .= '<td>' . $row['test_name'] . '</td>';
    $WriteHTML .= '<td>' . $row['reading'] . '</td>';
    $WriteHTML .= '<td>' . ($row['test_state'] == 1 ? 'OK' : 'NOK') . '</td>';
    // $WriteHTML .= '<td>' . $row['qad'] . '</td><td>' . $row['qRemark'] . '</td>';
    $WriteHTML .= '</tr>';
}
$WriteHTML .= '<tr><td height="130px" colspan="3"><table width="100%"><tr><td height="130px" style="text-align:center;vertical-align:bottom;border:none !important">' . $user['username'] . '<br>(Tested by)</td><td style="text-align:center;vertical-align:bottom">' . ' .' . '<br>(Checked by)</td></tr></table></td></tr>';
$WriteHTML .= '</tbody></table>';


$foot = '<center> SHEET {PAGENO} / {nbpg} </center>';
$mpdf->SetHTMLFooter($foot);
$mpdf->writeHTML($WriteHTML);
$mpdf->SetTitle($serial);
$mpdf->Output($serial . '.pdf', 'I');
