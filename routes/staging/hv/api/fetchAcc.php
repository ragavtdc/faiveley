<?php

$rId = 16;
require '../../../includes/init.php';

$db = db();

extract($_POST);

check('serial', 'part id required');

if (strlen($serial) < 5) {
    err('Insufficient Serial no');
}

$result = mysqli_query($db, "SELECT pm.*,pas.id,pas.reading,pas.serial_no FROM a_part_accessories_staging pas inner join a_part_master pm on pm.part_id=pas.type where (pas.serial_no like '%$serial%' or pas.serial_alpha like '%$serial%') and pas.parent = 0 limit 1");

if (mysqli_num_rows($result) != 1) {
    err('Unit Not Found');
}

$row = mysqli_fetch_assoc($result);

$data['unit'] = $row;

$partId = $row['id'];

$result = mysqli_query($db, "SELECT * FROM a_stage_updation WHERE part_id=$partId AND stage = 2 ORDER BY id DESC LIMIT 1");

if (mysqli_num_rows($result) != 1) {
    err('Unit Not Assembled');
}

$row = mysqli_fetch_assoc($result);

$data['status'] = $status =  $row['status'];

if ($status == '-1') {
    // err('Already Updated');

    $result = mysqli_query($db, "SELECT am.*,pas.serial_no,pas.id FROM a_part_accessories_staging pas inner join a_accessories_master am on am.acc_id=pas.type where pas.parent=$partId and testing=1");
    $r = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $r[] = $row;
    }

    $data['acc'] = $r;

    mysqli_close($db);

    complete($data);
} else {
    // $result = mysqli_query($db, "SELECT * FROM a_hvtest_log where part_id=$partId and state=$status");
    // $r = array();
    // while ($row = mysqli_fetch_assoc($result)) {
    //     $r[] = $row;
    // }

    // $data['tests'] = $r;
    mysqli_close($db);
    err('Unit not in this stage');
}
