<?php

$rId = 16;
require '../../../includes/init.php';

$db = db();

// complete($_POST);

extract($_POST);

$userBy = $user['username'];

// check('status', 'Status is required');
// check('serial', 'Serial Number is required');
// check('reading', 'Reading is required');

// $result = mysqli_query($db, "SELECT id FROM a_part_accessories_staging WHERE serial_no = '$serial' AND parent=0 ORDER BY created_at DESC LIMIT 1");

// $row = mysqli_fetch_assoc($result);

// $partId = $row['id'];



// if (mysqli_query($db, "UPDATE a_stage_updation SET status='$status',updated_by='$userBy' WHERE part_id='$partId' AND stage=2 AND status = -1")) {
//     $nextStage = $status == '1' ? 3 : 1;
//     if (mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES('$partId','$nextStage',-1,'$userBy')")) {
//         mysqli_close($db);
//         complete(true);
//     }
// }
// err('Unable to update');


check('cLevel', 'Configuration Level required');
check('partID', 'Part ID required');
check('readings', 'Measurment value required');
check('testState', 'Test OK/NOK required');
check('tests', 'Test names required');
check('stageState', 'stageState required');

// complete($_POST);

if (count($readings) != count($testState) || count($testState) != count($tests)) {
    err('Insufficient data');
}

for ($i = 0; $i < count($tests); $i++) {
    $test = $tests[$i];
    $tState = $testState[$i];
    $reading = $readings[$i];
    mysqli_query($db, "INSERT INTO a_hvtest_log(part_id,test_name,reading,test_state,state,created_by)VALUES('$partID','$test','$reading','$tState','$stageState','$userBy')");
}

mysqli_query($db, "UPDATE a_part_accessories_staging SET reading = '$cLevel' WHERE id='$partID'");

$status = $stageState;

if (mysqli_query($db, "UPDATE a_stage_updation SET status='$status',updated_by='$userBy' WHERE part_id='$partID' AND stage=2 AND status = -1")) {
    $nextStage = $status == '1' ? 3 : 1;
    if (mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES('$partID','$nextStage',-1,'$userBy')")) {
        mysqli_close($db);
        complete(true);
    }
}

err('Unable to update');
