<?php

$rId = 4;
require '../includes/init.php';

$db = db();

$res = array();

$stmt = $db->prepare("SELECT username FROM user_master WHERE state=1");

if ($stmt->execute()) {
    $result = $stmt->get_result();
    while ($rows = mysqli_fetch_assoc($result)) {
        $res[] = $rows;
    }
}
$stmt->close();

head();
?>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-database'></i> Staging </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-database fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>Staging</a></li>
    </ul>
</div>
<!-- <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item col-lg-12 col-md-12 col-sm-12 text-center">
                <a class="nav-link active" id="addUserTab" data-toggle="pill" href="#addUser" role="tab" aria-controls="addUser" aria-selected="true">Manage Users</a>
            </li>
        </ul> -->
<!-- <div class="tab-content container" id="pills-tabContent" style="margin: 16px;"> -->
<!-- <br> -->
<!-- <div class="tab-pane fade show active" id="addUser" role="tabpanel" aria-labelledby="addUserTab"> -->
<?php if (isset($_SESSION['error'])) { ?>
    <h1 class="tile red text-center text-white">
        <?php echo $_SESSION['error'];
        unset($_SESSION['error']); ?>
    </h1>
<?php } ?>

<?php if (isset($_SESSION['success'])) { ?>
    <h1 class="tile green text-center text-white">
        <?php echo $_SESSION['success'];
        unset($_SESSION['success']); ?>
    </h1>
<?php } ?>
<div class="container">
    <div class="row tile">
        <div class='form-group col-lg-6 col-md-6 col-sm-12'>
            <label class='control-label'>Barcode Scan</label>
            <input class='form-control' type='text' placeholder='Scanned Barcode' name='barcode' id="barcode" onkeyup="if (event.keyCode === 13) {$('#getDetailsBtn').click();}" required autofocus>
        </div>
        <div class='form-group col-lg-6 col-md-6 col-sm-12 mt-3 text-center'>
            <!-- <div id="progressDiv" style="display: none;" class="col-lg-2 offset-lg-5 col-md-2 offset-md-5 col-sm-12">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                    </div> -->
            <div id="progressDiv" style="display: none;" class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
            <a id="getDetailsBtn" onclick="getBarcode()" class="btn btn-purple btn-lg pl-4 pr-4"><i class="fa fa-qrcode" style="font-size: 30px;"></i></a>
        </div>
    </div>
</div>
<div class="container" id="currentStageDiv" style="display: none;">
    <div class="row">
        <div class="tile col-12 blue-text">
            <h3 id="currentStage"></h3>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="tile col-12 blue-text text-center" id="completedDiv" style="display: none;">
            <h3>COMPLETED</h3>
        </div>
        <div class="tile col-lg-5 col-md-12 col-sm-12" id="mainStage1" style="display: none;">
            <h4>STAGE 1 - ASSEMBLING</h4>
            <br>
            <!-- <div class="row"> -->
            <div class="tile z-depth-1 col-lg-12 col-md-12 col-sm-12">
                <div class="col-12 text-center">PRELIMINARY SERIALIZATION</div>
                <br>
                <div class="row text-center">
                    <!-- <div class="col-4">
                                <input type="radio" name="prel_condition" value="0"><span class="red-text ml-2">To be Start</span></input>
                            </div> -->
                    <div class="col-6">
                        <input type="radio" name="prel_condition" value="1"><span class="amber-text ml-2">Ongoing</span></input>
                    </div>
                    <div class="col-6">
                        <input type="radio" name="prel_condition" value="2"><span class="green-text ml-2">Completed</span></input>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Enter Comments' name='comments' id="comments1" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <div id="progressDiv1" style="display: none;" class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="subStageUpdate1" onclick="updateStageStatus('1','1','prel_condition')" name="subStageUpdate1">UPDATE</button>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-1">
                        <h6>OR</h6>
                    </div>
                    <br>
                    <div class="col-12">
                        <select class='form-control' disabled id='failureAccessory1'>
                            <option disabled selected>Select Failure Accessory</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Failure Comments (If Any Failure)' name='fail_comments' id="fail_comments1" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="failureUpdate1" onclick="addToFailure('1','1')" name="failureUpdate1">FAILURE</button>
                    </div>
                </div>
            </div>
            <!-- </div> -->
            <div class="tile z-depth-1 col-lg-12 col-md-12 col-sm-12">
                <div class="col-12 text-center">PCBA INSERTION</div>
                <br>
                <div class="row text-center">
                    <!-- <div class="col-4">
                                <input type="radio" name="pcba_condition" value="0"><span class="red-text ml-2">To be Start</span></input>
                            </div> -->
                    <div class="col-6">
                        <input type="radio" name="pcba_condition" value="1"><span class="amber-text ml-2">Ongoing</span></input>
                    </div>
                    <div class="col-6">
                        <input type="radio" name="pcba_condition" value="2"><span class="green-text ml-2">Completed</span></input>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Enter Comments' name='comments' id="comments2" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <div id="progressDiv2" style="display: none;" class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="subStageUpdate2" onclick="updateStageStatus('1','2','pcba_condition')" name="subStageUpdate2">UPDATE</button>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-1">
                        <h6>OR</h6>
                    </div>
                    <br>
                    <div class="col-12">
                        <select class='form-control' disabled id='failureAccessory2'>
                            <option disabled selected>Select Failure Accessory</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Failure Comments (If Any Failure)' name='fail_comments' id="fail_comments2" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="failureUpdate2" onclick="addToFailure('1','2')">FAILURE</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="tile col-lg-5 col-md-12 col-sm-12 offset-lg-2" id="mainStage2" style="display: none;">
            <h4>STAGE 2 - TESTING</h4>
            <br>
            <div class="tile z-depth-1 col-12">
                <div class="col-12 text-center">INSULATION TEST</div>
                <br>
                <div class="row text-center">
                    <!-- <div class="col-4">
                                <input type="radio" name="insul_condition" value="0"><span class="red-text ml-2">To be Start</span></input>
                            </div> -->
                    <div class="col-6">
                        <input type="radio" name="insul_condition" value="1"><span class="amber-text ml-2">Ongoing</span></input>
                    </div>
                    <div class="col-6">
                        <input type="radio" name="insul_condition" value="2"><span class="green-text ml-2">Completed</span></input>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Enter Comments' name='comments' id="comments3" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div id="progressDiv3" style="display: none;" class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="subStageUpdate3" onclick="updateStageStatus('2','3','insul_condition')" name="subStageUpdate3">UPDATE</button>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-1">
                        <h6>OR</h6>
                    </div>
                    <br>
                    <div class="col-12">
                        <select class='form-control' disabled id='failureAccessory3'>
                            <option disabled selected>Select Failure Accessory</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Failure Comments (If Any Failure)' name='fail_comments' id="fail_comments3" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="failureUpdate3" onclick="addToFailure('2','3')">FAILURE</button>
                    </div>
                </div>
            </div>
            <!-- </div> -->
            <div class="tile z-depth-1 col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-12 text-center">PERFORMANCE TEST</div>
                <br>
                <div class="row text-center">
                    <!-- <div class="col-4">
                                <input type="radio" name="perform_condtion" value="0"><span class="red-text ml-2">To be Start</span></input>
                            </div> -->
                    <div class="col-6">
                        <input type="radio" name="perform_condtion" value="1"><span class="amber-text ml-2">Ongoing</span></input>
                    </div>
                    <div class="col-6">
                        <input type="radio" name="perform_condtion" value="2"><span class="green-text ml-2">Completed</span></input>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Enter Comments' name='comments' id="comments4" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div id="progressDiv4" style="display: none;" class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="subStageUpdate4" onclick="updateStageStatus('2','4','perform_condtion')" name="subStageUpdate4">UPDATE</button>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-1">
                        <h6>OR</h6>
                    </div>
                    <br>
                    <div class="col-12">
                        <select class='form-control' disabled id='failureAccessory4'>
                            <option disabled selected>Select Failure Accessory</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Failure Comments (If Any Failure)' name='fail_comments' id="fail_comments4" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="failureUpdate4" onclick="addToFailure('2','4')">FAILURE</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="tile col-lg-12 col-md-12 col-sm-12" id="mainStage3" style="display: none;">
            <h4>STAGE 3 - BURNING</h4>
            <br>
            <div class="tile z-depth-1 col-lg-12 col-md-12 col-sm-12">
                <div class="row text-center">
                    <div class="col-12 text-center">ESS CHAMBER TEST</div>
                    <br>
                    <br>
                    <!-- <input type="radio" name="ess_chamber_condition" value="0"><span class="red-text ml-2">To be Start</span></input> -->
                    <div class="col-6">
                        <input type="radio" name="ess_chamber_condition" value="1"><span class="amber-text ml-2 mr-2">Ongoing</span></input>
                    </div>
                    <div class="col-6">
                        <input type="radio" name="ess_chamber_condition" value="2"><span class="green-text ml-2 mr-2">Completed</span></input>
                    </div>
                    <br><br>
                    <div class="col-lg-8 col-md-8 col-sm-12 text-center">
                        <input class='form-control' disabled type='text' placeholder='Enter Comments' name='comments' id="comments5" required>
                    </div>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <div id="progressDiv5" style="display: none;" class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <button disabled class="btn btn-grey btn-block btn-sm mt-1" id="subStageUpdate5" onclick="updateStageStatus('3','5','ess_chamber_condition')" name="subStageUpdate5">UPDATE</button>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-1">
                        <h6>OR</h6>
                    </div>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <select class='form-control' disabled id='failureAccessory5'>
                            <option disabled selected>Select Failure Accessory</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Failure Comments (If Any Failure)' name='fail_comments' id="fail_comments5" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="failureUpdate5" onclick="addToFailure('3','5')">FAILURE</button>
                    </div>
                </div>
            </div>
            <!-- </div> -->
            <div class="tile z-depth-1 col-lg-12 col-md-12 col-sm-12">
                <div class="row text-center">
                    <div class="col-lg-12 text-center">FINAL SERIALIZATION</div>
                    <br><br>
                    <!-- <input type="radio" name="final_serial_condition" value="0"><span class="red-text ml-2">To be Start</span></input> -->
                    <div class="col-6">
                        <input type="radio" name="final_serial_condition" value="1"><span class="amber-text ml-2 mr-2">Ongoing</span></input>
                    </div>
                    <div class="col-6">
                        <input type="radio" name="final_serial_condition" value="2"><span class="green-text ml-2 mr-2">Completed</span></input>
                    </div>
                    <br><br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Enter Comments' name='comments' id="comments6" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <div id="progressDiv6" style="display: none;" class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <button disabled class="btn btn-grey btn-block btn-sm mt-1" id="subStageUpdate6" onclick="updateStageStatus('3','6','final_serial_condition')" name="subStageUpdate6">UPDATE</button>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-1">
                        <h6>OR</h6>
                    </div>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <select class='form-control' disabled id='failureAccessory6'>
                            <option disabled selected>Select Failure Accessory</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Failure Comments (If Any Failure)' name='fail_comments' id="fail_comments6" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="failureUpdate6" onclick="addToFailure('3','6')">FAILURE</button>
                    </div>
                </div>
            </div>

            <div class="tile z-depth-1 col-lg-12 col-md-12 col-sm-12">
                <div class="row text-center">
                    <div class="col-12 text-center">SOFTWARE DOWNLOAD</div>
                    <br><br>
                    <!-- <input type="radio" name="soft_down_condition" value="0"><span class="red-text ml-2">To be Start</span></input> -->
                    <div class="col-6">
                        <input type="radio" name="soft_down_condition" value="1"><span class="amber-text ml-2 mr-2">Ongoing</span></input>
                    </div>
                    <div class="col-6">
                        <input type="radio" name="soft_down_condition" value="2"><span class="green-text ml-2 mr-2">Completed</span></input>
                    </div>
                    <br><br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Enter Comments' name='comments' id="comments7" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <div id="progressDiv7" style="display: none;" class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <button disabled class="btn btn-grey btn-block btn-sm mt-1" id="subStageUpdate7" onclick="updateStageStatus('3','7','soft_down_condition')" name="subStageUpdate7">UPDATE</button>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-1">
                        <h6>OR</h6>
                    </div>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <select class='form-control' disabled id='failureAccessory7'>
                            <option disabled selected>Select Failure Accessory</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Failure Comments (If Any Failure)' name='fail_comments' id="fail_comments7" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="failureUpdate7" onclick="addToFailure('3','7')">FAILURE</button>
                    </div>
                </div>
            </div>

            <div class="tile z-depth-1 col-lg-12 col-md-12 col-sm-12">
                <div class="row text-center">
                    <div class="col-12 text-center">OPERATOR VISUAL INSPECTION</div>
                    <br><br>
                    <!-- <input type="radio" name="op_visual_condition" value="0"><span class="red-text ml-2">To be Start</span></input> -->
                    <div class="col-6">
                        <input type="radio" name="op_visual_condition" value="1"><span class="amber-text ml-2 mr-2">Ongoing</span></input>
                    </div>
                    <div class="col-6">
                        <input type="radio" name="op_visual_condition" value="2"><span class="green-text ml-2 mr-2">Completed</span></input>
                    </div>
                    <br><br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Enter Comments' name='comments' id="comments8" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <div id="progressDiv8" style="display: none;" class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <button disabled class="btn btn-grey btn-block btn-sm mt-1" id="subStageUpdate8" onclick="updateStageStatus('3','8','op_visual_condition')" name="subStageUpdate8">UPDATE</button>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-1">
                        <h6>OR</h6>
                    </div>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <select class='form-control' disabled id='failureAccessory8'>
                            <option disabled selected>Select Failure Accessory</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Failure Comments (If Any Failure)' name='fail_comments' id="fail_comments8" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="failureUpdate8" onclick="addToFailure('3','8')">FAILURE</button>
                    </div>
                </div>
            </div>

            <div class="tile z-depth-1 col-lg-12 col-md-12 col-sm-12">
                <div class="row text-center">
                    <div class="col-12 text-center">CHECK FOR SOFTWARE</div>
                    <br><br>
                    <!-- <input type="radio" name="check_for_soft_condition" value="0"><span class="red-text ml-2">To be Start</span></input> -->
                    <div class="col-6">
                        <input type="radio" name="check_for_soft_condition" value="1"><span class="amber-text ml-2 mr-2">Ongoing</span></input>
                    </div>
                    <div class="col-6">
                        <input type="radio" name="check_for_soft_condition" value="2"><span class="green-text ml-2 mr-2">Completed</span></input>
                    </div>
                    <br><br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Enter Comments' name='comments' id="comments9" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <div id="progressDiv9" style="display: none;" class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <button disabled class="btn btn-grey btn-block btn-sm mt-1" id="subStageUpdate9" onclick="updateStageStatus('3','9','check_for_soft_condition')" name="subStageUpdate9">UPDATE</button>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-1">
                        <h6>OR</h6>
                    </div>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <select class='form-control' disabled id='failureAccessory9'>
                            <option disabled selected>Select Failure Accessory</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Failure Comments (If Any Failure)' name='fail_comments' id="fail_comments9" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="failureUpdate9" onclick="addToFailure('3','9')">FAILURE</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="tile col-lg-12 col-md-12 col-sm-12" id="mainStage4" style="display: none;">
            <h4>STAGE 4 - FINAL QUALITY INSPECTION</h4>
            <br>
            <div class="tile z-depth-1 col-lg-12 col-md-12 col-sm-12">
                <div class="row text-center">
                    <div class="col-12 text-center">QUALITY CHECKING</div>
                    <br><br>
                    <!-- <input type="radio" name="qual_check_condition" value="0"><span class="red-text ml-2">To be Start</span></input> -->
                    <div class="col-6">
                        <input type="radio" name="qual_check_condition" value="1"><span class="amber-text ml-2 mr-2">Ongoing</span></input>
                    </div>
                    <div class="col-6">
                        <input type="radio" name="qual_check_condition" value="2"><span class="green-text ml-2 mr-2">Completed</span></input>
                    </div>
                    <br><br>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Enter Comments' name='comments' id="comments10" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <div id="progressDiv10" style="display: none;" class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <button disabled class="btn btn-grey btn-block btn-sm mt-1" id="subStageUpdate10" onclick="updateStageStatus('4','10','qual_check_condition')" name="subStageUpdate10">UPDATE</button>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-1">
                        <h6>OR</h6>
                    </div>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <select class='form-control' disabled id='failureAccessory10'>
                            <option disabled selected>Select Failure Accessory</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <input class='form-control' disabled type='text' placeholder='Failure Comments (If Any Failure)' name='fail_comments' id="fail_comments10" required>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <button disabled class="btn btn-grey btn-sm btn-block mt-1" id="failureUpdate10" onclick="addToFailure('4','10')">FAILURE</button>
                    </div>
                </div>
            </div>
            <!-- </div> -->
        </div>
    </div>
</div>

<div class="container" id="failures" style="display: none;">
    <div class="row">
        <div class="tile col-12 blue-text">
            <h4 class="text-center" style="font-weight: bold;">FAILURES</h4>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Main Stage</th>
                        <th scope="col">Sub Stage</th>
                        <th scope="col">Failure Accessory</th>
                        <th scope="col">Comments</th>
                        <th scope="col">Time</th>
                    </tr>
                </thead>
                <tbody id="failureTable">

                </tbody>
            </table>
        </div>
    </div>
</div>

<?php foot();
?>

<!-- Page specific javascripts-->
<!-- Bootstrap core CSS -->
<link href='../../js/mdb-css/bootstrap.min.css' rel='stylesheet'>
<!-- Material Design Bootstrap -->
<link href='../../js/mdb-css/mdb.min.css' rel='stylesheet'>
<!-- Your custom styles ( optional ) -->
<link href='../../css/style.css' rel='stylesheet'>
<!-- JQuery -->
<script type='text/javascript' src='../../js/mdb-js/mdb.min.js'></script>
<script type='text/javascript' src='../../js/mdb-js/addons/progressBar.js'></script>
<script type='text/javascript' src='../../js/mdb-js/addons/progressBar.min.js'></script>
<script src='../../js/plugins/plotly-latest.min.js' type='text/javascript'></script>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<?php

$stmt = $db->prepare("SELECT * FROM part_accessories_master WHERE state=1");

if ($stmt->execute()) {
    $result = $stmt->get_result();
    while ($rows = mysqli_fetch_assoc($result)) {
        echo "<script>$('select').append($('<option>', {value: " . $rows['id'] . ",text: '" . $rows['accessories_part_name'] . "'}));</script>;";
    }
}
$stmt->close();
?>
<script>
    function getBarcode() {
        $('#completedDiv').hide();
        $('#currentStageDiv').hide();
        $('#mainStage1').hide();
        $('#mainStage2').hide();
        $('#mainStage3').hide();
        $('#mainStage4').hide();
        for (var i = 0; i < 11; i++) {
            $('#subStageUpdate' + i).attr('disabled', 'disabled');
            $('#subStageUpdate' + i).removeClass('btn-primary');
            $('#subStageUpdate' + i).addClass('btn-grey');

            $('#failureUpdate' + i).attr('disabled', 'disabled');
            $('#failureUpdate' + i).removeClass('btn-primary');
            $('#failureUpdate' + i).addClass('btn-grey');

            $('#failureAccessory' + i).attr('disabled', 'disabled');

            $('#failures').hide();
            $('#failureTable').html('');
        }
        for (var j = 0; j < 11; j++) {
            $('#comments' + j).val('');
            $('#comments' + j).attr('disabled', 'disabled');
            $('#fail_comments' + j).attr('disabled', 'disabled');
        }

        $("input[type=radio]").prop('checked', false);
        var progressDiv = $('#progressDiv');
        var getDetailsBtn = $('#getDetailsBtn');
        // var selectedUser = $('#selectedUser').val();
        // var updateUser = $('#updateUser');
        // var saveUser = $('#saveUser');
        // $('#username').parent().hide();
        // $('#fileUploader').parent().parent().css('margin-top', '20px');
        // $('#fileUploader').parent().prev().hide();

        // <-- Barcode to be get from barcode scan device --> //

        var barcode = $('#barcode').val();

        if (barcode == null || barcode == '') {
            toast('Invalid Attempt : ', 'Barcode Not Available');
            return;
        }

        progressDiv.show();
        getDetailsBtn.hide();
        // updateUser.show();
        // saveUser.hide();

        $.ajax({
            url: 'api/manage-stage.php',
            method: 'POST',
            data: {
                'code': barcode,
            },
            success: function(result) {
                console.log(result);
                var res = $.parseJSON(result);
                if (res.state == true) {
                    if (res.data == 0) {
                        toast('Invalid Code : ', 'Barcode Not Available...  Try to add in Part Mangement');
                        progressDiv.hide();
                        getDetailsBtn.show();
                        return;
                        // toast('Error', 'Part Code Not Available');
                    } else {
                        if (res.data == null) {
                            $('#subStageUpdate1').removeAttr('disabled');
                            $('#subStageUpdate1').removeClass('btn-grey');
                            $('#subStageUpdate1').addClass('btn-primary');

                            $('#failureUpdate1').removeAttr('disabled');
                            $('#failureUpdate1').removeClass('btn-grey');
                            $('#failureUpdate1').addClass('btn-primary');

                            $('#failureAccessory1').removeAttr('disabled');

                            $('#comments1').removeAttr('disabled');
                            $('#fail_comments1').removeAttr('disabled');
                            // $('#currentStageDiv').show();
                            // $('#currentStage').html('CURRENT POSITION<br><br><div class="row"><span class="col-sm-12 col-lg-6" style="">Main Stage : ASSEMBLING</span><span class="col-lg-6 col-sm-12">Sub Stage : PRELIMINARY SERIALIZATION</span></div>');
                            $('#mainStage1').show();
                            progressDiv.hide();
                            getDetailsBtn.show();
                            return;
                        }
                        if (res.data.sub_stage_no == '11') {
                            $('#completedDiv').show();
                            // return;
                        } else {
                            $('#currentStageDiv').show();
                            $('#currentStage').html(
                                'CURRENT POSITION<br><br><div class="row"><a onclick="gotoDiv(' + res.data
                                .main_stage_no + ')" class="col-sm-12 col-lg-6">Main Stage : ' + res.data
                                .main + '</a><span class="col-lg-6 col-sm-12">Sub Stage : ' + res.data.sub +
                                '</span></div>');
                        }
                        $('#subStageUpdate' + res.data.sub_stage_no).removeAttr('disabled');
                        $('#subStageUpdate' + res.data.sub_stage_no).removeClass('btn-grey');
                        $('#subStageUpdate' + res.data.sub_stage_no).addClass('btn-primary');

                        $('#failureUpdate' + res.data.sub_stage_no).removeAttr('disabled');
                        $('#failureUpdate' + res.data.sub_stage_no).removeClass('btn-grey');
                        $('#failureUpdate' + res.data.sub_stage_no).addClass('btn-primary');

                        $('#failureAccessory' + res.data.sub_stage_no).removeAttr('disabled');

                        $('#comments' + res.data.sub_stage_no).removeAttr('disabled');
                        $('#fail_comments' + res.data.sub_stage_no).removeAttr('disabled');
                        $('#mainStage' + res.data.main_stage_no).show();
                        if (res.data.last_status_condition != '2') {
                            $('#comments' + res.data.sub_stage_no).val(res.data.comments);
                        }
                        for (var i = res.data.main_stage_no; i > 0; i--) {
                            $('#mainStage' + i).show();
                        }
                        // $('#comments' + res.data.sub_stage_no).val(res.data.comments);

                        if (res.data.fails.length > 0) {
                            $('#failures').show();

                            var html = '';

                            for (const item in res.data.fails) {
                                if (res.data.fails.hasOwnProperty(item)) {
                                    const element = res.data.fails[item];
                                    var dt = formatDate(element.created_at);
                                    html += '<tr><td>' + element.main + '</td><td>' + element.sub +
                                        '</td><td>' + element.accessory + '</td><td>' + element
                                        .failure_comments + '</td><td>' + dt + '</td></tr>';
                                }
                            }
                            $('#failureTable').html(html);
                        }
                    }
                } else {
                    toast('Invalid Attempt : ', 'Server Error... Try Again Later');
                }
                progressDiv.hide();
                getDetailsBtn.show();
                window.history.replaceState({}, window.location, "/routes/staging/");
            }
        });

    }

    function gotoDiv(num) {
        window.location.href += '#mainStage' + num;
        window.history.replaceState({}, window.location, "/routes/staging/");
    }

    function updateStageStatus(stageId, subStageId, statusCondition) {
        var barcode = $('#barcode').val();
        var progressDiv = $('#progressDiv' + subStageId);
        var updateBtn = $('#subStageUpdate' + subStageId);
        var failUpdateBtn = $('#failureUpdate' + subStageId);
        var statusCondition = $("input[name=" + statusCondition + "]:checked").val();
        var comments = $('#comments' + subStageId).val();

        if (barcode == null || barcode == '') {
            toast('Invalid Attempt : ', 'Barcode Not Found');
            return;
        }

        if (statusCondition == null || statusCondition == '') {
            toast('Invalid Attempt : ', 'Status is Required');
            return;
        }

        if (comments == null || comments == '') {
            toast('Invalid Attempt : ', 'Comments is Required');
            return;
        }

        progressDiv.show();
        updateBtn.hide();
        failUpdateBtn.hide();

        var mainStage = stageId;
        var subStage = subStageId;

        if (statusCondition == '2') {
            subStage = parseInt(subStage) + 1;
        }

        if (subStage == '3' && statusCondition == '2') {
            mainStage = parseInt(mainStage) + 1;
        } else if (subStage == '5' && statusCondition == '2') {
            mainStage = parseInt(mainStage) + 1;
        } else if (subStage == '10' && statusCondition == '2') {
            mainStage = parseInt(mainStage) + 1;
        } else if (subStage == '11' && statusCondition == '2') {
            mainStage = parseInt(mainStage) + 1;
        }

        $.ajax({
            url: 'api/manage-stage.php',
            method: 'POST',
            data: {
                'type': 'update',
                'partCode': barcode,
                'statusCondition': statusCondition,
                'comments': comments,
                'mainStage': mainStage,
                'subStage': subStage,
                'fromMain': stageId,
                'fromSub': subStageId
            },
            success: function(result) {
                console.log(result);
                var res = $.parseJSON(result);
                if (res.state == true) {
                    if (res.data.status == 'success') {
                        progressDiv.hide();
                        successtoast('Success : ', 'Stage Updated');
                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                    } else {
                        toast('Failed : ', 'Unable to update... Try again later');
                        progressDiv.hide();
                        updateBtn.show();
                        failUpdateBtn.show();
                    }
                } else {
                    if (res.err.invalid == 'stage mismatch') {
                        toast('Cannot Update : ', 'Invalid Attempt');
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    }
                    updateBtn.removeAttr('disabled');
                }
            }
        });
    }

    function addToFailure(mainStage, subStage) {
        var fAccess = $('#failureAccessory' + subStage).val();
        var fComments = $('#fail_comments' + subStage).val();
        var barcode = $('#barcode').val();
        var progressDiv = $('#progressDiv' + subStage);
        var failUpdateBtn = $('#failureUpdate' + subStage);
        var updateBtn = $('#subStageUpdate' + subStage);

        if (fAccess == '') {
            toast('Warning : ', 'Select an Accessory');
            return;
        }
        if (fComments == '') {
            toast('Warning : ', 'Enter Failure Comments');
            return;
        }

        if (barcode == null || barcode == '') {
            toast('Invalid Attempt : ', 'Barcode Not Available');
            return;
        }

        updateBtn.hide();
        failUpdateBtn.hide();
        progressDiv.show();

        $.ajax({
            url: 'api/manage-stage.php',
            method: 'POST',
            data: {
                'failure': true,
                'partCode': barcode,
                'fAccessory': fAccess,
                'fComments': fComments,
                'mainStage': mainStage,
                'subStage': subStage,
            },
            success: function(result) {
                console.log(result);
                var res = $.parseJSON(result);
                if (res.state == true) {
                    if (res.data.status == 'success') {
                        successtoast('Success : ', 'Failure Updated');
                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                    } else {
                        toast('Failed : ', 'Unable to update... Try again later');
                        updateBtn.show();
                        failUpdateBtn.show();
                        progressDiv.hide();
                    }
                } else {
                    if (res.err.invalid == 'stage mismatch') {
                        toast('Cannot Update : ', 'Invalid Attempt');
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    }
                }
            }
        });
    }

    function formatDate(date) {
        var d = new Date(date);
        console.log(d.getMonth());
        var hh = d.getHours();
        var m = d.getMinutes();
        var s = d.getSeconds();
        var dd = "AM";
        var h = hh;
        if (h >= 12) {
            h = hh - 12;
            dd = "PM";
        }
        if (h == 0) {
            h = 12;
        }
        m = m < 10 ? "0" + m : m;

        s = s < 10 ? "0" + s : s;

        /* if you want 2 digit hours:
        h = h<10?"0"+h:h; */

        var pattern = new RegExp("0?" + hh + ":" + m + ":" + s);

        var replacement = h + ":" + m;
        /* if you want to add seconds
        replacement += ":"+s;  */
        replacement += " " + dd;

        return date.replace(pattern, replacement);
    }
</script>

</body>

</html>