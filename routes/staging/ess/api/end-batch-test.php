<?php

$rId = 18;

require '../../../includes/init.php';

$db = db();

extract($_POST);

// complete($_POST);

// $stmt = $db->prepare('INSERT INTO a_batch_test_log(batch_no,date,started_by,stopped_by,test_started_date,test_started_time,test_ended_date,test_ended_time)VALUES(?,?,?,?,?,?,?,?)');

// $stmt->bind_param('ssiissss', $batch_no, $bdate, $testStartedBy, $testEndedBy, $sdate, $stime, $edate, $etime);

// if ($stmt->execute()) {
//     $bn = mysqli_insert_id($db);

$query = '';

for ($i = 0; $i < count($enduutSerial); $i++) {
    if ($enduutSerial[$i] != "") {
        $us = $enduutSerial[$i];
        $ds = 'null';
        if (isset($enddisableSocket) && count($enddisableSocket) > 0) {
            if (in_array($i . '**1', $enddisableSocket)) {
                $ds = 1;
            }
        }
        $rs = 'null';
        switch ($i) {
            case 0:
                if (isset($endradioSocket0)) {
                    $rs = $endradioSocket0;
                }
                break;
            case 1:
                if (isset($endradioSocket1)) {
                    $rs = $endradioSocket1;
                }
                break;
            case 2:
                if (isset($endradioSocket2)) {
                    $rs = $endradioSocket2;
                }
                break;
            case 3:
                if (isset($endradioSocket3)) {
                    $rs = $endradioSocket3;
                }
                break;
            case 4:
                if (isset($endradioSocket4)) {
                    $rs = $endradioSocket4;
                }
                break;
            case 5:
                if (isset($endradioSocket5)) {
                    $rs = $endradioSocket5;
                }
                break;
            case 6:
                if (isset($endradioSocket6)) {
                    $rs = $endradioSocket6;
                }
                break;
            case 7:
                if (isset($endradioSocket7)) {
                    $rs = $endradioSocket7;
                }
                break;
            default:
                $rs = null;
                break;
        }
        $fds = 'null';
        if ($endfailureDetailSocket[$i] != '') {
            $fds = $endfailureDetailSocket[$i];
        }
        $query .= "UPDATE a_batch_test SET test_result=$rs, failure_detail=$fds, stopped_by='$testEndBy', test_ended_date='$tedate', test_ended_time='$tetime' WHERE uut_serial='$us';";

        // $created_by = $user['username'];
        // $result = mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES((SELECT id FROM a_part_accessories_staging WHERE serial_no='$us' AND parent=0 ORDER BY created_at DESC LIMIT 1),4,$rs,'$created_by')");

        $result = mysqli_query($db, "SELECT id FROM a_part_accessories_staging WHERE serial_no = '$us' AND parent=0 ORDER BY created_at DESC LIMIT 1");

        $row = mysqli_fetch_assoc($result);

        $partId = $row['id'];

        $userBy = $user['username'];

        if (mysqli_query($db, "UPDATE a_stage_updation SET status='$rs',updated_by='$userBy' WHERE part_id='$partId' AND stage=4 AND status = -1")) {
            $ns = $rs == '1' ? 5 : $nextStage[$i];
            mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES('$partId','$ns',-1,'$userBy')");
        }
    }
}

// $stmt = $db->prepare($query);

if (mysqli_multi_query($db, $query)) {
    complete(true);
} else {
    err(mysqli_error($db));
}
// } else {
//     err(mysqli_error($db));
// }
