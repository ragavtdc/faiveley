<?php

$rId = 18;

require '../../../includes/init.php';

$db = db();

extract($_POST);

$res = array();

check('serial', 'UUT Serial Number is required');

$result = mysqli_query($db, "SELECT batch_no FROM a_batch_test WHERE uut_serial='$serial' ORDER BY id DESC LIMIT 1");

if (mysqli_num_rows($result) != 0) {
    $row = mysqli_fetch_assoc($result);

    $batch = $row['batch_no'];

    complete($batch);
} else {
    err('UUT Serial - ' . $serial . ' not found');
}
