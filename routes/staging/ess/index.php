<?php

$rId = 18;
require '../../includes/init.php';

$db = db();

$res = array();
$failures = array();

$stmt = $db->prepare('SELECT username,emp_name FROM user_master WHERE FIND_IN_SET(7,role) AND state=1');

if ($stmt->execute()) {
    $result = $stmt->get_result();
    while ($rows = mysqli_fetch_assoc($result)) {
        $res[] = $rows;
    }
}

$bno = '';

if ($result = mysqli_query($db, "SELECT batch_no FROM a_batch_test ORDER BY batch_no desc LIMIT 1")) {
    $row = mysqli_fetch_assoc($result);
    $bno = $row['batch_no'];
    if (mysqli_num_rows($result) == 0) {
        $bno = '1';
    } else {
        $bno = $bno + 1;
    }
}

if ($resultt = mysqli_query($db, "SELECT id,failure_name FROM failure_master WHERE state=1")) {
    while ($rowss = mysqli_fetch_assoc($resultt)) {
        $failures[] = $rowss;
    }
}
head();
?>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-fire'></i> ESS Testing </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-fire fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>ESS Testing</a></li>
    </ul>
</div>
<?php if (isset($_SESSION['error'])) {
?>
    <h1 class='tile red text-center text-white'>
        <?php echo $_SESSION['error'];
        unset($_SESSION['error']);
        ?>
    </h1>
<?php }
?>

<?php if (isset($_SESSION['success'])) {
?>
    <h1 class='tile green text-center text-white'>
        <?php echo $_SESSION['success'];
        unset($_SESSION['success']);
        ?>
    </h1>
<?php }
?>

<div class='container'>
    <div class="row">
        <ul class="nav nav-pills mb-3 col-12" id="pills-tab" role="tablist">
            <li class="nav-item col-6">
                <a class="nav-link active text-center" id="pills-start-tab" data-toggle="pill" href="#pills-start" role="tab" aria-controls="pills-start" aria-selected="true">Start New Batch</a>
            </li>
            <li class="nav-item col-6">
                <a class="nav-link text-center" id="pills-end-tab" data-toggle="pill" href="#pills-end" role="tab" aria-controls="pills-end" aria-selected="false">End Old Batch</a>
            </li>

        </ul>
        <div class="tab-content container" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-start" role="tabpanel" aria-labelledby="pills-start-tab">
                <div class="tile">
                    <form id="batchDetailsFrm">
                        <!-- <div class="row">
                            <div class="col-4 offset-md-4">
                                <center>
                                    <label class='control-label'>Select Sockets Count</label>
                                    <select class="custom-select" id="socketsCount" name="socketsCount">
                                        <option selected>Select...</option>
                                        <option value="4">4</option>
                                        <option value="8">8</option>
                                    </select>
                                </center>
                            </div>
                            <br>
                        </div> -->
                        <div class="row">
                            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                                <label class='control-label'>Date</label>
                                <input class='form-control' type='date' placeholder='Select Date' name='bdate' id="bdate" required>
                            </div>
                            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                                <label class='control-label'>Batch No.</label>
                                <input class='form-control' type='text' value="<?php echo $bno ?>" name='batch_no' id="batch_no" required>
                            </div>
                            <div class="col-6">
                                <label class='control-label'>Operator (Test Start)</label>
                                <select class="custom-select" id="testStartedBy" name="testStartedBy">
                                    <option value="" selected disabled>Select...</option>
                                    <?php foreach ($res as $item) {
                                        echo '<option value="' . $item['username'] . '">' . $item['emp_name'] . '</option>';
                                    } ?>
                                </select>
                            </div>

                            <div class="col-6 hide">
                                <label class='control-label'>Operator (Test Stop)</label>
                                <select class="custom-select" id="testEndedBy" name="testEndedBy">
                                    <option value="" selected disabled>Select...</option>
                                    <?php
                                    // foreach ($res as $item) {
                                    // echo '<option value="' . $item['username'] . '">' . $item['emp_name'] . '</option>';
                                    // } 
                                    ?>
                                </select>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                                <label class='control-label'>Test Start Date</label>
                                <input class='form-control' type='date' name='sdate' id="sdate" required>
                            </div>
                            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                                <label class='control-label'>Test Start Time</label>
                                <input class='form-control' type='time' name='stime' id="stime" required>
                            </div>
                            <div class='form-group col-lg-6 col-md-6 col-sm-12 hide'>
                                <label class='control-label'>Test End Date</label>
                                <input class='form-control' type='date' name='edate' id="edate">
                            </div>
                            <div class='form-group col-lg-6 col-md-6 col-sm-12 hide'>
                                <label class='control-label'>Test End Time</label>
                                <input class='form-control' type='time' name='etime' id="etime">
                            </div>
                            <div class="col-6">
                                <label class='control-label'>Select Sockets Count</label>
                                <select class="custom-select" id="socketsCount" name="socketsCount">
                                    <option selected>Select...</option>
                                    <option value="4">4</option>
                                    <option value="8">8</option>
                                </select>
                            </div>
                        </div>
                        <!-- </form> -->
                        <!-- <form id="socketDetailsFrm"> -->
                        <div class="table-responsive hide" id="socketTable">
                            <table class="table table-striped">
                                <thead>
                                    <th></th>
                                    <th class="text-center">UUT Serial No.</th>
                                    <th></th>
                                    <th class="hide">Test Result</th>
                                    <th class="text-center hide">Failure Details</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width:70px">
                                            Socket 0
                                        </td>
                                        <td style="width: 300px;">
                                            <input class='form-control' type='text' name='uutSerial[]' id="uutSerial0" required>
                                        </td>
                                        <td>
                                            <input type="checkbox" value="0**1" name="disableSocket[]" id="disableSocket0">
                                            <label class="form-check-label" for="socket0">
                                                Disabled
                                            </label>
                                        </td>
                                        <td class="hide">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket0" id="radioSocket01" value="1">
                                                <label class="form-check-label" for="radioSocket01">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket0" id="radioSocket02" value="0">
                                                <label class="form-check-label" for="radioSocket02">Fail</label>
                                            </div>
                                        </td>
                                        <td class="hide">
                                            <select class="custom-select" id="failureDetailSocket0" name="failureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:80px">
                                            Socket 1
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='uutSerial[]' id="uutSerial1">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="1**1" name="disableSocket[]" id="disableSocket1">
                                            <label class="form-check-label" for="socket1">
                                                Disabled
                                            </label>
                                        </td>
                                        <td class="hide">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket1" id="radioSocket11" value="1">
                                                <label class="form-check-label" for="radioSocket11">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket1" id="radioSocket12" value="0">
                                                <label class="form-check-label" for="radioSocket12">Fail</label>
                                            </div>
                                        </td>
                                        <td class="hide">
                                            <select class="custom-select" id="failureDetailSocket1" name="failureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:80px">
                                            Socket 2
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='uutSerial[]' id="uutSerial2">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="2**1" name="disableSocket[]" id="disableSocket2">
                                            <label class="form-check-label" for="socket2">
                                                Disabled
                                            </label>
                                        </td>
                                        <td class="hide">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket2" id="radioSocket21" value="1">
                                                <label class="form-check-label" for="radioSocket21">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket2" id="radioSocket22" value="0">
                                                <label class="form-check-label" for="radioSocket22">Fail</label>
                                            </div>
                                        </td>
                                        <td class="hide">
                                            <select class="custom-select" id="failureDetailSocket2" name="failureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:80px">
                                            Socket 3
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='uutSerial[]' id="uutSerial3">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="3**1" name="disableSocket[]" id="disableSocket3">
                                            <label class="form-check-label" for="socket3">
                                                Disabled
                                            </label>
                                        </td>
                                        <td class="hide">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket3" id="radioSocket31" value="1">
                                                <label class="form-check-label" for="radioSocket31">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket3" id="radioSocket32" value="0">
                                                <label class="form-check-label" for="radioSocket32">Fail</label>
                                            </div>
                                        </td>
                                        <td class="hide">
                                            <select class="custom-select" id="failureDetailSocket3" name="failureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="trSocket4">
                                        <td style="width:80px">
                                            Socket 4
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='uutSerial[]' id="uutSerial4">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="4**1" name="disableSocket[]" id="disableSocket4">
                                            <label class="form-check-label" for="socket4">
                                                Disabled
                                            </label>
                                        </td>
                                        <td class="hide">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket4" id="radioSocket41" value="1">
                                                <label class="form-check-label" for="radioSocket41">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket4" id="radioSocket42" value="0">
                                                <label class="form-check-label" for="radioSocket42">Fail</label>
                                            </div>
                                        </td>
                                        <td class="hide">
                                            <select class="custom-select" id="failureDetailSocket4" name="failureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="trSocket5">
                                        <td style="width:80px">
                                            Socket 5
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='uutSerial[]' id="uutSerial5">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="5**1" name="disableSocket[]" id="disableSocket5">
                                            <label class="form-check-label" for="socket5">
                                                Disabled
                                            </label>
                                        </td>
                                        <td class="hide">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket5" id="radioSocket51" value="1">
                                                <label class="form-check-label" for="radioSocket51">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket5" id="radioSocket52" value="0">
                                                <label class="form-check-label" for="radioSocket52">Fail</label>
                                            </div>
                                        </td>
                                        <td class="hide">
                                            <select class="custom-select" id="failureDetailSocket5" name="failureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="trSocket6">
                                        <td style="width:80px">
                                            Socket 6
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='uutSerial[]' id="uutSerial6">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="6**1" name="disableSocket[]" id="disableSocket6">
                                            <label class="form-check-label" for="socket6">
                                                Disabled
                                            </label>
                                        </td>
                                        <td class="hide">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket6" id="radioSocket61" value="1">
                                                <label class="form-check-label" for="radioSocket61">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket6" id="radioSocket62" value="0">
                                                <label class="form-check-label" for="radioSocket62">Fail</label>
                                            </div>
                                        </td>
                                        <td class="hide">
                                            <select class="custom-select" id="failureDetailSocket6" name="failureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="trSocket7">
                                        <td style="width:80px">
                                            Socket 7
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='uutSerial[]' id="uutSerial7">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="7**1" name="disableSocket[]" id="disableSocket7">
                                            <label class="form-check-label" for="socket7">
                                                Disabled
                                            </label>
                                        </td>
                                        <td class="hide">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket7" id="radioSocket71" value="1">
                                                <label class="form-check-label" for="radioSocket71">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="radioSocket7" id="radioSocket72" value="0">
                                                <label class="form-check-label" for="radioSocket72">Fail</label>
                                            </div>
                                        </td>
                                        <td class="hide">
                                            <select class="custom-select" id="failureDetailSocket7" name="failureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="text-center">
                                <button id="updateBtn" class="btn btn-success" type="submit">START BATCH TEST</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade container" id="pills-end" role="tabpanel" aria-labelledby="pills-end-tab">
                <div class="tile">
                    <form id="endbatchDetailsFrm">
                        <div class="row">
                            <div class="col-6">
                                <label class='control-label'>Select Sockets Count</label>
                                <select class="custom-select" id="socketCount" name="socketCount">
                                    <option selected>Select...</option>
                                    <option value="4">4</option>
                                    <option value="8">8</option>
                                </select>
                            </div>
                            <div class="col-6">
                                <label class='control-label'>Operator (Test Stop)</label>
                                <select class="custom-select" id="testEndBy" name="testEndBy">
                                    <option value="" selected disabled>Select...</option>
                                    <?php
                                    foreach ($res as $item) {
                                        echo '<option value="' . $item['username'] . '">' . $item['emp_name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                                <label class='control-label'>Test End Date</label>
                                <input class='form-control' type='date' name='tedate' id="tedate" required>
                            </div>
                            <div class='form-group col-lg-6 col-md-6 col-sm-12'>
                                <label class='control-label'>Test End Time</label>
                                <input class='form-control' type='time' name='tetime' id="tetime" required>
                            </div>
                        </div>
                        <div class="table-responsive hide" id="endsocketTable">
                            <table class="table table-striped">
                                <thead>
                                    <th></th>
                                    <th class="text-center">UUT Serial No.</th>
                                    <th></th>
                                    <th>Batch No.</th>
                                    <th>Test Result</th>
                                    <th class="text-center">Failure Details</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width:70px">
                                            Socket 0
                                        </td>
                                        <td style="width: 300px;">
                                            <input class='form-control' type='text' name='enduutSerial[]' id="enduutSerial0" required onchange="getBatchNumber(0)">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="0**1" name="enddisableSocket[]" id="enddisableSocket0">
                                            <label class="form-check-label" for="socket0">
                                                Disabled
                                            </label>
                                        </td>
                                        <td style="width: 100px;">
                                            <input class="form-control" type="text" id="endBatchNo0" name="endBatchNo[]" placeholder="Batch">
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket0" id="endradioSocket01" value="1">
                                                <label class="form-check-label" for="endradioSocket01">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket0" id="endradioSocket02" value="0">
                                                <label class="form-check-label" for="endradioSocket02">Fail</label>
                                            </div>
                                        </td>
                                        <td>
                                            <select id="nextStage0" name="nextStage[]" class="browser-default custom-select">
                                                <option selected>Select Next Stage</option>
                                                <option value="1">ASSEMBLING</option>
                                                <option value="3">FUNCTIONAL TESTING</option>
                                            </select>
                                            <select class="custom-select" id="endfailureDetailSocket0" name="endfailureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:80px">
                                            Socket 1
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='enduutSerial[]' id="enduutSerial1" onchange="getBatchNumber(1)">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="1**1" name="enddisableSocket[]" id="enddisableSocket1">
                                            <label class="form-check-label" for="socket1">
                                                Disabled
                                            </label>
                                        </td>
                                        <td style="width: 100px;">
                                            <input class="form-control" type="text" id="endBatchNo1" name="endBatchNo[]" placeholder="Batch">
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket1" id="endradioSocket11" value="1">
                                                <label class="form-check-label" for="endradioSocket11">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket1" id="endradioSocket12" value="0">
                                                <label class="form-check-label" for="endradioSocket12">Fail</label>
                                            </div>
                                        </td>
                                        <td>
                                            <select id="nextStage1" name="nextStage[]" class="browser-default custom-select">
                                                <option selected>Select Next Stage</option>
                                                <option value="1">ASSEMBLING</option>
                                                <option value="3">FUNCTIONAL TESTING</option>
                                            </select>
                                            <select class="custom-select" id="endfailureDetailSocket1" name="endfailureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:80px">
                                            Socket 2
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='enduutSerial[]' id="enduutSerial2" onchange="getBatchNumber(2)">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="2**1" name="enddisableSocket[]" id="enddisableSocket2">
                                            <label class="form-check-label" for="socket2">
                                                Disabled
                                            </label>
                                        </td>
                                        <td style="width: 100px;">
                                            <input class="form-control" type="text" id="endBatchNo2" name="endBatchNo[]" placeholder="Batch">
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket2" id="endradioSocket21" value="1">
                                                <label class="form-check-label" for="endradioSocket21">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket2" id="endradioSocket22" value="0">
                                                <label class="form-check-label" for="endradioSocket22">Fail</label>
                                            </div>
                                        </td>
                                        <td>
                                            <select id="nextStage2" name="nextStage[]" class="browser-default custom-select">
                                                <option selected>Select Next Stage</option>
                                                <option value="1">ASSEMBLING</option>
                                                <option value="3">FUNCTIONAL TESTING</option>
                                            </select>
                                            <select class="custom-select" id="endfailureDetailSocket2" name="endfailureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:80px">
                                            Socket 3
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='enduutSerial[]' id="enduutSerial3" onchange="getBatchNumber(3)">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="3**1" name="enddisableSocket[]" id="enddisableSocket3">
                                            <label class="form-check-label" for="socket3">
                                                Disabled
                                            </label>
                                        </td>
                                        <td style="width: 100px;">
                                            <input class="form-control" type="text" id="endBatchNo3" name="endBatchNo[]" placeholder="Batch">
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket3" id="endradioSocket31" value="1">
                                                <label class="form-check-label" for="endradioSocket31">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket3" id="endradioSocket32" value="0">
                                                <label class="form-check-label" for="endradioSocket32">Fail</label>
                                            </div>
                                        </td>
                                        <td>
                                            <select id="nextStage3" name="nextStage[]" class="browser-default custom-select">
                                                <option selected>Select Next Stage</option>
                                                <option value="1">ASSEMBLING</option>
                                                <option value="3">FUNCTIONAL TESTING</option>
                                            </select>
                                            <select class="custom-select" id="endfailureDetailSocket3" name="endfailureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="endtrSocket4">
                                        <td style="width:80px">
                                            Socket 4
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='enduutSerial[]' id="enduutSerial4" onchange="getBatchNumber(4)">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="4**1" name="enddisableSocket[]" id="enddisableSocket4">
                                            <label class="form-check-label" for="socket4">
                                                Disabled
                                            </label>
                                        </td>
                                        <td style="width: 100px;">
                                            <input class="form-control" type="text" id="endBatchNo4" name="endBatchNo[]" placeholder="Batch">
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket4" id="endradioSocket41" value="1">
                                                <label class="form-check-label" for="endradioSocket41">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket4" id="endradioSocket42" value="0">
                                                <label class="form-check-label" for="endradioSocket42">Fail</label>
                                            </div>
                                        </td>
                                        <td>
                                            <select id="nextStage4" name="nextStage[]" class="browser-default custom-select">
                                                <option selected>Select Next Stage</option>
                                                <option value="1">ASSEMBLING</option>
                                                <option value="3">FUNCTIONAL TESTING</option>
                                            </select>
                                            <select class="custom-select" id="endfailureDetailSocket4" name="endfailureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="endtrSocket5">
                                        <td style="width:80px">
                                            Socket 5
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='enduutSerial[]' id="enduutSerial5" onchange="getBatchNumber(5)">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="5**1" name="enddisableSocket[]" id="enddisableSocket5">
                                            <label class="form-check-label" for="socket5">
                                                Disabled
                                            </label>
                                        </td>
                                        <td style="width: 100px;">
                                            <input class="form-control" type="text" id="endBatchNo5" name="endBatchNo[]" placeholder="Batch">
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket5" id="endradioSocket51" value="1">
                                                <label class="form-check-label" for="endradioSocket51">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket5" id="endradioSocket52" value="0">
                                                <label class="form-check-label" for="endradioSocket52">Fail</label>
                                            </div>
                                        </td>
                                        <td>
                                            <select id="nextStage5" name="nextStage[]" class="browser-default custom-select">
                                                <option selected>Select Next Stage</option>
                                                <option value="1">ASSEMBLING</option>
                                                <option value="3">FUNCTIONAL TESTING</option>
                                            </select>
                                            <select class="custom-select" id="endfailureDetailSocket5" name="endfailureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="endtrSocket6">
                                        <td style="width:80px">
                                            Socket 6
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='enduutSerial[]' id="enduutSerial6" onchange="getBatchNumber(6)">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="6**1" name="enddisableSocket[]" id="enddisableSocket6">
                                            <label class="form-check-label" for="socket6">
                                                Disabled
                                            </label>
                                        </td>
                                        <td style="width: 100px;">
                                            <input class="form-control" type="text" id="endBatchNo6" name="endBatchNo[]" placeholder="Batch">
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket6" id="endradioSocket61" value="1">
                                                <label class="form-check-label" for="endradioSocket61">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket6" id="endradioSocket62" value="0">
                                                <label class="form-check-label" for="endradioSocket62">Fail</label>
                                            </div>
                                        </td>
                                        <td>
                                            <select id="nextStage6" name="nextStage[]" class="browser-default custom-select">
                                                <option selected>Select Next Stage</option>
                                                <option value="1">ASSEMBLING</option>
                                                <option value="3">FUNCTIONAL TESTING</option>
                                            </select>
                                            <select class="custom-select" id="endfailureDetailSocket6" name="endfailureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="endtrSocket7">
                                        <td style="width:80px">
                                            Socket 7
                                        </td>
                                        <td>
                                            <input class='form-control' type='text' name='enduutSerial[]' id="enduutSerial7" onchange="getBatchNumber(7)">
                                        </td>
                                        <td>
                                            <input type="checkbox" value="7**1" name="enddisableSocket[]" id="enddisableSocket7">
                                            <label class="form-check-label" for="socket7">
                                                Disabled
                                            </label>
                                        </td>
                                        <td style="width: 100px;">
                                            <input class="form-control" type="text" id="endBatchNo7" name="endBatchNo[]" placeholder="Batch">
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket7" id="endradioSocket71" value="1">
                                                <label class="form-check-label" for="endradioSocket71">Pass</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="endradioSocket7" id="endradioSocket72" value="0">
                                                <label class="form-check-label" for="endradioSocket72">Fail</label>
                                            </div>
                                        </td>
                                        <td>
                                            <select id="nextStage7" name="nextStage[]" class="browser-default custom-select">
                                                <option selected>Select Next Stage</option>
                                                <option value="1">ASSEMBLING</option>
                                                <option value="3">FUNCTIONAL TESTING</option>
                                            </select>
                                            <select class="custom-select" id="endfailureDetailSocket7" name="endfailureDetailSocket[]">
                                                <option selected value="">Select...</option>
                                                <?php foreach ($failures as $item) {
                                                    echo '<option value="' . $item['id'] . '">' . $item['failure_name'] . '</option>';
                                                } ?>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="text-center">
                                <button id="endupdateBtn" class="btn btn-success" type="submit">END BATCH TEST</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

<?php foot();
?>

<!-- Page specific javascripts-->
<!-- Bootstrap core CSS -->
<link href='../../../js/mdb-css/bootstrap.min.css' rel='stylesheet'>
<!-- Material Design Bootstrap -->
<link href='../../../js/mdb-css/mdb.min.css' rel='stylesheet'>
<!-- Your custom styles ( optional ) -->
<link href='../../../js/mdb-css/style.css' rel='stylesheet'>
<!-- JQuery -->
<script type='text/javascript' src='../../../js/mdb-js/mdb.min.js'></script>
<script type='text/javascript' src='../../../js/mdb-js/addons/progressBar.js'></script>
<script type='text/javascript' src='../../../js/mdb-js/addons/progressBar.min.js'></script>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        for (let i = 0; i < 8; i++) {
            $('#disableSocket' + i).on('change', function(e) {
                if (e.target.checked == true) {
                    $('#uutSerial' + i).hide();
                    $("input[name='radioSocket" + i + "']").parent().hide();
                    $('#failureDetailSocket' + i).hide();
                } else {
                    $('#uutSerial' + i).show();
                    $("input[name='radioSocket" + i + "']").parent().show();
                    $('#failureDetailSocket' + i).show();
                }
            });
        }

        for (let i = 0; i < 8; i++) {
            $('#enddisableSocket' + i).on('change', function(e) {
                if (e.target.checked == true) {
                    $('#enduutSerial' + i).hide();
                    $("input[name='endradioSocket" + i + "']").parent().hide();
                    $('#endfailureDetailSocket' + i).hide();
                    $('#nextStage' + i).hide();
                    $('#endBatchNo' + i).hide();
                } else {
                    $('#enduutSerial' + i).show();
                    $("input[name='endradioSocket" + i + "']").parent().show();
                    $('#endfailureDetailSocket' + i).show();
                    $('#endBatchNo' + i).show();
                    $('#nextStage' + i).show();
                }
            });
        }

        for (let i = 0; i < 8; i++) {
            $("input[name='endradioSocket" + i).on('change', function(e) {
                if ($("input[name='endradioSocket" + i + "']:checked").val() == '0') {
                    $('#nextStage' + i).removeClass('disabled');
                    $('#endfailureDetailSocket' + i).removeClass('disabled');
                } else {
                    $('#nextStage' + i).addClass('disabled');
                    $('#endfailureDetailSocket' + i).addClass('disabled');
                }
            });
        }
    });

    $('#socketsCount').on('change', function(e) {
        var count = e.target.value;

        if (count == '4') {
            $('#socketTable').removeClass('hide');
            $('#trSocket4').addClass('hide');
            $('#trSocket5').addClass('hide');
            $('#trSocket6').addClass('hide');
            $('#trSocket7').addClass('hide');
        } else if (count == '8') {
            $('#socketTable').removeClass('hide');
            $('#trSocket4').removeClass('hide');
            $('#trSocket5').removeClass('hide');
            $('#trSocket6').removeClass('hide');
            $('#trSocket7').removeClass('hide');
        } else {
            $('#socketTable').addClass('hide');
        }
    });

    $('#socketCount').on('change', function(e) {
        var count = e.target.value;

        if (count == '4') {
            $('#endsocketTable').removeClass('hide');
            $('#endtrSocket4').addClass('hide');
            $('#endtrSocket5').addClass('hide');
            $('#endtrSocket6').addClass('hide');
            $('#endtrSocket7').addClass('hide');
        } else if (count == '8') {
            $('#endsocketTable').removeClass('hide');
            $('#endtrSocket4').removeClass('hide');
            $('#endtrSocket5').removeClass('hide');
            $('#endtrSocket6').removeClass('hide');
            $('#endtrSocket7').removeClass('hide');
        } else {
            $('#endsocketTable').addClass('hide');
        }
    });

    $('#batchDetailsFrm').on('submit', function(e) {
        e.preventDefault();

        $('#updateBtn').addClass('disabled');

        // submitBatchTest();

        sCount = $('#socketsCount').val();

        checkBarcodes(sCount);

    });

    $('#endbatchDetailsFrm').on('submit', function(e) {
        e.preventDefault();

        $('#endupdateBtn').addClass('disabled');

        // submitBatchTest();

        sCount = $('#socketCount').val();

        endcheckBarcodes(sCount);

    });

    function submitBatchTest() {
        $.ajax({
            url: 'api/new-batch-test.php',
            type: 'POST',
            data: $('#batchDetailsFrm').serialize(),
            success: function(result) {
                console.log(result);
                var res = $.parseJSON(result);
                if (res.state) {
                    successToast('Submitted Successfully');
                    $('#updateBtn').removeClass('disabled');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1500);
                } else {
                    toast('Failed : ', res.err);
                    $('#updateBtn').removeClass('disabled');
                    return;
                }
            }
        });
    }

    function endBatchTest() {
        $.ajax({
            url: 'api/end-batch-test.php',
            type: 'POST',
            data: $('#endbatchDetailsFrm').serialize(),
            success: function(result) {
                console.log(result);
                var res = $.parseJSON(result);
                if (res.state) {
                    successToast('Submitted Successfully');
                    $('#endupdateBtn').removeClass('disabled');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1500);
                } else {
                    toast('Failed : ', res.err);
                    $('#endupdateBtn').removeClass('disabled');
                    return;
                }
            }
        });
    }
</script>

<script>
    function checkBarcodes(sCount) {

        let serials = [];

        for (let i = 0; i < sCount; i++) {
            if ($('#disableSocket' + i).is(':checked')) {
                $('#uutSerial' + i).val("");
                continue;
            } else {
                serials.push($('#uutSerial' + i).val());
                // if ($("input[name='radioSocket" + i + "']:checked").val() == undefined) {
                //     toast('Test Result is Required for Entered Serials');
                //     $('#updateBtn').removeClass('disabled');
                //     return;
                // } else if ($("input[name='radioSocket" + i + "']:checked").val() == '0') {
                //     if ($('#failureDetailSocket' + i).val() == '') {
                //         toast('Failure Accessory is Required');
                //         $('#updateBtn').removeClass('disabled');
                //         return;
                //     }
                // }
            }
            // console.log($('#uutSerial' + i).val());
        }
        // console.log(serials);
        // return;

        // var barcode = $('#barcode').val();

        if (serials.length == 0) {
            toast('Invalid Attempt : ', 'Enter UUT Serials Numbers');
            return;
        }

        if (serials.length == 0) {
            toast('Invalid Attempt : ', 'Enter UUT Serials Numbers');
            return;
        }

        let err = 0;

        ajax(
            'api/check-serials.php', {
                'serials': serials,
            },
            function(data) {
                console.log(data);
                data.au.forEach(au => {
                    console.log(au);
                    if ($.inArray(au, serials) > -1) {
                        toast(au + ' Unit is not Ready for ESS Test');
                        // $('#updateBtn').removeClass('disabled');
                        err++;
                    }

                });
                data.una.forEach(una => {
                    if ($.inArray(una, serials)) {
                        toast(una + ' Unit is not Assembled');
                        // $('#updateBtn').removeClass('disabled');
                        err++;
                    }
                });
                data.unf.forEach(unf => {
                    if ($.inArray(unf, serials)) {
                        toast(unf + ' Unit not Found');
                        // $('#updateBtn').removeClass('disabled');
                        err++;
                    }
                });
                if (err > 0) {
                    $('#updateBtn').removeClass('disabled');
                    return;
                }
                // if (data) {
                submitBatchTest();
                // }
            }
        );
    }

    function endcheckBarcodes(sCount) {

        let serials = [];

        for (let i = 0; i < sCount; i++) {
            if ($('#enddisableSocket' + i).is(':checked')) {
                $('#enduutSerial' + i).val("");
                continue;
            } else {
                serials.push($('#enduutSerial' + i).val());
                if ($("input[name='endradioSocket" + i + "']:checked").val() == undefined) {
                    toast('Test Result is Required for Entered Serials');
                    $('#endupdateBtn').removeClass('disabled');
                    return;
                } else if ($("input[name='endradioSocket" + i + "']:checked").val() == '0') {
                    if ($('#endfailureDetailSocket' + i).val() == '') {
                        toast('Failure Accessory is Required');
                        $('#endupdateBtn').removeClass('disabled');
                        return;
                    }
                    if ($('#nextStage' + i).val() == '') {
                        toast('Next Stage is Required');
                        $('#endupdateBtn').removeClass('disabled');
                        return;
                    }
                }
            }
            // console.log($('#uutSerial' + i).val());
        }
        // console.log(serials);
        // return;

        // var barcode = $('#barcode').val();

        if (serials.length == 0) {
            toast('Invalid Attempt : ', 'Enter UUT Serials Numbers');
            return;
        }

        let err = 0;

        ajax(
            'api/check-serials.php', {
                'serials': serials,
            },
            function(data) {
                console.log(data);
                data.au.forEach(au => {
                    console.log(au);
                    if ($.inArray(au, serials) > -1) {
                        toast(au + ' Unit is not Ready for ESS Test');
                        // $('#updateBtn').removeClass('disabled');
                        err++;
                    }

                });
                data.una.forEach(una => {
                    if ($.inArray(una, serials)) {
                        toast(una + ' Unit is not Assembled');
                        // $('#updateBtn').removeClass('disabled');
                        err++;
                    }
                });
                data.unf.forEach(unf => {
                    if ($.inArray(unf, serials)) {
                        toast(unf + ' Unit not Found');
                        // $('#updateBtn').removeClass('disabled');
                        err++;
                    }
                });
                if (err > 0) {
                    $('#endupdateBtn').removeClass('disabled');
                    return;
                }
                // if (data) {
                endBatchTest();
                // }
            }
        );
    }

    function getBatchNumber(num) {
        let serial = $('#enduutSerial' + num).val();

        let endBatchNo = $('#endBatchNo' + num);

        ajax(
            'api/get-batch-num.php', {
                'serial': serial,
            },
            function(data) {
                endBatchNo.val(data);
            });
    }
</script>
</body>

</html>