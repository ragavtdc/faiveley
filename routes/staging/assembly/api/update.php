<?php

$rId = 15;

require '../../../includes/init.php';
require './algorithms.php';

check('type', 'Accessory cannot be empty');
check('parent', 'Select a valid Unit');
check('serial', 'Serial Number required');

$db = db();

extract($_POST);
$rework = 0;
function checkFailure($db, $serial)
{
    $stmt1 = mysqli_query($db, "SELECT * FROM a_rework_master WHERE reworked_part_serial_no= '$serial' ORDER by id DESC ");

    $count = mysqli_num_rows($stmt1);

    $row = mysqli_fetch_assoc($stmt1);

    $reworkstate = $row['state'];
    if ($count > 0 &&  $reworkstate == 1) {
        $GLOBALS['$rework'] = 1;
    }
    if ($reworkstate == 0) {
        $result = mysqli_query($db, "SELECT * FROM a_part_accessories_staging WHERE serial_no='$serial' AND testing=0");
        if (mysqli_num_rows($result) > 0) {
            err('Entered Serial Number is Already Marked as Failure');
        }
    }
    return false;
}

function checkExisting($db, $serial)
{
    if ($GLOBALS['$rework'] == 0) {
        $result = mysqli_query($db, "SELECT * FROM a_part_accessories_staging WHERE serial_no='$serial'");

        if (mysqli_num_rows($result) > 0) {
            err('Entered Serial Number is Already Used');
        }
    }
    return false;
}

checkFailure($db, $serial);

//checkExisting($db, $serial);

if ($parent == '0') {
    $result = mysqli_query($db, "SELECT algorithm FROM a_part_master WHERE part_id = $type AND state=1");
} else {
    $result = mysqli_query($db, "SELECT algorithm FROM a_accessories_master WHERE acc_id = $type AND state=1");
}

$row = mysqli_fetch_assoc($result);

$unitAlg = algo($row['algorithm'], $serial);

$stmt = $db->prepare("INSERT INTO a_part_accessories_staging(serial_no,type,parent,check_digit_a,check_digit_b,serial_alpha,num1,alpha1,num2,alpha2,inserted_by)VALUES(?,?,?,?,?,?,?,?,?,?,?)");

$stmt->bind_param('sssssssssss', $serial, $type, $parent, $unitAlg['check_digita'], $unitAlg['check_digitb'], $unitAlg['serial_alpha'], $unitAlg['num1'], $unitAlg['alpha1'], $unitAlg['num2'], $unitAlg['alpha2'], $user['username']);

if (!$stmt->execute()) {
    err(mysqli_error($db));
}

complete(true);
