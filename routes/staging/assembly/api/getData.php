<?php

$rId = 15;

require '../../../includes/init.php';
require './algorithms.php';
require_once 'rework.php';
$db = db();

extract($_POST);

function getAccs($db, $partId)
{

    $result = mysqli_query($db, "SELECT * FROM a_accessories_master where part_id = $partId and state = 1");

    $data = array();

    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }
    return $data;
}

function getCheckDigit($db, $partId)
{

    $result = mysqli_query($db, "SELECT check_digit FROM a_part_master where part_id = $partId and state = 1 LIMIT 1");

    $data = '';

    $row = mysqli_fetch_assoc($result);
    $data = $row['check_digit'];

    return $data;
}

function getAccsFromDb($db, $serial, $acc)
{
    $result = mysqli_query($db, "SELECT * FROM a_part_accessories_staging WHERE parent='$serial' AND testing=1");

    $data = array();

    while ($row = mysqli_fetch_assoc($result)) {
        $row['rework'] = getReworkDetails($db, $row['serial_no']);
        $data[] = $row;
    }
    $res = array();

    foreach ($acc as $ac) {
        $res[$ac['acc_id']] = null;
    }
    foreach ($data as $da) {
        $res[$da['type']] = $da;
    }

    return $res;
}

function getLastFailedStage($db, $serial)
{
    $result = mysqli_query($db, "SELECT su.stage,msm.main_stage_description FROM a_stage_updation su INNER JOIN main_stage_master msm ON msm.main_stage_no = su.stage WHERE su.part_id=(SELECT id FROM a_part_accessories_staging WHERE serial_no='$serial') ORDER BY su.id DESC LIMIT 1 OFFSET 1");

    if (mysqli_num_rows($result) != 1) {
        return 0;
    }
    $row = mysqli_fetch_assoc($result);

    return $row['stage'] . '***' . $row['main_stage_description'];
}

function getCurrentStage($db, $serial)
{
    $result = mysqli_query($db, "SELECT su.stage,su.status FROM a_stage_updation su WHERE su.part_id=(SELECT id FROM a_part_accessories_staging WHERE serial_no='$serial') ORDER BY su.id DESC LIMIT 1");

    if (mysqli_num_rows($result) != 1) {
        return 0;
    }
    $row = mysqli_fetch_assoc($result);

    if ($row['stage'] == '1' & $row['status'] == '-1') {
        return 1;
    }
    return 0;
}

$data = array();

$acc = getAccs($db, $partId);

$data['acc'] = $acc;

$checkD = getCheckDigit($db, $partId);

$data['checkDigit'] = $checkD;

$data['cameFrom'] = getLastFailedStage($db, $serial);
$data['current'] = getCurrentStage($db, $serial);

$result = mysqli_query($db, "SELECT * FROM a_part_accessories_staging WHERE serial_no='$serial' AND parent=0");

if (mysqli_num_rows($result) == 1) {
    $data['fresh'] = false;
    $data['unitAlg'] = $row = mysqli_fetch_assoc($result);
    $data['accAlg'] = getAccsFromDb($db, $row['id'], $acc);
} else {
    $data['fresh'] = true;
    if ($partId == '1') {
        $data['alg'] = algo(1, $serial);
    } elseif ($partId == '2') {
        $data['alg'] = algo(1, $serial);
    } elseif ($partId == '3') {
        $data['alg'] = algo(1, $serial);
    }
}

complete($data);

// echo json_encode($array);
