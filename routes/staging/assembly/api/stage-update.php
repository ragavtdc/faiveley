<?php

$rId = 15;

require '../../../includes/init.php';

check('serial', 'Unit Serial Number required');
check('partId', 'Selected Part is required');

$db = db();

extract($_POST);

function getAccsCount($db, $partId)
{
    $result = mysqli_query($db, "SELECT COUNT(*) AS count FROM a_accessories_master WHERE part_id=$partId");

    $row = mysqli_fetch_assoc($result);

    return $row['count'];
}

function getAssemblyCount($db, $serial, $partId)
{
    $result = mysqli_query($db, "SELECT COUNT(*) AS count FROM a_part_accessories_staging WHERE parent=(SELECT id FROM a_part_accessories_staging WHERE serial_no='$serial') AND testing=1 AND parent !=0 ORDER BY created_at DESC");

    $row = mysqli_fetch_assoc($result);

    return $row['count'];
}

function getPartId($db, $serialss)
{
    $result = mysqli_query($db, "SELECT id FROM a_part_accessories_staging WHERE serial_no='$serialss' AND parent=0");

    if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_assoc($result);
        return $row['id'];
    }
    err('Part not found');
}

$pid = getPartId($db, $serial);

$accsCount = getAccsCount($db, $partId);

$assemblyCount = getAssemblyCount($db, $serial, $partId);

$userBy = $user['username'];

if ($accsCount == $assemblyCount) {
    // if (mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES((SELECT id FROM a_part_accessories_staging WHERE serial_no='$serial' AND type=$partId AND parent=0 ORDER BY created_at DESC LIMIT 1),1,1,'$createdBy')")) {
    //     complete(true);
    // } else {
    //     err(mysqli_error($db));
    // }

    if (mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES('$pid',1,1,'$userBy')")) {
        if (mysqli_query($db, "INSERT INTO a_stage_updation(part_id,stage,status,created_by)VALUES('$pid',2,-1,'$userBy')")) {
            mysqli_close($db);
            complete(true);
        }
    }
} else {
    err('Assembly Not Completed');
}
