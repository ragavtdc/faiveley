<?php

$rId = 15;

require '../../../includes/init.php';
require './algorithms.php';
require 'rework.php';
$db = db();

extract($_POST);

function getCheckDigit($db, $id)
{

    $result = mysqli_query($db, "SELECT check_digit FROM a_accessories_master where acc_id = $id and state = 1 LIMIT 1");

    $data = '';

    $row = mysqli_fetch_assoc($result);
    $data = $row['check_digit'];

    return $data;
}

$result = mysqli_query($db, "SELECT * FROM a_accessories_master where acc_id = $id and state = 1");

$res = array();

$row = mysqli_fetch_assoc($result);

$type = $row['algorithm'];

$res['accs'] = algo($type, $serial);

$checkD = getCheckDigit($db, $id);

$res['checkDigit'] = $checkD;

$res['rework'] = getReworkDetails($db, $serial);

complete($res);
