<?php
// echo json_encode($_POST);
// die();
// convert("2966199002780000336000412259");
function convert($value)
{

    $mo = substr($value, 0, 7);
    $partno = substr($value, 7, 10);
    $rev_l1 = substr($value, 17, 2);
    $fill = substr($value, 19, 2);
    $rev_l2 = substr($value, 21, 3);
    $serial_no = substr($value, 24, 4);
    $num1 = ((int) $rev_l1);
    $i = fmod($num1, 33);

    $alpha1 = chr($i + 65);
    $num2 = ((int) $rev_l2);
    $i2 = fmod($num2, 33);

    $alpha2 = chr($i2 + 65);
    $serial_Alpha = $mo . $partno . $alpha1 . $fill . $alpha2 . $serial_no;
    $array = array("check_digita" => "$partno", "serial_alpha" => "$serial_Alpha", "num1" => "$num1", "num2" => "$num2", "alpha1" => "$alpha1", "alpha2" => "$alpha2", "check_digitb" => "$partno",);

    return $array;
}

function motherBoard($value)
{
    // $value = "0073380600414961003500562627";
    $i = 0;
    $val = str_split($value);
    $mo = substr($value, 0, 8);
    $partno = substr($value, 8, 10);
    $rev_l1 = substr($value, 18, 2);
    $fill = substr($value, 20, 2);
    $rev_l2 = substr($value, 22, 2);
    $serial_no = substr($value, 24, 4);
    $num1 = ((int) $rev_l1);
    $i = fmod($num1, 33);
    $alpha1 = chr($i + 65);
    $num2 = ((int) $rev_l2);
    $i2 = fmod($num2, 33);

    $alpha2 = chr($i2 + 65);

    $serial_Alpha = $mo . $partno . $alpha1 . $fill . $alpha2 . $serial_no;

    $array = array("check_digita" => "$partno", "serial_alpha" => "$serial_Alpha", "num1" => "$num1", "num2" => "$num2", "alpha1" => "$alpha1", "alpha2" => "$alpha2", "check_digitb" => "$partno",);

    return $array;
}

function powerSupply($value)
{
    // $value = "9557983002754510033020014615";
    $i = 0;
    $val = str_split($value);
    $mo = substr($value, 0, 7);
    $partno = substr($value, 7, 10);
    $rev_l1 = substr($value, 17, 2);
    $rev_l2 = substr($value, 19, 2);
    $fill = substr($value, 21, 3);
    $serial_no = substr($value, 24, 4);
    $num1 = ((int) $rev_l1);
    $i = fmod($num1, 33);
    $alpha1 = chr($i + 65);
    $num2 = ((int) $rev_l2);
    $i2 = fmod($num2, 33);
    if ($num2 >= 33)
        $alpha2 = chr($i2 + 65);
    else
        $alpha2 = $rev_l2;

    $serial_Alpha = $mo . $partno . $alpha1 . $alpha2 . $serial_no;

    $array = array("check_digita" => "$partno", "serial_alpha" => "$serial_Alpha", "num1" => "$num1", "num2" => "$num2", "alpha1" => "$alpha1", "alpha2" => "$alpha2", "check_digitb" => "$partno",);

    return $array;
}

function doo($value)
{
    // $value = "2389701000045654041000570061";
    $mo = substr($value, 0, 7);
    $partno = substr($value, 7, 10);
    $partno_int = (int) $partno;
    $rev_l1 = substr($value, 17, 2);
    $fill = substr($value, 19, 2);
    $rev_l2 = substr($value, 21, 3);
    $serial_no = substr($value, 24, 4);
    $num1 = ((int) $rev_l1);
    $i = fmod($num1, 33);

    $alpha1 = chr($i + 65);
    $num2 = ((int) $rev_l2);
    $i2 = fmod($num2, 33);

    $alpha2 = chr($i2 + 65);
    $serial_Alpha = $mo . $partno . $alpha1 . $fill . $alpha2 . $serial_no;

    $array = array("check_digita" => "$partno", "serial_alpha" => "$serial_Alpha", "num1" => "$num1", "num2" => "$num2", "alpha1" => "$alpha1", "alpha2" => "$alpha2", "check_digitb" => "$partno",);

    return $array;
}

// if (isset($type) && $type == '1') {
//     $res = convert($partCode);
// } elseif (isset($type) && $type == '2') {
//     $res = motherBoard($partCode);
// } elseif (isset($type) && $type == '3') {
//     $res = powerSupply($partCode);
// } elseif (isset($type) && $type == '4') {
//     $res = doo($partCode);
// }
// function algo($type, $serial)
// {

    extract($_GET);
    if ($type == 1) {
        $res = convert($serial);
    } elseif ($type == 2) {
        $res = motherBoard($serial);
    } elseif ($type == 3) {
        $res = powerSupply($serial);
    } elseif ($type == 4) {
        $res = doo($serial);
    }
    echo json_encode($res);
// }
