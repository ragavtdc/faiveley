<?php

$rId = 15;

require '../../../includes/init.php';
require './algorithms.php';

check('unitId', 'Select A Unit');
check('unitSerial', 'Serial Number is required');
check('allAcc', 'Accessories details required');

$db = db();

extract($_POST);

$result = mysqli_query($db, "SELECT algorithm FROM a_part_master WHERE part_id = $unitId AND state=1");


$type = mysqli_fetch_assoc($result);

$unitAlg = algo($type['algorithm'], $unitSerial);

$stmt = $db->prepare("INSERT INTO a_part_accessories_staging(serial_no,type,check_digit_a,check_digit_b,serial_alpha,num1,alpha1,num2,alpha2,inserted_by)VALUES(?,?,?,?,?,?,?,?,?,?)");

$stmt->bind_param('ssssssssss', $unitSerial, $unitId, $unitAlg['check_digita'], $unitAlg['check_digitb'], $unitAlg['serial_alpha'], $unitAlg['num1'], $unitAlg['alpha1'], $unitAlg['num2'], $unitAlg['alpha2'], $user['username']);

if (!$stmt->execute()) {
    err(mysqli_error($db));
}

$parentId = mysqli_insert_id($db);

$allAcc = explode(',', $allAcc);

foreach ($allAcc as $acc) {
    $id = explode(':', $acc)[0];
    $serial = explode(':', $acc)[1];

    $result = mysqli_query($db, "SELECT algorithm FROM a_accessories_master WHERE acc_id = $id AND state=1");

    $type = mysqli_fetch_assoc($result);

    $unitAlg = algo($type['algorithm'], $serial);

    $stmt = $db->prepare("INSERT INTO a_part_accessories_staging(serial_no,type,parent,check_digit_a,check_digit_b,serial_alpha,num1,alpha1,num2,alpha2,inserted_by)VALUES(?,?,?,?,?,?,?,?,?,?,?)");

    $stmt->bind_param('sssssssssss', $serial, $id, $parentId, $unitAlg['check_digita'], $unitAlg['check_digitb'], $unitAlg['serial_alpha'], $unitAlg['num1'], $unitAlg['alpha1'], $unitAlg['num2'], $unitAlg['alpha2'], $user['username']);

    $stmt->execute();
}

complete(true);
