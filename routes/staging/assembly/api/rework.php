<?php

function getReworkDetails($db, $partcode)
{
    // $partcode="2966199002780000336000412259";
    $stmt = $db->prepare("SELECT * FROM a_rework_master WHERE reworked_part_serial_no=? ORDER by id DESC ");

    $stmt->bind_param('s', $partcode);

    $data = array();
    $stmt->execute();
    $result = $stmt->get_result();
    $stmt->close();
    $count = $result->num_rows;
    if ($count === 0) return -1;
    return ($count - 1);
}
