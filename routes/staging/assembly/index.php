<?php

$rId = 15;
require '../../includes/init.php';

$db = db();

$parts = array();

if ($result = mysqli_query($db, "SELECT * FROM a_part_master WHERE state=1")) {
    while ($row = mysqli_fetch_assoc($result)) {
        $parts[] = $row;
    }
}

head();
?>

<div class='app-title mb-0'>
    <div>
        <h1><i class='fa fa-american-sign-language-interpreting'></i> Assembling </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-american-sign-language-interpreting fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>Assembling</a></li>
    </ul>
</div>
<?php if (isset($_SESSION['error'])) {
?>
    <h1 class='tile red text-center text-white'>
        <?php echo $_SESSION['error'];
        unset($_SESSION['error']);
        ?>
    </h1>
<?php }
?>

<?php if (isset($_SESSION['success'])) {
?>
    <h1 class='tile green text-center text-white'>
        <?php echo $_SESSION['success'];
        unset($_SESSION['success']);
        ?>
    </h1>
<?php }
?>

<!-- <div class='container'> -->
<div class="row mt-2">
    <div class='form-group col-lg-6 col-md-6 col-sm-12'>
        <label class='control-label'>Select Part</label>
        <select class="custom-select" name="selectedPart" id="selectedPart" onchange="$('#parts_table').show();$('#part_description').html($('#selectedPart option:selected').html());">
            <option value="">Select...</option>
            <?php foreach ($parts as $item) {
                echo '<option value="' . $item['part_id'] . '">' . $item['part_name'] . '</option>';
            } ?>
        </select>
        <div class=' col-sm-12 text-center'>
            <div id='progressDiv' style='display: none;'>
                <div class='progress'>
                    <div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='75' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="tile">
    <!-- <div class="row"> -->
    <table class="table table-responsive">
        <thead>
            <tr>
                <th>Check Digit A</th>
                <th>Part Description</th>
                <th>Serial Number</th>
                <th>Check Digit B</th>
                <th>Serial Alpha Numeric</th>
                <th>Num 1</th>
                <th>Alpha 1</th>
                <th>Num 2</th>
                <th>Alpha 2</th>
                <th>Rework</th>
            </tr>
        </thead>
        <tbody id="parts_table" style="display: none;">
            <tr id="trow">
                <td>
                    <p id="check_digit_a"></p>
                </td>
                <td>
                    <p id="part_description"></p>
                </td>
                <td>
                    <input class='form-control' type='text' placeholder='Enter Serial Number' name='unit_no' id="unit_no" onchange="fillUnit('1','0027800003')" required autofocus>
                    <p id="paraUnit"></p>
                </td>
                <td>
                    <p id="check_digit_b"></p>
                </td>
                <td>
                    <p id="serial_alpha_no"></p>
                </td>
                <td>
                    <p id="num1"></p>
                </td>
                <td>
                    <p id="alpha1"></p>
                </td>
                <td>
                    <p id="num2"></p>
                </td>
                <td>
                    <p id="alpha2"></p>
                </td>
                <td>
                    <p id="reworkTd"></p>
                </td>
                <td>
                    <button class="btn btn-success" style="display: none;" id="assembleBtn" onclick="assembleUnit()">ASSEMBLE</button>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-12 text-center">
            <button class="btn btn-success disabled" style="display: none;" id="submitBtn" onclick="newSubmit()">SUBMIT</button>
            <button class="btn btn-success" style="display: none;" data-toggle="modal" data-target="#stageSelectionModal" id="reworkBtn" onclick="reworked()">SUBMIT</button>
        </div>
    </div>
    <!-- </div> -->

    <!-- Modal -->
    <div class="modal fade" id="stageSelectionModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">STAGE SELECTION</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6 class="m-4 text-center" id="cameFrom"></h6>
                    <br>
                    <br>
                    <form id="stageUpdateFrm">

                        <input type="hidden" id="serialss" name="serialss" value="">

                        <input type="hidden" name="cStage" value="1">

                        <input type="hidden" name="cStageState" value="1">

                        <input type="hidden" name="insertion" value="false">

                        <select name="nextStage" class="browser-default custom-select">
                            <option selected>Select Next Stage</option>
                            <option value="2">HV IR TESTING</option>
                            <option value="3">FUNCTIONAL TESTING</option>
                            <option value="4">ESS TESTING</option>
                            <option value="6">PRE DISPATCH INSPECTION</option>
                            <option value="7">ACCEPTED (QA)</option>
                        </select>

                        <button type="submit" class="btn btn-primary">Submit</button>

                    </form>

                    <!-- Group of default radios - option 2 -->
                    <!-- <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="failRadio" name="passFailRadio" checked>
                        <label class="custom-control-label" for="failRadio">FAIL</label>
                    </div> -->

                    <!-- Basic dropdown -->

                    <!-- Basic dropdown -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- </div> -->

<?php foot();
?>

<!-- Page specific javascripts-->
<!-- Bootstrap core CSS -->
<link href='../../../js/mdb-css/bootstrap.min.css' rel='stylesheet'>
<!-- Material Design Bootstrap -->
<link href='../../../js/mdb-css/mdb.min.css' rel='stylesheet'>
<!-- Your custom styles ( optional ) -->
<link href='../../../js/mdb-css/style.css' rel='stylesheet'>
<!-- JQuery -->
<script type='text/javascript' src='../../../js/mdb-js/mdb.min.js'></script>
<script type='text/javascript' src='../../../js/mdb-js/addons/progressBar.js'></script>
<script type='text/javascript' src='../../../js/mdb-js/addons/progressBar.min.js'></script>
<script src='../../../js/plugins/plotly-latest.min.js' type='text/javascript'></script>
<script src='../../../js/plugins/bootstrap-notify.min.js'></script>
<script src='../../../js/toast.js'></script>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    function newSubmit() {
        let serial = $('#unit_no').val();
        let selectedPartId = $('#selectedPart').val();

        if (serial.trim() == '') {
            toast('Unit Serial Number is required');
            return;
        }

        if (selectedPartId == '') {
            toast('Select an Unit from the list');
            return;
        }

        let data = {
            'serial': serial,
            'partId': selectedPartId
        }
        let func = (data) => {
            if (data) {
                successToast('Stage Updated');
                setTimeout(() => {
                    window.location.reload();
                }, 1500);
            } else {
                toast('Try Again Later');
            }
        }
        ajax('api/stage-update.php', data, func);
    }

    let acc;

    let uFresh = false;
    let current = 0;

    function fillUnit(algorithm, checkDigit) {

        let serialNumber = $('#unit_no').val();

        $('#serialss').val(serialNumber);

        if (serialNumber.trim() == '') {
            toast('Invalid Attempt : ', 'Enter serial number');
            return;
        }
        let selectedPartId = $('#selectedPart').val();
        $.ajax({
            url: 'api/getData.php',
            type: 'POST',
            data: {
                'serial': serialNumber,
                'type': algorithm,
                'partId': selectedPartId,
            },
            success: function(result) {
                var res = $.parseJSON(result);
                if (res.state == true) {
                    var data = res.data;
                    console.log(data);
                    acc = data.acc;
                    // $('#paraUnit').html(serialNumber);
                    // $('#unit_no').hide();
                    current = data.current;
                    if (data.cameFrom == '0') {
                        uFresh = true;
                    } else {
                        $('#cameFrom').html('Previous Stage : ' + data.cameFrom.split('***')[1]);
                    }
                    if (data.fresh) {

                        if (data.alg.check_digita == data.checkDigit) {
                            newUnit(data.acc, data.alg, data.rework);
                            $('#paraUnit').html(serialNumber);
                            $('#unit_no').hide();
                        } else {
                            toast('You are scanning the wrong part');
                            return;
                        }
                    } else {
                        if (data.unitAlg.check_digit_a == data.checkDigit) {
                            oldUnit(data.unitAlg, data.acc, data.accAlg);
                            $('#paraUnit').html(serialNumber);
                            $('#unit_no').hide();
                        } else {
                            toast('You are scanning the wrong part');
                            return;
                        }
                    }

                    if (fails == 0 && uFresh == false && current == 1) {
                        $('#reworkBtn').show();
                        $('#reworkBtn').removeClass('disabled');
                    }
                    // $('#part_description').val(res.data.);
                    // $('#serial_alpha_no').html(res.data.serial_alpha);
                    // $('#check_digit_a').html(res.data.check_digita);
                    // $('#check_digit_b').html(res.data.check_digitb);
                    // $('#num1').html(res.data.num1);
                    // $('#alpha1').html(res.data.alpha1);
                    // $('#num2').html(res.data.num2);
                    // $('#alpha2').html(res.data.alpha2);
                }
                // loadExistingDetails(serialNumber);
            }
        });
    }

    function newUnit(a, alg, re) {

        let html = '';
        a.forEach(acc => {
            html += '<tr id="trow' + acc.acc_id + '" style="display:none"><td><p id="check_digit_a' + acc.acc_id + '"></p></td><td><p id="part_description' + acc.acc_id + '">' + acc.acc_name + '</p></td>';
            html += '<td><input class="form-control" type="text" placeholder="Enter Serial Number" name="serial_no[]" id="serial_no' + acc.acc_id + '" onchange="fillFields(\'' + acc.acc_id + '\',\'\',\'' + acc.algorithm + '\')" required><p id="para' + acc.acc_id + '"></p></td>';
            html += '<td><p id="check_digit_b' + acc.acc_id + '"></p></td><td><p id="serial_alpha_no' + acc.acc_id + '"></p></td>';
            html += '<td><p id="num1' + acc.acc_id + '"></p></td><td><p id="alpha1' + acc.acc_id + '"></p></td><td><p id="num2' + acc.acc_id + '"></p></td><td><p id="alpha2' + acc.acc_id + '"></p></td><td><p id="reworkTd' + acc.acc_id + '"></p></td><td><button class="btn btn-success" id="updateBtn' + acc.acc_id + '" onclick="assembleAcc(' + acc.acc_id + ',' + acc.algorithm + ')">ASSEMBLE</button></td></tr>';
        });
        $('#assembleBtn').show();
        // $('#submitBtn').show();
        $('#parts_table').append(html);

        $('#serial_alpha_no').html(alg.serial_alpha);
        $('#check_digit_a').html(alg.check_digita);
        $('#check_digit_b').html(alg.check_digitb);
        $('#num1').html(alg.num1);
        $('#alpha1').html(alg.alpha1);
        $('#num2').html(alg.num2);
        $('#alpha2').html(alg.alpha2);
        $('#reworkTd').html(reWorkText(re));
    }

    let fails = 0;

    function oldUnit(alg, a, accAlg) {
        $('#serial_alpha_no').html(alg.serial_alpha);
        $('#check_digit_a').html(alg.check_digit_a);
        $('#check_digit_b').html(alg.check_digit_b);
        $('#num1').html(alg.num1);
        $('#alpha1').html(alg.alpha1);
        $('#num2').html(alg.num2);
        $('#alpha2').html(alg.alpha2);
        let html = '';
        a.forEach(acc => {

            html += '<tr id="oldDiv' + acc.acc_id + '" class="red lighten-3 text-white"><td><p id="check_digit_a' + acc.acc_id + '"></p></td><td><p id="part_description' + acc.acc_id + '">' + acc.acc_name + '</p></td>';
            html += '<td><input class="form-control" type="text" placeholder="Enter Serial Number" name="serial_no[]" id="serial_no' + acc.acc_id + '" onchange="fillFields(\'' + acc.acc_id + '\',\'\',\'' + acc.algorithm + '\')" required><p id="para' + acc.acc_id + '"></p></td>';
            html += '<td><p id="check_digit_b' + acc.acc_id + '"></p></td><td><p id="serial_alpha_no' + acc.acc_id + '"></p></td>';
            html += '<td><p id="num1' + acc.acc_id + '"></p></td><td><p id="alpha1' + acc.acc_id + '"></p></td><td><p id="num2' + acc.acc_id + '"></p></td><td><p id="alpha2' + acc.acc_id + '"></p></td><td><p id="reworkTd' + acc.acc_id + '"></p></td><td><button class="btn btn-success" id="updateBtn' + acc.acc_id + '" onclick="update(' + acc.acc_id + ',' + alg.id + ')">UPDATE</button></td></tr>';
        });
        $('#parts_table').append(html);

        for (const key in accAlg) {

            const alg = accAlg[key];
            console.log(alg);
            if (alg === null) {
                fails++;
                continue;
            }
            $('#oldDiv' + alg.type).removeClass('red lighten-3 text-white');
            $('#updateBtn' + alg.type).hide();
            $('#para' + alg.type).html(alg.serial_no);
            $('#serial_no' + alg.type).hide();

            $('#check_digit_a' + alg.type).html(alg.check_digit_a);
            $('#serial_no' + alg.type).val(alg.serial_no);
            $('#serial_alpha_no' + alg.type).html(alg.serial_alpha);
            $('#check_digit_b' + alg.type).html(alg.check_digit_b);
            $('#num1' + alg.type).html(alg.num1);
            $('#alpha1' + alg.type).html(alg.alpha1);
            $('#num2' + alg.type).html(alg.num2);
            $('#alpha2' + alg.type).html(alg.alpha2);
            $('#reworkTd' + alg.type).html(reWorkText(alg.rework));
            console.log(alg.type, alg.rework);
        }

        if (fails == 0) {

            if (uFresh) {
                $('#submitBtn').show();
                $('#submitBtn').removeClass('disabled');
            }
        }
    }

    function fillFields(id) {

        $('#updateBtn' + id).attr('disabled', false);
        let serialNumber = $('#serial_no' + id).val();

        if (serialNumber.trim() == '') {
            toast('Invalid Attempt', 'Enter a valid serial number');
            return;
        }

        $.ajax({
            url: 'api/getAccAlg.php',
            type: 'POST',
            data: {
                'id': id,
                'serial': serialNumber
            },
            success: function(result) {
                var res = $.parseJSON(result);
                console.log(res);
                if (res.state == true) {
                    var accs = res.data.accs;
                    var checkDigit = res.data.checkDigit;

                    if (accs.check_digita == checkDigit) {
                        $('#serial_alpha_no' + id).html(accs.serial_alpha);
                        $('#check_digit_a' + id).html(accs.check_digita);
                        $('#check_digit_b' + id).html(accs.check_digitb);
                        $('#num1' + id).html(accs.num1);
                        $('#alpha1' + id).html(accs.alpha1);
                        $('#num2' + id).html(accs.num2);
                        $('#alpha2' + id).html(accs.alpha2);

                        $('#reworkTd' + id).html(reWorkText(res.data.rework));
                        if (res.data.rework == 0) {
                            $('#updateBtn' + id).attr('disabled', true);
                        } else {
                            $('#updateBtn' + id).attr('disabled', false);
                        }

                    } else {
                        toast('You are scanning the wrong part');
                        $('#updateBtn' + id).attr('disabled', true);
                        return;
                    }
                    // $('#part_description').val(res.data.);
                }
            }
        });
    }

    function update(type, parent) {
        let serial = $('#serial_no' + type).val();

        if (serial.trim() == '') {
            toast('Invalid Serial');
            return;
        }
        let data = {
            'type': type,
            'parent': parent,
            'serial': serial
        }

        const func = (data) => {
            $('#updateBtn' + type).hide();
            $('#para' + type).html(serial);
            $('#serial_no' + type).hide();
            // if (acc.length == type) {
            //     $('#submitBtn').show();
            //     $('#submitBtn').removeClass('disabled');
            // }
            // $('#trow' + type).next().show();
            successToast('Assembled Successfully');
            $('#oldDiv' + type).removeClass('red lighten-3 text-white');
            fails = fails - 1;
            if (fails == 0) {

                if (uFresh) {
                    $('#submitBtn').show();
                    $('#submitBtn').removeClass('disabled');
                } else {
                    $('#reworkBtn').show();
                    $('#reworkBtn').removeClass('disabled');
                }
            }
            // setTimeout(() => {
            //     window.location.reload();
            // }, 1500);
        }
        ajax('api/update.php', data, func);
    }

    function assembleUnit() {
        let serial = $('#unit_no').val();

        let type = $('#selectedPart').val();

        if (serial.trim() == '') {
            toast('Invalid Serial');
            return;
        }
        let data = {
            'type': type,
            'parent': '0',
            'serial': serial
        }

        const func = (data) => {
            $('#assembleBtn').hide();

            $('#trow').next().show();
            $('#submitBtn').show();
            successToast('Assembled');
            // setTimeout(() => {
            //     window.location.reload();
            // }, 1500);
        }
        ajax('api/update.php', data, func);
    }

    let assembledAcc = 0;

    function assembleAcc(id, algorithm) {
        let serial = $('#serial_no' + id).val();
        let unitSerial = $('#unit_no').val();

        if (serial.trim() == '') {
            toast('Invalid Serial');
            return;
        }
        let data = {
            'type': id,
            'parentSerial': unitSerial,
            'serial': serial,
            'algorithm': algorithm
        }

        const func = (data) => {
            $('#updateBtn' + id).hide();
            $('#serial_no' + id).hide();
            $('#para' + id).html(serial);
            assembledAcc++;
            if (acc.length == assembledAcc) {
                $('#submitBtn').removeClass('disabled');
                $('#reworkBtn').removeClass('disabled');
            } else {
                $('#trow' + id).next().show();
            }
            successToast('Assembled Successfully');
            // setTimeout(() => {
            //     window.location.reload();
            // }, 1500);
        }
        ajax('api/assemble.php', data, func);
    }

    $('#stageUpdateFrm').submit((e) => {
        e.preventDefault();
        const func = (data) => {
            successToast('Updated Successfully');
            setTimeout(() => {
                window.location.reload();
            }, 1500);
        }
        ajax('../api/stage-update.php', $('#stageUpdateFrm').serialize(), func);
    });

    function reWorkText(r) {
        if (r == '-1') return '-';
        if (r == '0') return '<strong class="white text-danger">Failure Part</strong>';
        if (r == undefined || r == null) return '-';
        return '<b class="white text-warning text-center">' + r + ' reworks</b>';

    }
</script>
</body>

</html>