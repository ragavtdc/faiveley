<?php

$rId = 17;
require '../../includes/init.php';

$db = db();

head();
?>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-database'></i> Functional Testing </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-database fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>Functional Testing </a></li>
    </ul>
</div>
<div class="container">

    <div class="row tile">
        <div class='form-group col-lg-6 col-md-6 col-sm-12'>
            <!-- <label class='control-label'>Barcode Scan</label> -->
            <input class='form-control' type='text' placeholder='Scan Barcode' name='serial' id="serial" required autofocus onkeyup="if (event.keyCode === 13) {fetchAcc();}">
        </div>
        <div class="form-group col-lg-6 col-md-6 col-sm-12">
            <button id="getInfoBtn" onclick="fetchAcc()" class="btn waves-effect waves-light btn-sm btn-info" data-toggle="tooltip" data-original-title="Get Unit Info">Get Unit Info</button>
        </div>
    </div>
</div>

<div class=' col-sm-12 text-center'>
    <div id='progressDiv' style='display: none;'>
        <div class='progress'>
            <div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='75' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div>
        </div>
    </div>
</div>

<div class="container tile" id="accDiv" style="display: none;">
    <ul class="list-group" id="accList">

    </ul>

    <!-- <select id="nextStage" class="browser-default custom-select" required>
        <option selected>Select Next Stage</option>
        <option value="1">ASSEMBLING</option>
        <option value="4">ESS TEST</option>
    </select> -->

    <div class="text-center mt-4">
        <button class="btn waves-effect waves-light btn-sm ml-3 btn-danger" data-toggle="tooltip" data-original-title="Mark as Failed" id="failBtn" onclick="updateStage(0)"><i class="material-icons text-danger"></i>SAVE STAGE AS FAILED</button>
        <button class="btn waves-effect waves-light btn-sm ml-3 btn-success" data-toggle="tooltip" data-original-title="Mark as Passed" id="passBtn" onclick="updateStage(1)"><i class="material-icons text-success"></i>SAVE STAGE AS PASSED</button>
    </div>
</div>

<?php foot();
?>

<link href='<?php echo $path ?>js/mdb-css/bootstrap.min.css' rel='stylesheet'>

<script type='text/javascript' src='<?php echo $path ?>js/mdb-js/addons/progressBar.js'></script>
<script type='text/javascript' src='<?php echo $path ?>js/mdb-js/addons/progressBar.min.js'></script>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    let selectedPartVal;
    var progress = $('#progressDiv');
    var accDiv = $('#accDiv');

    function fetchAcc() {
        selectedPartVal = $('#serial').val();
        accDiv.hide();
        progress.show();
        var data = {
            'partId': selectedPartVal
        }
        var func = (data) => {
            progress.hide();
            accDiv.show();
            console.log(data);
            let html = '';
            html += '<li class="list-group-item d-flex justify-content-between align-items-center">' + data.unit.part_name + '</li>';
            $('#serial').val(data.unit.serial_no);
            data.acc.forEach(ac => {
                html += '<li id="list' + ac.acc_id + '" class="list-group-item d-flex justify-content-between align-items-center">';
                html += ac.serial_no + ' - ' + ac.acc_name;
                html += '<div><button class="btn btn-flat btn-sm p-1 waves-effect waves-light" onclick="markAsFailed(' + ac.id + ',' + ac.acc_id + ',\'' + ac.serial_no + '\',\'' + ac.acc_name + '\')">Mark As Failure</button></div>';
                html += '</li>';
            });
            $('#accList').html(html);

        };
        ajax('api/fetchAcc.php', data, func);
    }

    function updateStage(status) {
        progress.show();
        selectedPartVal = $('#serial').val();
        let data = {
            'status': status,
            'serial': selectedPartVal
        }
        let func = (data) => {
            if (data) {
                successToast('Stage Updated');
                progress.hide();
                setTimeout(() => {
                    window.location.reload();
                }, 1500);
            } else {
                toast("Try again later...");
                progress.hide();
            }
        }
        ajax('api/updateStage.php', data, func);
    }

    function markAsFailed(id, accId, serial, name) {
        let data = {
            'id': id,
            'accId': accId,
            'serial': serial,
            'name': name
        }
        let func = (data) => {
            if (data) {
                successToast('Marked As Failure');
                $('#passBtn').hide();
                $('#list' + accId).removeClass('d-flex');
                $('#list' + accId).hide();
                progress.hide();
            } else {
                toast('Try Again Later...');
                progress.hide();
            }
        }
        progress.show();
        ajax('api/markFailure.php', data, func);
    }
</script>
</body>

</html>