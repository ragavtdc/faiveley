<?php
$rId = 20;
require '../../includes/init.php';
require_once '../../print/vendor/autoload.php';

check('serial', 'Serial no Required');
check('checked', 'Checked by required');

$db = db();

extract($_GET);

$result = mysqli_query($db, "SELECT a.id,a.type,b.part_name,b.part_description FROM a_part_accessories_staging a INNER JOIN a_part_master b ON a.type = b.part_id WHERE a.serial_no = '$serial' AND a.parent=0 ORDER BY a.created_at DESC LIMIT 1");
if (mysqli_num_rows($result) != 1) {
    err('No part found');
}
$row = mysqli_fetch_assoc($result);

$partId = $row['id'];
$partName = $row['part_name'];
$partDesc = $row['part_description'];


$result = mysqli_query($db, "SELECT id,checklist FROM a_checklist_master");

$rows = array();

while ($row = mysqli_fetch_assoc($result)) {
    $rows[] = $row;
}

$result = mysqli_query($db, "SELECT checklist_id,fpd,remarks FROM a_checklist_log WHERE part_id = '$partId' AND fpd IS NOT NULL");

$fpds = array();

while ($row = mysqli_fetch_assoc($result)) {
    $fpds[$row['checklist_id']] = $row;
}

$result = mysqli_query($db, "SELECT checklist_id,qad,remarks FROM a_checklist_log WHERE part_id = '$partId' AND qad IS NOT NULL");

$qads = array();

while ($row = mysqli_fetch_assoc($result)) {
    $qads[$row['checklist_id']] = $row;
}


for ($i = 0; $i < count($rows); $i++) {
    $e = $rows[$i]['id'];
    if (isset($fpds[$e])) {
        $rows[$i]['fpd'] = $fpds[$e]['fpd'] == 1 ? 'Yes' : 'No';
        $rows[$i]['fRemark'] = $fpds[$e]['remarks'];
    } else {
        $rows[$i]['fpd'] = '-';
        $rows[$i]['fRemark'] = '-';
    }
    if (isset($qads[$e])) {
        $rows[$i]['qad'] = $qads[$e]['qad'] == 1 ? 'Yes' : 'No';
        $rows[$i]['qRemark'] = $qads[$e]['remarks'];
    } else {
        $rows[$i]['qad'] = '-';
        $rows[$i]['qRemark'] = '-';
    }
}

$result = mysqli_query($db, "SELECT * FROM a_stage_updation WHERE part_id = '$partId' AND stage = 6 ORDER BY id desc limit 1");
if (mysqli_num_rows($result) != 1) {
    err('This part not reached stage pre-dispatch stage!');
}
$r = mysqli_fetch_assoc($result);
$checkedBy = $r['updated_by'];



// complete($rows);

$mpdf = new \Mpdf\Mpdf([
    'setAutoTopMargin' => 'stretch',
    'autoMarginPadding' => 0,
    'mode' => 'utf-8',
    'format' => 'A4-L',
    'orientation' => 'L'
]);

$mpdf->SetHTMLHeader(' 
');
$WriteHTML = '
<style>
    table,
    th,
    td {
        border: 1px solid grey;
        border-collapse: collapse;
        text-align: center;
        vertical-align: middle;
    }
</style>
<header style="text-align:center; margin-bottom:10px">
        <img height="80" src="../../../images/logo-wabtec.png" />
</header>
<table width="100%">
<thead>
    <tr class="text-center">
        <th colspan="10" style="font-weight: bold;font-size: 24px;">
            PRE DISPATCH INSPECTION CHECK LIST
        </th>
    </tr>
    <tr>
        <th colspan="1">
            Serial
        </th>
        <td style="text-align: start;" colspan="5" id="partName">
        ' . $serial . '
        </td>
    </tr>
    <tr>
        <th colspan="1">
            Part Name
        </th>
        <td style="text-align: start;" colspan="5" id="partName">
        ' . $partName . '
        </td>
    </tr>
    <tr>
        <th colspan="1">
            Description
        </th>
        <td style="text-align: start;" colspan="5" id="desc">
        ' . $partDesc . '
        </td>
    </tr>
    <tr>
        <th colspan="1">
            Date
        </th>
        <td style="text-align: start;" colspan="5" id="curdate">
        ' . date("d-m-Y") . '
        </td>
    </tr>
    <tr>
        <th>
            SL No.
        </th>
        <th>
            Description
        </th>
        <th>
            FPD
        </th>
        <th>
            Remarks
        </th>
        <th>
            QAD
        </th>
        <th>
            Remarks
        </th>
    </tr>
</thead>
<tbody id="checklistBody">';


for ($i = 0; $i < count($rows); $i++) {
    $row = $rows[$i];
    $WriteHTML .= '<tr>';
    $WriteHTML .= '<td>' . $row['id'] . '</td>';
    $WriteHTML .= '<td style="text-align:start">' . $row['checklist'] . '</td>';
    $WriteHTML .= '<td>' . $row['fpd'] . '</td><td>' . $row['fRemark'] . '</td>';
    $WriteHTML .= '<td>' . $row['qad'] . '</td><td>' . $row['qRemark'] . '</td>';
    $WriteHTML .= '</tr>';
}
$WriteHTML .= '<tr><td height="130px" colspan="2"><table width="100%"><tr><td height="130px" style="text-align:center;vertical-align:bottom;border:none !important">' . $checkedBy . '<br>(Checked by)</td><td style="text-align:center;vertical-align:bottom">' . $checked . '<br>(Approved by)</td></tr></table></td><td style="text-align:start;border:none;">Rev. 0</td></tr>';
$WriteHTML .= '</tbody></table>';

// for ($i = 0; $i < 8; $i++) {
//     $WriteHTML .= ('
//     <tr>
//     <td>1</td>
//     <td>1</td>
//     <td>RACK 84TE 3U G2S</td>
//     <td>FT0039310-100</td>
//     <td><img src="../../images/bs-2.jpg"  width="6.5cm" alt=""></td>
// </tr>

// ');
// }
$foot = '<center> SHEET {PAGENO} / {nbpg} </center>';
$mpdf->SetHTMLFooter($foot);
$mpdf->writeHTML($WriteHTML);
$mpdf->Output($serial . '.pdf', 'I');
