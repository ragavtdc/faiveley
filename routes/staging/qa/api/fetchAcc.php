<?php

$rId = 21;
require '../../../includes/init.php';

$db = db();

extract($_POST);

check('partId', 'part id required');

$result = mysqli_query($db, "SELECT pm.*,pas.id FROM a_part_accessories_staging pas inner join a_part_master pm on pm.part_id=pas.type where pas.serial_no='$partId' and pas.parent = 0");

if (mysqli_num_rows($result) != 1) {
    err('Unit Not Found');
}

$row = mysqli_fetch_assoc($result);

$data['unit'] = $row;

$parent = $row['id'];

$result = mysqli_query($db, "SELECT * FROM a_stage_updation WHERE part_id=$parent ORDER BY created_at DESC LIMIT 1");

if (mysqli_num_rows($result) != 1) {
    err('Unit Not Assembled');
}

$row = mysqli_fetch_assoc($result);

if ($row['stage'] != '6' || $row['status'] != '1') {
    err('Already Updated');
}

$result = mysqli_query($db, "SELECT am.*,pas.serial_no,pas.id FROM a_part_accessories_staging pas inner join a_accessories_master am on am.acc_id=pas.type where pas.parent=$parent and pas.testing=1");
$r = array();
while ($row = mysqli_fetch_assoc($result)) {
    $r[] = $row;
}

$data['acc'] = $r;

mysqli_close($db);

complete($data);
