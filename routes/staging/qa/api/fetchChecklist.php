<?php

$rId = 21;
require '../../../includes/init.php';

$db = db();

extract($_POST);

check('partId', 'part id required');


if (strlen($partId) < 5) {
    err('Insufficient Serial no');
}


$result = mysqli_query($db, "SELECT pm.*,pas.id,pas.serial_no FROM a_part_accessories_staging pas inner join a_part_master pm on pm.part_id=pas.type where (pas.serial_no like '%$partId%' or pas.serial_alpha like '%$partId%')  and pas.parent = 0");

if (mysqli_num_rows($result) == 0) {
    err('Unit Not Found');
}

$row = mysqli_fetch_assoc($result);

$data['unit'] = $row;

$parent = $row['id'];

$result = mysqli_query($db, "SELECT * FROM a_stage_updation WHERE part_id=$parent AND stage=7 AND (status=-1 OR status=1) ORDER BY created_at DESC LIMIT 1");

if (mysqli_num_rows($result) != 1) {
    err('Unit Not Assembled');
}

$row = mysqli_fetch_assoc($result);

// if ($row['stage'] != '6' || $row['status'] != '1') {
//     err('Already Updated');
// }

$data['status'] = $row['status'];

$result = mysqli_query($db, "SELECT id,checklist FROM a_checklist_master WHERE state=1");
$r = array();
while ($row = mysqli_fetch_assoc($result)) {
    $r[] = $row;
}

$counts = count($r);

$ress = mysqli_query($db, "SELECT * FROM a_checklist_log WHERE part_id=(SELECT id FROM a_part_accessories_staging WHERE serial_no='$partId') AND NOT qad IS NULL ORDER BY checklist_id, created_at DESC LIMIT $counts");
$v = array();
while ($rows = mysqli_fetch_assoc($ress)) {
    $v[] = $rows;
}

$data['cm'] = $r;

$data['cv'] = $v;

mysqli_close($db);

complete($data);
