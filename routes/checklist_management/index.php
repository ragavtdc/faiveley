<?php

$rId = 24;
require '../includes/init.php';

$db = db();

function getCheckList(): array
{
    $db = db();
    $rows = array();
    $result = mysqli_query($db, "SELECT * FROM a_checklist_master");
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    mysqli_close($db);
    return $rows;
}
$cList = getCheckList();
head();
?>

<div class='app-title'>
    <div>
        <h1><i class='fa fa-users'></i> Checklist Management </h1>
    </div>
    <!-- </ul> -->
    <ul class='app-breadcrumb breadcrumb'>
        <li class='breadcrumb-item'><a href='#'><i class='fa fa-product-hunt fa-lg'></i></a>
        </li>
        <li class='breadcrumb-item'><a href='#'>Checklist Management</a></li>
    </ul>
</div>

<div class='container'>
    <!--Accordion wrapper-->
    <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

        <!-- Accordion Edit card -->
        <div class="tile">
            <!-- Card header -->
            <div class="tile-header" role="tab">
                <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1">
                    <h5 class="mb-0">
                        Edit Checklist
                    </h5>
                </a>
            </div>
            <!-- Card body -->
            <div id="collapseOne1" class="collapse show mt-3" role="tabpanel" data-parent="#accordionEx">
                <div class="tile-body">
                    <ul class="list-group">
                        <?php foreach ($cList as $value) {
                            echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
                            echo '<div> <p class="mb-0">';
                            echo $value['checklist'];
                            echo '</p> </div>';
                            echo '<div>';
                            echo '<button data-toggle="modal" data-target="#basicExampleModal" class="btn btn-flat btn-sm p-1 mx-2" onclick="edit(' . $value['id'] . ',\'' . $value['checklist'] . ' \')"><i class="material-icons red" style="font-size: 20px">edit</i></button>';
                            echo '</div> </li>';
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Accordion card -->
        <!-- Accordion Add card -->
        <div class="tile">
            <!-- Card header -->
            <div class="tile-header" role="tab">
                <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne2">
                    <h5 class="mb-0">
                        Add New Checklist
                    </h5>
                </a>
            </div>
            <!-- Card body -->
            <div id="collapseOne2" class="collapse show mt-3" role="tabpanel" data-parent="#accordionEx">
                <div class="tile-body">
                    <form id="addNew">
                        <div class='row'>
                            <div class='form-group col-sm-12'>
                                <!-- <label class='control-label'>Checklist Info</label> -->
                                <input class='form-control' type='text' placeholder='Enter Checklist' name='newChecklist' required>
                            </div>
                            <div class='form-group btn-container col-lg-12 col-md-12 col-sm-12 text-center'>
                                <button type="submit" class='btn btn-primary'>ADD</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Accordion card -->


    </div>
    <!-- Accordion wrapper -->

</div>
<!-- </div> -->

<!-- Modal -->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Accessory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="updateChecklist">
                    <div class='form-group'>
                        <label class='control-label'>Checklist info</label>
                        <input class='form-control' type="text" name="cName" id="cName" required>
                    </div>
                    <input type="hidden" name="cId" id="cId">
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary ">Update</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<?php foot();
?>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script>
    const addForm = $('#addNew');
    const updateForm = $('#updateChecklist');

    addForm.submit((e) => {
        e.preventDefault();
        let func = (data) => {
            if (data === true) {
                successToast('added successfully');
                window.location.reload();
            }
        };
        ajax('./api/add.php', addForm.serialize(), func);
    });

    updateForm.submit((e) => {
        e.preventDefault();
        let func = (data) => {
            if (data === true) {
                successToast('updated successfully');
                window.location.reload();
            }
        };
        ajax('./api/update.php', updateForm.serialize(), func);
    });

    function edit(id, checklist) {
        $('#cName').val(checklist);
        $('#cId').val(id);
    }
</script>
</body>

</html>