<?php
session_start();

if ( !isset( $_SESSION['role'] ) ) {
    $_SESSION['close'] = 'close';
    header( 'Location: page-login.php' );
}

?>
<!DOCTYPE html>
<html lang='en'>

<head>
    <title>Dashboard - Wabtec</title>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <!-- Main CSS-->
    <link rel='stylesheet' type='text/css' href='css/main.css'>
    <!-- Font-icon css-->
    <link rel='stylesheet' type='text/css' href='js/font-awesome/4.7.0/css/font-awesome.min.css'>
    <!--     Fonts and icons     -->
    <link rel='stylesheet' type='text/css' href='css/material.css' />
    <link href='https://fonts.googleapis.com/css?family=Libre Barcode 39' rel='stylesheet'>
    <link href='css/style.css' rel='stylesheet'>
</head>
<style>
#stageOneTile,
#stageTwoTile,
#stageThreeTile,
#stageFourTile {
    box-shadow: 10px 10px 5px grey;
}
</style>

<body class='app sidebar-mini'>
    <!-- Navbar-->
    <header class='app-header'><a class='app-header__logo' href='#'>Wabtec</a>
        <!-- Sidebar toggle button-->
        <a class='app-sidebar__toggle' href='#' data-toggle='sidebar' aria-label='Hide Sidebar'></a>
        <!-- Navbar Right Menu-->
        <ul class='app-nav'>
            <!-- User Menu-->
            <li class='dropdown'><a class='app-nav__item' href='#' data-toggle='dropdown'
                    aria-label='Open Profile Menu'><i class='fa fa-user fa-lg'></i></a>
                <ul class='dropdown-menu settings-menu dropdown-menu-right'>
                    <?php if ( $_SESSION['role'] != 'Guest' ) {
    ?>
                    <li><a class='dropdown-item' href='page-user.php'><i class='fa fa-user fa-lg'></i> Profile</a></li>

                    <li><a class='dropdown-item' href='page-lockscreen.php'><i class='fa fa-lock fa-lg'></i>
                            Lock</a>
                    </li>
                    <?php }
    ?>
                    <li><a class='dropdown-item' href='page-login.php'><i class='fa fa-sign-out fa-lg'></i> Logout</a>
                    </li>
                </ul>
            </li>
        </ul>
    </header>
    <!-- Sidebar menu-->
    <div class='app-sidebar__overlay' data-toggle='sidebar'></div>
    <aside class='app-sidebar'>
        <div class='app-sidebar__user'><img class='app-sidebar__user-avatar' src='images/emp.png' alt='User Image'>
            <div>
                <p class='app-sidebar__user-name'>
                    <?php echo strtoupper( $_SESSION['username'] );
    ?></p>
            </div>
        </div>
        <ul class='app-menu'>
            <li><a class='app-menu__item active' href='#'><i class='app-menu__icon fa fa-dashboard'></i><span
                        class='app-menu__label'>Dashboard</span></a>
            </li>
            <?php
    if ( $_SESSION['role'] == 'Super Admin' ) {
        ?>
            <li><a class='app-menu__item' href='routes/user-management/'><i class='app-menu__icon fa fa-users'></i>
                    <span class='app-menu__label'>User Management</span>
                </a></li>

            <li><a class='app-menu__item' href='routes/staging/'><i class='app-menu__icon fa fa-database'></i>
                    <span class='app-menu__label'>Staging</span>
                </a></li>
            <?php }
        ?>
            <li><a class='app-menu__item' href='routes/print/'><i class='app-menu__icon fa fa-print'></i>
                    <span class='app-menu__label'>Report</span>
                </a></li>
        </ul>
    </aside>
    <main class='app-content'>
        <div class='app-title'>
            <div>
                <h1><i class='fa fa-dashboard'></i> Dashboard </h1>
            </div>
            <ul class='app-breadcrumb breadcrumb'>
                <li class='breadcrumb-item'><a href='#'><i class='fa fa-home fa-lg'></i></a>
                </li>
                <li class='breadcrumb-item'><a href='#'>Dashboard</a></li>
            </ul>
        </div>

        <div class='row align-items-center'>
            <div class='col-md-1'></div>
            <div class='col-md-5'>
                <div class='input-group'>
                    <input type='text' class='form-control' placeholder='Part code' name='productId' id='prodCode'
                        autocomplete='off' required autofocus>
                    <div class='input-group-append'>
                        <button class='input-group-text' onclick='dashBoardData()'>Check</button>
                    </div>
                </div>
            </div>
            <div class='col-sm-2'></div>
            <div class='col-sm-4 float-sm-right'>
                <ul class='list-group list-group-horizontal'>
                    <li class='list-group-item green white-text'>Comlpleted</li>
                    <li class='list-group-item amber'>On going</li>
                    <li class='list-group-item red white-text'>To be start</li>
                </ul>
            </div>
        </div>

        <div class='row'>
            <div class='col-md-1'></div>
            <div class='col-md-5' id='partNameArea'>

            </div>
        </div>

        <br>

        <!-- Dashboard card values starts here -->
        <div class='row'>
            <div class='col-sm-1'></div>
            <div class='col-sm-10'>
                <div class='row'>
                    <div class='col-md-12 tile' data-toggle='tooltip' data-placement='right' title='ASSEMBLE'
                        id='stageOneTile'>
                        <div class='row'>
                            <div class='col-sm-1'>
                                <p>PRELIMINARY SERIALIZATION</p>
                            </div>
                            <center>
                                <div class='col-sm-1'>
                                    <center>
                                        <div id='iconStageOneSubOne'>
                                            <button class='btn btn-red btn-sm btnState'>start</button>
                                        </div>
                                    </center>
                                </div>
                            </center>
                            <div class='col-sm-1'></div>
                            <div class='col-sm-1'>
                                <p>PCBA INSERTION</p>
                            </div>
                            <center>
                                <div class='col-sm-1'>
                                    <center>
                                        <div id='iconStageOneSubTwo'>
                                            <button class='btn btn-red btn-sm btnState'>start</button>
                                        </div>
                                    </center>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-md-12 tile' data-toggle='tooltip' data-placement='right' title='TEST'
                        id='stageTwoTile'>
                        <div class='row'>
                            <div class='col-sm-1'>
                                <p>INSULATION TEST</p>
                            </div>
                            <center>
                                <div class='col-sm-1'>
                                    <center>
                                        <div id='iconStageTwoSubOne'>
                                            <button class='btn btn-red btn-sm btnState'>start</button>
                                        </div>
                                    </center>
                                </div>
                            </center>
                            <div class='col-sm-1'></div>
                            <div class='col-sm-1'>
                                <p>PERFORMANCE TEST</p>
                            </div>
                            <center>
                                <div class='col-sm-1'>
                                    <center>
                                        <div id='iconStageTwoSubTwo'>
                                            <button class='btn btn-red btn-sm btnState'>start</button>
                                        </div>
                                    </center>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-md-12 tile' data-toggle='tooltip' data-placement='right' title='BURN'
                        id='stageThreeTile'>
                        <div class='row'>
                            <div class='col-sm-1'>
                                <p>ESS CHAMBER TEST</p>
                            </div>
                            <center>
                                <div class='col-sm-1'>
                                    <center>
                                        <div id='iconStageThreeSubOne'>
                                            <button class='btn btn-red btn-sm btnState'>start</button>
                                        </div>
                                    </center>
                                </div>
                            </center>
                            <div class='col-sm-1'></div>
                            <div class='col-sm-1'>
                                <p>FINAL SERIALIZATION</p>
                            </div>
                            <center>
                                <div class='col-sm-1'>
                                    <center>
                                        <div id='iconStageThreeSubTwo'>
                                            <button class='btn btn-red btn-sm btnState'>start</button>
                                        </div>
                                    </center>
                                </div>
                            </center>
                            <div class='col-sm-1'></div>
                            <div class='col-sm-1'>
                                <p>SOFTWARE DOWNLOAD</p>
                            </div>
                            <center>
                                <div class='col-sm-1'>
                                    <center>
                                        <div id='iconStageThreeSubThree'>
                                            <button class='btn btn-red btn-sm btnState'>start</button>
                                        </div>
                                    </center>
                                </div>
                            </center>
                            <div class='col-sm-1'></div>
                            <div class='col-sm-2' id='rmv'></div>
                            <div class='col-sm-1'>
                                <p>OPERATOR VISUAL INSPECTION</p>
                            </div>
                            <center>
                                <div class='col-sm-1'>
                                    <center>
                                        <div id='iconStageThreeSubFour'>
                                            <button class='btn btn-red btn-sm btnState'>start</button>
                                        </div>
                                    </center>
                                </div>
                            </center>
                            <div class='col-sm-1'></div>
                            <div class='col-sm-1'>
                                <p>CHECK FOR SOFTWARE</p>
                            </div>
                            <center>
                                <div class='col-sm-1'>
                                    <center>
                                        <div id='iconStageThreeSubFive'>
                                            <button class='btn btn-red btn-sm btnState'>start</button>
                                        </div>
                                    </center>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-md-12 tile' data-toggle='tooltip' data-placement='right' title='FINAL QUALITY'
                        id='stageFourTile'>
                        <div class='row'>
                            <div class='col-sm-1'>
                                <p>PACKING</p>
                            </div>
                            <center>
                                <div class='col-sm-1'>
                                    <center>
                                        <div id='iconStageFourSubOne'>
                                            <button class='btn btn-red btn-sm btnState'>start</button>
                                        </div>
                                    </center>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class='footer' id='fixed-footer'>
            <div class='container-fluid'>
                <nav class='float-right'>
                    <ul>
                        <div class='copyright float-right' style='padding-right: 30px;'>
                            &copy;
                            <script>
                            document.write(new Date().getFullYear())
                            </script>,
                            <a href='http://bittechpro.in/' target='blank'>BIT-Techno Product</a>
                        </div>
                    </ul>
                </nav>
            </div>
        </footer>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src='js/jquery-3.3.1.min.js'></script>
    <script src='js/popper.min.js'></script>
    <script src='js/bootstrap.min.js'></script>
    <script src='js/main.js'></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src='js/plugins/pace.min.js'></script>
    <!-- Page specific javascripts-->
    <!-- Bootstrap core CSS -->
    <link href='js/mdb-css/bootstrap.min.css' rel='stylesheet'>
    <!-- Material Design Bootstrap -->
    <link href='js/mdb-css/mdb.min.css' rel='stylesheet'>
    <!-- Your custom styles ( optional ) -->
    <link href='js/mdb-css/style.css' rel='stylesheet'>
    <!-- JQuery -->
    <script type='text/javascript' src='js/mdb-js/mdb.min.js'></script>
    <script src='js/plugins/plotly-latest.min.js' type='text/javascript'></script>
    <script src='js/plugins/bootstrap-notify.min.js'></script>
    <script type='text/javascript' src='js/plugins/sweetalert.min.js'></script>

    <script>
    function disableBackButton() {
        window.history.forward();
    }
    setTimeout('disableBackButton()', 0);

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
    </script>
</body>

<script type='text/javascript'>
function dashBoardData() {
    var prodCode = document.getElementById('prodCode');
    $.ajax({
        type: 'post',
        url: 'apis/dashboardData.php',
        data: {
            'partCode': prodCode.value
        },
        dataType: 'json',
        success: function(data) {
            if (data.status == 'ok') {
                $('#partNameArea').html(
                    '<span>Part name : ' + data.result.part_name + '</span><br><span> Part number :  ' +
                    data.result.part_number + '</span><br><span>Last scanned part code : ' + data.result
                    .part_code_id + '</span>'
                );
                //Referring main satge one
                if (data.result.main_stage_no == '1') {
                    $('#stageTwoTile').removeClass('animated pulse infinite shadow-lg');
                    $('#stageThreeTile').removeClass('animated pulse infinite shadow-lg');
                    $('#stageFourTile').removeClass('animated pulse infinite shadow-lg');
                    $('#stageOneTile').addClass('animated pulse infinite shadow-lg');
                    $('#rmv').addClass('col-sm-2');
                    $('#rmv').removeClass('col-sm-1');

                    //Referring sub stage one
                    if (data.result.sub_stage_no == '1') {
                        $('#iconStageOneSubTwo').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageTwoSubOne').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageTwoSubTwo').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageThreeSubOne').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageThreeSubTwo').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageThreeSubThree').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageThreeSubFour').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageThreeSubFive').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageFourSubOne').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        if (data.result.status == '0') {
                            $('#iconStageOneSubOne').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#iconStageOneSubOne').html(
                                '<button class="btn btn-amber btn-sm btnState">ongoing</button>'
                            );
                        } else if (data.result.status == '2') {
                            $('#iconStageOneSubOne').html(
                                '<button class="btn btn-green btn-sm btnState">completed</button>');
                        }
                    }
                    //Referring sub stage two
                    if (data.result.sub_stage_no == '2') {
                        $('#iconStageOneSubOne').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        $('#iconStageTwoSubOne').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageTwoSubTwo').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageThreeSubOne').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageThreeSubTwo').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageThreeSubThree').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageThreeSubFour').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageThreeSubFive').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );
                        $('#iconStageFourSubOne').html(
                            '<button class="btn btn-red btn-sm btnState">start</button>'
                        );

                        if (data.result.status == '0') {
                            $('#iconStageOneSubTwo').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#iconStageOneSubTwo').html(
                                '<button class="btn btn-amber btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#iconStageOneSubTwo').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        }
                    }
                }
                //Referring main stage two
                if (data.result.main_stage_no == '2') {
                    $('#stageOneTile').removeClass('animated pulse infinite');
                    $('#stageThreeTile').removeClass('animated pulse infinite shadow-lg');
                    $('#stageFourTile').removeClass('animated pulse infinite shadow-lg');
                    $('#stageTwoTile').addClass('animated pulse infinite shadow-lg');
                    $('#stageOneTile').addClass('shadow-lg');
                    $('#rmv').addClass('col-sm-2');
                    $('#rmv').removeClass('col-sm-1');

                    $('#iconStageOneSubOne').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageOneSubTwo').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageTwoSubTwo').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageThreeSubOne').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageThreeSubTwo').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageThreeSubThree').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageThreeSubFour').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageThreeSubFive').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageFourSubOne').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );

                    //Referring sub stage one
                    if (data.result.sub_stage_no == '3') {
                        if (data.result.status == '0') {
                            $('#iconStageTwoSubOne').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#iconStageTwoSubOne').html(
                                '<button class="btn btn-amber btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#iconStageTwoSubOne').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        }
                    }
                    //Referring sub stage two
                    if (data.result.sub_stage_no == '4') {
                        $('#iconStageTwoSubOne').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>');
                        if (data.result.status == '0') {
                            $('#iconStageTwoSubTwo').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#iconStageTwoSubTwo').html(
                                '<button class="btn btn-amber btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#iconStageTwoSubTwo').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        }
                    }
                }

                //Referring main stage three
                if (data.result.main_stage_no == '3') {
                    $('#stageTwoTile').removeClass('animated pulse infinite');
                    $('#stageOneTile').removeClass('animated pulse infinite');
                    $('#stageFourTile').removeClass('animated pulse infinite shadow-lg');
                    $('#stageThreeTile').addClass('animated pulse infinite shadow-lg');
                    $('#stageTwoTile').addClass('shadow-lg');
                    $('#stageOneTile').addClass('shadow-lg');

                    $('#iconStageOneSubOne').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageOneSubTwo').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageTwoSubOne').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageTwoSubTwo').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubOne').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageThreeSubTwo').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageThreeSubThree').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageThreeSubFour').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageThreeSubFive').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );
                    $('#iconStageFourSubOne').html(
                        '<button class="btn btn-red btn-sm btnState">start</button>'
                    );

                    //Referring sub stage one
                    if (data.result.sub_stage_no == '5') {
                        if (data.result.status == '0') {
                            $('#iconStageThreeSubOne').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#iconStageThreeSubOne').html(
                                '<button class="btn btn-amber btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#iconStageThreeSubOne').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        }
                    }

                    //Referring sub stage two
                    if (data.result.sub_stage_no == '6') {
                        $('#iconStageThreeSubOne').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        $('#rmv').removeClass('col-sm-2');
                        $('#rmv').addClass('col-sm-1');
                        if (data.result.status == '0') {
                            $('#iconStageThreeSubTwo').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#iconStageThreeSubTwo').html(
                                '<button class="btn btn-amber btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#iconStageThreeSubTwo').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        }
                    }
                    //Referring sub stage three
                    if (data.result.sub_stage_no == '7') {
                        $('#iconStageThreeSubOne').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        $('#iconStageThreeSubTwo').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        if (data.result.status == '0') {
                            $('#iconStageThreeSubThree').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#iconStageThreeSubThree').html(
                                '<button class="btn btn-amber btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#iconStageThreeSubThree').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        }
                    }
                    //Referring sub stage four
                    if (data.result.sub_stage_no == '8') {
                        $('#iconStageThreeSubOne').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        $('#iconStageThreeSubTwo').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        $('#iconStageThreeSubThree').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        if (data.result.status == '0') {
                            $('#iconStageThreeSubFour').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#iconStageThreeSubFour').html(
                                '<button class="btn btn-amber btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#iconStageThreeSubFour').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        }
                    }

                    //Referring sub stage five
                    if (data.result.sub_stage_no == '9') {
                        $('#iconStageThreeSubOne').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        $('#iconStageThreeSubTwo').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        $('#iconStageThreeSubThree').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        $('#iconStageThreeSubFour').html(
                            '<button class="btn btn-green btn-sm btnState">completed</button>'
                        );
                        if (data.result.status == '0') {
                            $('#iconStageThreeSubFive').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#iconStageThreeSubFive').html(
                                '<button class="btn btn-amber btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#iconStageThreeSubFive').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        }
                    }
                }

                //Referring main stage four
                if (data.result.main_stage_no == '4') {
                    $('#stageTwoTile').removeClass('animated infinite pulse');
                    $('#stageThreeTile').removeClass('animated infinite pulse');
                    $('#stageOneTile').removeClass('animated pulse infinite');
                    $('#stageTwoTile').addClass('shadow-lg');
                    $('#stageThreeTile').addClass('shadow-lg');
                    $('#stageOneTile').addClass('shadow-lg');
                    $('#stageFourTile').addClass('animated infinite pulse shadow-lg');

                    $('#rmv').removeClass('col-sm-2');
                    $('#rmv').addClass('col-sm-1');

                    $('#iconStageOneSubOne').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageOneSubTwo').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageTwoSubOne').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageTwoSubTwo').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubOne').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubTwo').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubThree').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubFour').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubFive').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );

                    //Referring sub stage one
                    if (data.result.sub_stage_no == '10') {
                        if (data.result.status == '0') {
                            $('#iconStageFourSubOne').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        } else if (data.result.status == '1') {
                            $('#iconStageFourSubOne').html(
                                '<button class="btn btn-amber btn-sm btnState">ongoing</button>'
                            );
                        } else {
                            $('#iconStageFourSubOne').html(
                                '<button class="btn btn-red btn-sm btnState">start</button>'
                            );
                        }
                    }
                }

                //Referring main stage five
                if (data.result.main_stage_no == '5' && data.result.sub_stage_no == '11') {
                    $('#stageTwoTile').removeClass('animated infinite pulse');
                    $('#stageThreeTile').removeClass('animated infinite pulse');
                    $('#stageOneTile').removeClass('animated pulse infinite');
                    $('#stageFourTile').removeClass('animated pulse infinite');
                    $('#stageTwoTile').addClass('shadow-lg');
                    $('#stageThreeTile').addClass('shadow-lg');
                    $('#stageOneTile').addClass('shadow-lg');
                    $('#stageFourTile').addClass('shadow-lg');

                    $('#iconStageOneSubOne').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageOneSubTwo').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageTwoSubOne').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageTwoSubTwo').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubOne').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubTwo').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubThree').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubFour').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageThreeSubFive').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                    $('#iconStageFourSubOne').html(
                        '<button class="btn btn-green btn-sm btnState">completed</button>'
                    );
                }

            } else {
                prodCode.value = '';
                prodCode.focus();

                $.notify({
                    title: ' Sorry : ',
                    message: data.result.error,
                    icon: 'fa fa-exclamation-triangle'
                }, {
                    type: 'danger'
                });
            }
        }
    });
}
</script>

</html>