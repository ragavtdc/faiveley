<?php
session_start();
if ( !isset( $_SESSION['username'] ) ) {
    $_SESSION['close'] = 'close';
    header( 'Location: page-login.php' );
} else {
    $_SESSION['close'] = '';
}

$_SESSION['lock'] = true;

require 'routes/includes/functions.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='shortcut icon' href='images/favicon-wabtec.ico' type='image/x-icon'>
    <!-- Main CSS-->
    <link rel='stylesheet' type='text/css' href='css/main.css'>
    <!-- Font-icon css-->
    <link rel='stylesheet' type='text/css' href='js/font-awesome/4.7.0/css/font-awesome.min.css'>
    <title>Lockscreen</title>
    <script>
    function disableBackButton() {
        window.history.forward();
    }
    setTimeout('disableBackButton()', 0);
    </script>
</head>

<body onload='disableBackButton()'>
    <section class='material-half-bg'>
        <div class='cover' id='rlogo'>
            <h1>Wabtec</h1>
        </div>
    </section>
    <section class='lockscreen-content'>
        <div class='logo'>
            <!-- <h1>R. Stahl</h1> -->
            <center>
                <!-- <img src = 'images/faiveley.png' height = '40%' width = '25%'> -->
                <img src='<?php echo $path ?>images/logo-wabtec.png' height='40%' width='25%'>
            </center>
        </div>
        <div class='lock-box'><img class='rounded-circle user-image' src='images/emp.png'>
            <h4 class='text-center user-name'><?php echo strtoupper( $_SESSION['username'] );
?></p>
            </h4>
            <p class='text-center text-muted'><?php echo $_SESSION['usermail'];
?></p>
            <p class='text-center text-muted'>Account Locked</p>
            <form class='unlock-form' method='post' action='validation/lockscreen.php'>
                <div class='form-group'>
                    <label class='control-label'>PASSWORD</label>
                    <input class='form-control' name='password' type='password' placeholder='Password' autofocus
                        required>
                </div>
                <div class='form-group btn-container'>
                    <button class='btn btn-primary btn-block' type='submit'><i class='fa fa-unlock fa-lg'></i>UNLOCK
                    </button>
                </div>
            </form>
            <p><a href='page-login.php'>Not <?php echo $_SESSION['username'];
?> ? Login Here.</a></p>
        </div>
    </section>
    <?php foot();
?>
    <!-- Essential javascripts for application to work-->
    <script src='js/jquery-3.3.1.min.js'></script>
    <script src='js/popper.min.js'></script>
    <script src='js/bootstrap.min.js'></script>
    <script src='js/main.js'></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src='js/plugins/pace.min.js'></script>
    <script src='js/plugins/bootstrap-notify.min.js'></script>

    <?php if ( $_SESSION['error'] == 'error' ) {
    ?>
    <script type='text/javascript'>
    $.notify({
        title: 'Login error : ',
        message: 'Invalid password',
        icon: 'fa fa-exclamation-triangle'
    }, {
        type: 'danger'
    });
    </script>
    <?php }
?>
</body>

</html>
<?php
$_SESSION['error'] = '';
?>