<?php
session_start();

// $temp = $_REQUEST['temp'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Change Password - R. Stahl</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="js/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body style="background-color: #e5e5e5;" class="app sidebar-mini" onload="disableBackButton()">

    <header class="app-header">
        <a class="app-header__logo" href="#">R. Stahl</a>
    </header>
    <main>
        <!-- <div class="app-title">
            <div>
                <h1><i class="fa fa-cog"></i></h1>
            </div>
        </div> -->
        <img src="images/rstahl_logo.png" style="padding-left: 2%;padding-bottom: 4%; padding-top: 5%; width: 150px">
        <!-- <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="tile">
                    <div class="tile-title-w-btn">
                       <h3 class="title">Welcome</h3>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="tile">
                    <div class="tile-title-w-btn">
                        <h3 class="title">Reset Password</h3>
                    </div>
                    <div class="tile-body">
                        <form class="login-form" method="post" action="validation/change-password/change-password.php">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-6 form-group">
                                    <input class="form-control" type="hidden" name="temp" value="<?php echo $_SESSION['temp']?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-6 form-group">
                                    <input class="form-control" id="paswd" type="password" name="changePassword"
                                        autocomplete="off" required title="Password" placeholder="Enter new password"
                                        data-toggle="tooltip">
                                </div>
                                <div class="col-md-2 form-group">
                                    <button class="chbtn" type="button" onclick="view_hide_password()"><i id="pasIcon"
                                            class="fa fa-eye"></i></button><br><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-6 form-group">
                                    <input class="form-control" id="confrmPaswd" type="password"
                                        name="changeConfrmPassword" autocomplete="off" required title="Confirm password"
                                        placeholder="Confirm password" data-toggle="tooltip">
                                </div>
                                <div class="col-md-2 form-group">
                                    <button class="chbtn" type="button" onclick="view_hide_confirm_password()"><i
                                            id="confrmPasIcon" class="fa fa-eye"></i></button><br><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-6 form-group btn-container">
                                    <button class="btn btn-primary" type="submit" title="Reset"
                                        data-toggle="tooltip"><b>Reset</b></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="js/mdb-css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="js/mdb-css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="js/mdb-css/style.css" rel="stylesheet">
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>


    <script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    function view_hide_password() {
        var paswdType = document.getElementById('paswd').type;
        if (paswdType == 'password') {
            document.getElementById('paswd').setAttribute('type', 'text');
            document.getElementById('pasIcon').removeAttribute('class', 'fa-eye');
            document.getElementById('pasIcon').setAttribute('class', 'fa fa-eye-slash');
        } else if (paswdType == 'text') {
            document.getElementById('paswd').setAttribute('type', 'password');
            document.getElementById('pasIcon').removeAttribute('class', 'fa-eye-slash');
            document.getElementById('pasIcon').setAttribute('class', 'fa fa-eye');
        }
    }

    function view_hide_confirm_password() {
        var paswdType = document.getElementById('confrmPaswd').type;
        if (paswdType == 'password') {
            document.getElementById('confrmPaswd').setAttribute('type', 'text');
            document.getElementById('confrmPasIcon').removeAttribute('class', 'fa-eye');
            document.getElementById('confrmPasIcon').setAttribute('class', 'fa fa-eye-slash');
        } else if (paswdType == 'text') {
            document.getElementById('confrmPaswd').setAttribute('type', 'password');
            document.getElementById('confrmPasIcon').removeAttribute('class', 'fa-eye-slash');
            document.getElementById('confrmPasIcon').setAttribute('class', 'fa fa-eye');
        }
    }
    </script>
    <footer class="footer" id="fixed-footer">
        <div class="container-fluid">
            <nav class="float-right">
                <ul>
                    <div class="copyright float-right" style="padding-right: 30px;">
                        &copy;
                        <script>
                        document.write(new Date().getFullYear())
                        </script>,
                        <a href="http://bittechpro.in/" target="blank">BIT-Techno Product</a>
                    </div>
                </ul>
            </nav>
        </div>
    </footer>

    <script type="text/javascript">
    function disableBackButton() {
        window.history.forward();
    }
    setTimeout("disableBackButton()", 0);
    </script>

    <!-- <?php
        if ($_SESSION['check'] == 'incorrect') { ?>
    <script>
    swal('Incorrect', 'Password does not match.\nIncorrect password.', 'error');
    </script>
    <?php } ?> -->


</body>

</html>