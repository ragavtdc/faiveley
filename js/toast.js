function toast(title, msg = '') {
    $.notify({
        title: '<span style="color:white">' + title + '</span>',
        message: '<span style="color:white">' + msg + '</span>',
        icon: 'fa fa-exclamation-triangle text-white mr-2'
    }, {
        type: 'danger',
        placement: {
            from: "top",
            align: "right"
        },
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        delay: 2000,
        timer: 500,
    });
}

function successToast(title, msg = '') {
    $.notify({
        title: '<span style="color:white">' + title + '</span>',
        message: '<span style="color:white">' + msg + '</span>',
        icon: 'fa fa-exclamation-triangle text-white mr-2'
    }, {
        type: 'success',
        placement: {
            from: "top",
            align: "right"
        },
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        delay: 2000,
        timer: 500,
    });
}