<?php

require 'PHPMailerAutoload.php';
require 'class.smtp.php';
require 'class.phpmailer.php';

session_start();

if ( $_SESSION['check'] != 'correct' || !isset($_REQUEST['temp'])) {
    $_SESSION['error'] = 'Invalid parameters';
    die( header( 'Location: ../../page-login.php' ) );
}

extract( $_REQUEST );

$mail = new PHPMailer;

// echo !extension_loaded( 'openssl' ) ? 'Not Available' : 'Available';

$mail->isSMTP();
// Set mailer to use SMTP
$mail->Host = 'ssl://smtp.gmail.com';
// Specify main and backup SMTP servers
$mail->SMTPAuth = true;
$mail->Username = 'ragavanmurugan.desicrew@gmail.com';
// SMTP username
$mail->Password = 'Ragavmadhu44*';
// SMTP password
$mail->SMTPSecure = 'ssl';
// Enable TLS encryption, `ssl` also accepted
$mail->Port = 465;
// TCP port to connect to

$mail->setFrom( 'changepassword@rstahl.com', 'R. Stahl' );
$mail->addReplyTo( 'noreply@rstahl.com', 'R. Stahl' );
$mail->addAddress( $_SESSION['usermail'] );
// Add addrress to send to
// $mail->addAddress( 'ragavm13@gmail.com' );
// Add addrress to send to

$mail->isHTML( true );
// Set email format to HTML

$bodyContent = "<html>

<head>
<style>
    span {
        color: blue;
    }
    
    .top {
        background-color: #009687;
        height: 85px;
        width: 50%;
        padding: 30px;
    }
    
    h2 {
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
    }
    
    .card {
        background-color: whitesmoke;
        width: 50%;
        padding: 30px;
        box-shadow: 2px 5px 2px #999999;
        border-radius: 5px;
    }
    
    .btn {
        background-color: #009687;
        border: #009687;
        color: white;
        text-align: center;
        text-decoration: none;
        padding: 15px 32px;
        display: inline-block;
        font-size: 16px;
        border-radius: 5px;
        cursor: pointer;
        box-shadow: 2px 6px #999999;
        margin: 10px 20px 10px 20px;
    }
    
    .btn:hover {
        background-color: #0d7c71;
    }
    
    .btn:active {
        background-color: #0d7c71;
        box-shadow: 0px 3px #666666;
        transform: translateY(4px);
    }
    
    p {
        font-size: 16px;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        text-align: justify;
        padding-left: 150px;
    }
    
    img {
        position: relative;
        margin-top: auto;
        height: 85px;
    }
</style>
</head>

<body bgcolor='#e5e5e5'>
    <section>
        <center>
            <div class='top'>
                <img src='http://apps.bittechpro.in/projects/rstahl/images/rstahl_logo.png' width='150px'><br>
            </div>

            <div class='card'>

                <h2>RESET PASSWORD</h2>

                <div>
                    
                    <p>We heard you need a <span>password reset</span>. Click below and you will be <br>
                    redirected to secure site from which you can set a new password.</p>
                </div>
                <div class='row'>
                    <div class='col-md-12'>                            
                            
                          <a href='http://apps.bittechpro.in/projects/rstahl/validation/change-password/check-password-link-expiration.php?temp=$temp' 
                          target='blank'>  
                          <button class='btn' type='submit'><b>Reset Password</b></button></a>                            
                        
                    </div>
                </div>
                <div>
                    <p>If you didn't try to reset your password, <br> just <span>ignore</span> this mail.</p>
                </div>
        </center>
    </section>

</body>


</html>";
// $bodyContent .= '';
// <form method='post' action='http://apps.bittechpro.in/projects/rstahl/change-paswd.php'>
    // <a href='http://apps.bittechpro.in/projects/rstahl/change-paswd.php'>
    // </a>

$mail->Subject = 'R. Stahl Password Reset Link';
$mail->Body    = $bodyContent;

if ( !$mail->send() ) {
    $_SESSION['error'] = $mail->ErrorInfo;
    header( 'Location: ../../page-user.php' );
    // echo 'Not sent';
} else {
    $_SESSION['check'] = '';
    $_SESSION['mail'] = 'sent';
    header( 'Location: ../../page-login.php' );
    // echo 'Sent';
}