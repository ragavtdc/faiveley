<?php
require 'db/connect.php';

$db = db();

session_start();

if ( !isset( $_SESSION['role'] ) && !isset( $_SESSION['usermail'] ) ) {
    header( 'Location: ../page-login.php' );
}

extract( $_SESSION );

if ( isset( $_POST['password'] ) ) {

    extract( $_POST );

    // $password = md5( $password );

    $stmt = $db->prepare( "SELECT um.*,rm.role_name FROM user_master um INNER JOIN role_master rm ON rm.role_id=um.role WHERE um.email='$usermail' AND um.password=? AND um.state=1" );
    $stmt->bind_param( 's', $password );

    $res = array();

    if ( $stmt->execute() ) {
        $result = $stmt->get_result();
        while ( $row = mysqli_fetch_assoc( $result ) ) {
            $res = $row;
        }
    }

    if ( mysqli_num_rows( $result ) == 1 ) {
        // $_SESSION['role'] = $_POST['role'];
        $_SESSION['lock'] = false;
        header( 'Location: ../routes/dashboard/' );
    } else {
        $_SESSION['error'] = 'error';
        header( 'Location: ../page-lockscreen.php' );
    }
} else {
    $_SESSION['close'] = 'close';
    header( 'Location: ../page-lockscreen.php' );
}