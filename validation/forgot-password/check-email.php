<?php
session_start();

?><?php
// ini_set( 'display_errors', 1 );
// ini_set( 'display_startup_errors', 1 );
// error_reporting( E_ALL );

require( '../db/connect.php' );

$db = db();

// $result1 = array();
// $result = array();

$checked = false;

if ( isset( $_REQUEST['usermail'] ) ) {

    extract( $_REQUEST );

    // if ( !filter_var( $usermail, FILTER_VALIDATE_EMAIL ) ) {
    //     $_SESSION['invalid'] = true;
    //     die( header( 'Location: ../../page-login.php' ) );
    // }

    try {

        $sqlCheck = "SELECT * FROM [rstahl].[dbo].[login] WHERE user_mail = '$usermail'";

        $stmt = $db->prepare( $sqlCheck, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
        $stmt->execute();

        while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ) {
            $result = $row;
        }

        $getUmail = $result['user_mail'];

        if ( $stmt->rowCount() == 1 ) {

            // if ( $stmt->rowCount() > 0 ) {

            try {
                $db->beginTransaction();

                $sql = "DELETE FROM [rstahl].[dbo].[password_link_expiration] WHERE usermail = '$getUmail'";

                $stmt = $db->prepare( $sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
                $stmt->execute();

                $db->commit();

                try {

                    $t = time();

                    $encrypted_data = md5(md5($getUmail.$t));

                    $db->beginTransaction();

                    $sql = 'INSERT INTO [rstahl].[dbo].[password_link_expiration] (usermail, tempVal) VALUES (:usermail, :temp)';

                    $stmt = $db->prepare( $sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
                    $stmt->execute( array( ':usermail' => $usermail, ':temp' => $encrypted_data ) );

                    $db->commit();

                    $rowCount = $stmt->rowCount();

                    // if ( $rowCount == 1 ) {
                        $checked = true;
                    // }
                } catch ( Exception $ex ) {
                    // $_SESSION['check'] = $ex->getMessage();
                    // header( 'Location: ../../page-login.php' );
                    $checked = false;
                }

            } catch ( Exception $ex ) {
                // header( 'Location: ../../page-login.php' );
                $checked = false;
            }
            // }
        }

        if ( $checked ) {

            $sql = "SELECT * FROM [rstahl].[dbo].[password_link_expiration] WHERE usermail = '$getUmail'";

            $stmt = $db->prepare( $sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
            $stmt->execute();

            while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ) {
                $result1 = $row;
            }

            $_SESSION['check'] = 'correct';
            $_SESSION['usermail'] = $getUmail;
            header( 'Location: ../send-mail/change-password-link.php?temp='.$result1['tempVal'] );
            
        } else {
            $_SESSION['check'] = 'incorrect';
            header( 'Location: ../../page-login.php' );
        }
    } catch ( Exception $ex ) {
        $_SESSION['check'] = $ex->getMessage();
        header( 'Location: ../../page-login.php' );
    }

} else
header( 'Location: ../../page-login.php' );

$stmt = null;
$db = null;