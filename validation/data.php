<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
header('Access-Control-Allow-Origin: *');
$data = array();
$result = array();

require('db/connect.php');

$db = db();

$stmt = $db->prepare("SELECT TOP(1) * FROM [rstahl].[dbo].[sensor_data] ORDER BY sno DESC");
$stmt->execute();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$result = $row;
}

if ($stmt->rowCount() > 0) {
	$data['status'] = 'ok';
	$data['result'] = $result;
} else {
	$data['status'] = '';
	$data['result'] = '';
}

echo json_encode($data);

$stmt = null;
$db = null;