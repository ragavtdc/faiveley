<?php
header('Access-Control-Allow-Origin: *');

$data = array();
$result = array();

require('db/connect.php');

$db = db();

// $query = "SELECT * FROM [rstahl].[dbo].[sensor_data] WHERE curr_time >= DATEADD(DD, -1, getdate()) ORDER BY sno DESC";
// $query = "SELECT * FROM [rstahl].[dbo].[sensor_data] ORDER BY sno DESC";
$query = "SELECT TOP(8) * FROM [rstahl].[dbo].[sensor_data] ORDER BY sno DESC";

$stmt = $db->prepare($query);
$stmt->execute();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$result[] = $row;
}

if ($stmt->rowCount() > 0) {
	$data['success'] = 'ok';
	$data['result'] = $result;
} else {
	$data['status'] = '';
	$data['result'] = '';
}

//returns data as JSON format
echo json_encode($data);
// echo json_encode($data['result'][1]['temperature']);
// echo count($data['result']);

$stmt = null;
$db = null;