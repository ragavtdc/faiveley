<?php
require('db/connect.php');

$db = db();

if (isset($_REQUEST['username']) && isset($_REQUEST['password'])) {
    session_start();

    extract($_REQUEST);

    // $password = md5( $password );

    $stmt = $db->prepare('SELECT um.*,rm.role_name FROM user_master um INNER JOIN role_master rm ON rm.role_id=um.role WHERE um.username=? AND um.password=? AND um.state=1');

    $stmt->bind_param('ss', $username, $password);

    $res = array();

    if ($stmt->execute()) {
        $result = $stmt->get_result();
        while ($row = mysqli_fetch_assoc($result)) {
            $res = $row;
        }
    }

    // echo $result['user_type'];

    if (mysqli_num_rows($result) == 1) {
        $_SESSION['user'] = $res;
        $_SESSION['usermail'] = $res['email'];
        $_SESSION['role'] = $res['role_name'];
        $_SESSION['username'] = $res['username'];
        header('Location: ../routes/dashboard/');
    } else {
        $_SESSION['error'] = 'error';
        header('Location: ../page-login.php');
    }
} else
    header('Location: ../page-login.php');
