<?php
session_start();

?><?php
// ini_set( 'display_errors', 1 );
// ini_set( 'display_startup_errors', 1 );
// error_reporting( E_ALL );

require( '../db/connect.php' );

$db = db();

$result = array();

$result['inserteOn'] = 61;

$curDatetime = date( 'Y-m-d H:i:s.u' );

if ( isset( $_REQUEST['temp'] ) ) {

    extract( $_REQUEST );

    try {

        $sql = "SELECT * FROM [rstahl].[dbo].[password_link_expiration] WHERE tempVal = '$temp'";

        $stmt = $db->prepare( $sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
        $stmt->execute();

        while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ) {
            $result = $row;
        }

        $insertedOn = $result['insertedOn'];

        if ( $stmt->rowCount() > 0 ) {

            $sqlCh = "SELECT DATEDIFF(MINUTE, '$insertedOn', GETDATE())";

            $stmtCh = $db->prepare( $sqlCh, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
            $stmtCh->execute();

            while ( $row = $stmtCh->fetch( PDO::FETCH_ASSOC ) ) {
                $time = $row[''];
            }

            if ( $time > 60 ) {

                $db->beginTransaction();

                $sqlDel = "DELETE FROM [rstahl].[dbo].[password_link_expiration] WHERE tempVal = '$temp'";

                $stmtDel = $db->prepare( $sqlDel, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
                $stmtDel->execute();

                $db->commit();

                $_SESSION['link-expiration'] = 'expired';

                header( 'Location: ../../page-login.php' );

            } else {

                $_SESSION['temp'] = $temp;

                header( 'Location: ../../change-paswd.php' );
            }

        } else {

            $_SESSION['link-expiration'] = 'expired';

            header( 'Location: ../../page-login.php' );

        }

    } catch ( Exception $ex ) {
        $_SESSION['check'] = "Catch error: $ex->getMessage()";
        header( 'Location: ../../page-login.php' );
        // echo 'Catch Error : '.$ex->getMessage();
    }

} else
header( 'Location: ../../page-login.php' );

$stmt = null;
$db = null;