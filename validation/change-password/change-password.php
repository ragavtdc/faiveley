<?php
ini_set( 'display_errors', 1 );
ini_set( 'display_startup_errors', 1 );
error_reporting( E_ALL );

require( '../db/connect.php' );

$db = db();

session_start();

if ( isset( $_REQUEST['changePassword'] ) && isset( $_REQUEST['changeConfrmPassword'] ) && isset( $_REQUEST['temp'] ) ) {

    extract( $_REQUEST );

    $sql = "SELECT usermail from [rstahl].[dbo].[password_link_expiration] WHERE tempVal = '$temp'";

    $stmt = $db->prepare( $sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
    $stmt->execute();

    while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ) {
        $user = $row;
    }

    $usermail = $user['usermail'];

    $changePassword = md5($changePassword);

    $changeConfrmPassword = md5($changeConfrmPassword);

    if ( $changePassword == $changeConfrmPassword ) {

        $sql = "UPDATE [rstahl].[dbo].[login] SET password = '$changeConfrmPassword' WHERE user_mail = '$usermail'";

        $stmt = $db->prepare( $sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
        $stmt->execute();

        $rowCount = $stmt->rowCount();

        if ( $rowCount == 1 ) {
            $_SESSION['changePaswd'] = 'changed';
            header( 'Location: ../../page-login.php' );
        } else {
            echo $_SESSION['changePaswd'] = 'notChanged';
            header( 'Location: ../../page-login.php' );
        }
    } else {
        echo $_SESSION['changePaswd'] = 'notChanged';
        header( 'Location: ../../page-login.php' );
    }
} else {
    header( 'Location: ../../page-login.php' );
}

$stmt = null;
$db = null;