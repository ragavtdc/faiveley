<?php

session_start();

if (isset($_GET['role'])) {
    if ($_GET['role'] == 'Guest') {
        $_SESSION['role'] = 'Guest';
        $_SESSION['username'] = 'Guest';
        $_SESSION['usermail'] = 'ragavm13@gmail.com'; //Company mail
        header("Location: ../dashboard.php");
    } else {
        $_SESSION['close'] = 'close';
        header("Location: ../page-login.php");
    }
} else {
    $_SESSION['error'] = 'error';
    header("Location: ../page-login.php?error=");
}
