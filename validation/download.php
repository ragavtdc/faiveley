<?php

$data = array();
$result = array();

$datetime = new DateTime("now", new DateTimeZone('Asia/Kolkata'));
$datetime = $datetime->format('Y-m-d H:i:s');
$reportTime = $datetime;

$reportTime1 = array();

$reportTime1 = $reportTime;

$reportFromDate = date("Y-m-d");
$reportToDate = date("Y-m-d");

if (isset($_REQUEST['reportFromDate']) && !empty($_REQUEST['reportFromDate']) && isset($_REQUEST['reportToDate']) && !empty($_REQUEST['reportToDate'])) {
    $reportFromDate = $_REQUEST['reportFromDate'];
    $reportToDate = $_REQUEST['reportToDate'];
}

function download_send_headers($filename)
{
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

function array2csv(array &$array)
{
    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen("php://output", 'w');
    fputcsv($df, array_keys(reset($array)));
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}

require('db/connect.php');

$db = db();
//convert(date, getdate())
$query = "SELECT temperature AS TEMPERATURE, [current] AS CURRENT_, humidity AS HUMIDITY, pressure AS PRESSURE, dust AS DUST, lux AS LUX, pt100 AS PT_100, ktype AS KTYPE, ktype1 AS KTYPE_1, pressure1 as PRESSURE_1, curr_time AS TIME FROM [rstahl].[dbo].[sensor_data] WHERE CONVERT(date,curr_time) >= '$reportFromDate' AND CONVERT(date,curr_time) <= '$reportToDate' ORDER BY sno ASC ";
$stmt = $db->query($query);

$stmt = $db->prepare($query);
$stmt->execute();

// $result['report_time'] = $reportTime;

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $result[] = $row;
}

if ($stmt->rowCount() > 0) {
    $data = $result;
}

if ($reportFromDate == $reportToDate) {
    $reportDate = "for " . $reportFromDate;
} else {
    $reportDate = "from_" . $reportFromDate . " to_" . $reportToDate;
}

download_send_headers("rstahl_data_export_report " . $reportDate .  ".csv");
echo array2csv($data);

$stmt = null;
$db = null;
die();