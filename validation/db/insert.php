<?php
error_reporting(E_ALL); ini_set('display_errors', '1');
if( !isset($_POST['temperature']) || !isset($_POST['current']) || !isset($_POST['humidity']) || !isset($_POST['pressure'])
|| !isset($_POST['dust']) || !isset($_POST['lux']) ){
die('missing params');
}

require("connect.php");

$db = db();

extract($_POST);

$sql = "INSERT INTO [rstahl].[dbo].[sensor_data] (temperature, [current], humidity, pressure, dust, lux) VALUES
(:temperature,:current,:humidity,:pressure,:dust,:lux)";

$stmt = $db->prepare( $sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1  ) );
$stmt->execute( array( ':temperature' => $temperature, ':current' => $current, ':humidity' => $humidity, ':pressure' => $pressure, ':dust' => $dust, ':lux' => $lux ) );

$stmt = null;
$db = null;

?>