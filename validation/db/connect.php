<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

function db()
{
    $config = parse_ini_file('db.ini');
    // $dbContent = file_get_contents( 'http://apps.bittechpro.in/db/rstahl_db.ini' );
    // $config = parse_ini_file( $dbContent, true );

    // $host = $config['server'];
    $host = 'adc.bitsathy.ac.in';
    $dbname = 'faiveley';
    $username = $config['username'];
    $password = $config['password'];

    try {
        //******PDO METHOD
        $db = mysqli_connect($host, $username, $password, $dbname);

        //*******NORMAL METOD
        // $connectionInfo = array( 'Database'=>$dbname, 'UID'=>$username, 'PWD'=>$password );

        // $db = sqlsrv_connect( $host, $connectionInfo );
        // echo 'Connected';
    } catch (Exception $ex) {
        echo $ex;
    }

    return $db;
}
