<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require('../db/connect.php');

$db = db();

session_start();

if (isset($_REQUEST['usermail']) && isset($_REQUEST['username']) &&  isset($_REQUEST['role'])) {

    extract($_REQUEST);

    $chMail = $_SESSION['chMail'];

    if (!filter_var($usermail, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['invalid'] = true;
        die(header("Location: ../../page-user-management.php"));
    }
    try {
        $db->beginTransaction();

        $sql = "UPDATE [rstahl].[dbo].[login] SET user_mail = '$usermail', user_name = '$username', user_type = '$usertype' WHERE user_mail = '$chMail'";

        $stmt = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1));
        $stmt->execute();

        $db->commit();

        $rowCount = $stmt->rowCount();

        if ($rowCount == 1) {
            $_SESSION['updatedUser'] = 'updated';
            header("Location: ../../page-user-management.php");
        } else {
            $_SESSION['updatedUser'] = 'notUpdated';
            header("Location: ../../page-user-management.php");
        }
    } catch (Exception $ex) {
        $_SESSION['updatedUser'] = 'notUpdated';
        header("Location: ../../page-user-management.php");
    }
} else {
    header("Location: ../../page-login.php");
}

$_SESSION['chMail'] = '';

$stmt = null;
$db = null;
