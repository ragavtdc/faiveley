<?php
session_start();

?><?php
// ini_set( 'display_errors', 1 );
// ini_set( 'display_startup_errors', 1 );
// error_reporting( E_ALL );

require( '../db/connect.php' );

$db = db();

// $result = array();
$checked = false;

if ( isset( $_REQUEST['usermail'] ) ) {

    extract( $_REQUEST );

    if ( !filter_var( $usermail, FILTER_VALIDATE_EMAIL ) ) {
        $_SESSION['invalid'] = true;
        die( header( 'Location: ../../page-user-management.php' ) );
    }

    try {

        $sqlCheck = "SELECT TOP(1)* FROM [rstahl].[dbo].[login] WHERE user_mail = '$usermail'";

        $stmt = $db->prepare( $sqlCheck, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
        $stmt->execute();

        while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ) {
            $result = $row;
        }

        if ( $stmt->rowCount() == 1 ) {
            $checked = true;
        }

        if ( $checked ) {

            $db->beginTransaction();

            $sql = "DELETE FROM [rstahl].[dbo].[login] WHERE user_mail = '$usermail'";

            $stmt = $db->prepare( $sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1 ) );
            $stmt->execute();

            $db->commit();

            $rowCount = $stmt->rowCount();

            if ( $rowCount == 1 ) {
                $_SESSION['removedUser'] = 'removed';
                header( 'Location: ../../page-user-management.php' );
            } else {
                $_SESSION['removedUser'] = 'notRemoved';
                header( 'Location: ../../page-user-management.php' );
            }
        } else {
            $_SESSION['removedUser'] = 'notExist';
            header( 'Location: ../../page-user-management.php' );
        }
    } catch ( Exception $ex ) {
        $_SESSION['removedUser'] = 'notRemoved';
        header( 'Location: ../../page-user-management.php' );
    }

} else
header( 'Location: ../../page-login.php' );

$stmt = null;
$db = null;