<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require('../db/connect.php');

$db = db();

session_start();

$data = array();

if (isset($_REQUEST['usermail'])) {    
    
    extract($_REQUEST);

    if (!filter_var($usermail, FILTER_VALIDATE_EMAIL)) {
        $data['invalid'] = true;
    }

    $sql = "SELECT * FROM [rstahl].[dbo].[login] WHERE user_mail = '$usermail'";

    $stmt = $db->prepare( $sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1  ) );
    $stmt->execute();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	    $result = $row;
    }

    if ($stmt->rowCount() == 1) {
        $data['status'] = 'ok';
	    $data['result'] = $result;
    } else {
	    $data['status'] = 'none';
	    $data['result'] = '';
    }
    
} else{
    header("Location: ../../page-login.php");
}

$stmt = null;
$db = null;

echo json_encode($data);