<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

require('../db/connect.php');

$db = db();

session_start();

if (isset($_REQUEST['usermail']) && isset($_REQUEST['username']) && isset($_REQUEST['role'])) {

    extract($_REQUEST);

    $password = md5(strtolower($usertype));
    $usermail = strtolower($usermail);

    if (!filter_var($usermail, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['invalid'] = true;
        die(header("Location: ../../page-user-management.php"));
    }

    try {
        $db->beginTransaction();

        $sql = "INSERT INTO [rstahl].[dbo].[login] (user_type, password, user_mail, user_name) VALUES (:usertype, :pswd, :usermail, :username)";

        $stmt = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1));
        $stmt->execute(array(':usertype' => $usertype, ':pswd' => $password, ':usermail' => $usermail, ':username' => $username));

        $db->commit();

        $rowCount = $stmt->rowCount();

        if ($rowCount == 1) {
            $_SESSION['addedUser'] = 'added';
            header("Location: ../../page-user-management.php");
        } else {
            $_SESSION['addedUser'] = 'notAdded';
            header("Location: ../../page-user-management.php");
        }
    } catch (Exception $ex) {
        $_SESSION['addedUser'] = 'notAdded';
        header("Location: ../../page-user-management.php");
    }
} else
    header("Location: ../../page-login.php");

$stmt = null;
$db = null;
