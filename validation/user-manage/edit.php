<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require('../db/connect.php');

$db = db();

session_start();

if (isset($_REQUEST['usermail']) && isset($_REQUEST['username']) &&  isset($_REQUEST['role'])) {

    extract($_REQUEST);

    $chMail = $_SESSION['chMail'];

    if (!filter_var($usermail, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['error'] = 'error';
        die(header("Location: ../../page-user-management.php"));
    }

    // $db->beginTransaction();

    $sql = "UPDATE [rstahl].[dbo].[login] SET user_mail = '$usermail' AND [user_name] = '$username' AND user_type = '$usertype' WHERE user_mail = '$chMail'";

    $stmt = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1));
    $stmt->execute();

    // $db->commit();

    $rowCount = $stmt->rowCount();

    if ($rowCount == 1) {
        $_SESSION['editedUser'] = 'edited';
        header("Location: ../../page-user-management.php");
    } else {
        echo $_SESSION['editedUser'] = 'notEdited';
        header("Location: ../../page-user-management.php");
    }
} else {
    header("Location: ../../page-login.php");
}

$_SESSION['chMail'] = '';

$stmt = null;
$db = null;
