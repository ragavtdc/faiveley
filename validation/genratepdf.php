<?php
require( 'db/connect.php' );
require( 'db/fpdf.php' );

$db = db();

$datetime = new DateTime( 'now', new DateTimeZone( 'Asia/Kolkata' ) );
$datetime = $datetime->format( 'Y-m-d H:i:s' );
$reportTime = $datetime;

$date = new DateTime( 'now', new DateTimeZone( 'Asia/Kolkata' ) );
$date = $date->format( 'Y-m-d' );

$reportFromDate = date( 'Y-m-d' );
$reportToDate = date( 'Y-m-d' );

if ( isset( $_REQUEST['reportFromDate'] ) && !empty( $_REQUEST['reportFromDate'] ) && isset( $_REQUEST['reportToDate'] ) && !empty( $_REQUEST['reportToDate'] ) ) {
    $reportFromDate = $_REQUEST['reportFromDate'];
    $reportToDate = $_REQUEST['reportToDate'];
}

if ( $reportFromDate == $reportToDate ) {
    $reportDate = 'REPORT FOR: ' . $reportFromDate;
} else {
    $reportDate = 'REPORT FROM: ' . $reportFromDate . ' TO: ' . $reportToDate;
}

$query = "SELECT temperature, [current], humidity, pressure, dust, lux, pt100, ktype, ktype1, pressure1, curr_time FROM [rstahl].[dbo].[sensor_data] WHERE CONVERT(date,curr_time) >= '$reportFromDate' AND CONVERT(date,curr_time) <= '$reportToDate' ORDER BY sno ASC ";

$stmt = $db->prepare( $query );
$stmt->execute();

while ( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ) {
    $result[] = $row;
}

$queryEx = 'SELECT *  FROM [rstahl].[dbo].[exceed_data]';

$stmtEx = $db->prepare( $queryEx );
$stmtEx->execute();

while ( $row = $stmtEx->fetch( PDO::FETCH_ASSOC ) ) {
    $resultEx[] = $row;
}

$queryLow = 'SELECT *  FROM [rstahl].[dbo].[lower_data]';

$stmtLow = $db->prepare( $queryLow );
$stmtLow->execute();

while ( $row = $stmtLow->fetch( PDO::FETCH_ASSOC ) ) {
    $resultLow[] = $row;
}

$cube = chr( 179 );
$tempSign = '(' . iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#8451;' ) ) . ')';
$currSign = '(milliamps)';
$humSign = '(%rh)';
$presSign = '(bar)';
$dustSign = '(' . iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#x00B5;' ) ) . 'g/m' . $cube . ')';
$luxSign = '(lux)';
$timeSign = '(yyyy-mm-dd h:m:s:i)';

class PDF extends FPDF
 {
    // Page header

    function Header()
 {
        if ( $this->PageNo() != 1 ) {
            $this->SetFont( 'Arial', 'B', 9 );
            // $this->Cell( 15, 12, 'SNO', 1, 0, 'C' );
            // $this->Cell( 34, 12, 'TEMPERATURE', 1, 0, 'C' );
            // $this->Cell( 31, 12, 'CURRENT', 1, 0, 'C' );
            // $this->Cell( 34, 12, 'HUMIDITY', 1, 0, 'C' );
            // $this->Cell( 36, 12, 'PRESSURE', 1, 0, 'C' );
            // $this->Cell( 34, 12, 'DUST', 1, 0, 'C' );
            // $this->Cell( 41, 12, 'LUX', 1, 0, 'C' );
            // $this->Cell( 34, 12, 'PT 100', 1, 0, 'C' );
            // $this->Cell( 34, 12, 'K TYPE', 1, 0, 'C' );
            // $this->Cell( 34, 12, 'K TYPE 1', 1, 0, 'C' );
            // $this->Cell( 36, 12, 'PRESSURE 1', 1, 0, 'C' );
            // $this->Cell( 37, 12, 'TIME', 1, 0, 'C' );
            // $this->Ln();
            $this->SetXY( 10, 10 );
            $this->MultiCell( 15, 12, '   SNO        ', 1, 'C' );
            $this->SetXY( 25, 10 );
            $this->MultiCell( 34, 12, 'TEMPERATURE   ('.iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#8451;' ) ).')', 1, 'C' );
            $this->SetXY( 59, 10 );
            $this->MultiCell( 31, 12, 'CURRENT (milliamps)', 1, 'C' );
            $this->SetXY( 90, 10 );
            $this->MultiCell( 34, 12, '         HUMIDITY           (%rh)', 1, 'C' );
            $this->SetXY( 124, 10 );
            $this->MultiCell( 36, 12, '         PRESSURE            (bar)', 1, 'C' );
            $this->SetXY( 160, 10 );
            $this->MultiCell( 34, 12, '            DUST             ('.iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#x00B5;' ) ) . 'g/m' . chr( 179 ).')', 1, 'C' );
            $this->SetXY( 194, 10 );
            $this->MultiCell( 41, 12, '               LUX                 (lux)', 1, 'C' );
            $this->SetXY( 235, 10 );
            $this->MultiCell( 34, 12, '            PT 100               ('.iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#8451;' ) ).')', 1, 'C' );
            $this->SetXY( 269, 10 );
            $this->MultiCell( 34, 12, '             K TYPE               ('.iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#8451;' ) ).')', 1, 'C' );
            $this->SetXY( 303, 10 );
            $this->MultiCell( 34, 12, '            K TYPE 1              ('.iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#8451;' ) ).')', 1, 'C' );
            $this->SetXY( 337, 10 );
            $this->MultiCell( 36, 12, '       PRESSURE 1          (bar)', 1, 'C' );
            $this->SetXY( 373, 10 );
            $this->MultiCell( 37, 12, '            TIME                    (yyyy-mm-dd h:m:s:i)', 1, 'C' );
            // $this->Cell( 15, 8, '', 1, 0, 'C' );
            // $this->Cell( 34, 8, '(' . iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#8451;' ) ) . ')', 1, 0, 'C' );
            // $this->Cell( 31, 8, '(milliamps)', 1, 0, 'C' );
            // $this->Cell( 34, 8, '(%rh)', 1, 0, 'C' );
            // $this->Cell( 36, 8, '(bar)', 1, 0, 'C' );
            // $this->Cell( 34, 8, '(' . iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#x00B5;' ) ) . 'g/m' . chr( 179 ) . ')', 1, 0, 'C' );
            // $this->Cell( 41, 8, '(lux)', 1, 0, 'C' );
            // $this->Cell( 34, 8, '(' . iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#8451;' ) ) . ')', 1, 0, 'C' );
            // $this->Cell( 34, 8, '(' . iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#8451;' ) ) . ')', 1, 0, 'C' );
            // $this->Cell( 34, 8, '(' . iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', html_entity_decode( '&#8451;' ) ) . ')', 1, 0, 'C' );
            // $this->Cell( 36, 8, '(bar)', 1, 0, 'C' );
            // $this->Cell( 37, 8, '', 1, 0, 'C' );
            // $this->Ln();

            $this->SetFont( 'Arial', 'B', 8 );

            $this->Cell( 15, 8, '', 1, 0, 'C' );

            $this->Cell( 11, 8, 'MIN', 1, 0, 'C' );
            //temp
            $this->Cell( 11, 8, 'MAX', 1, 0, 'C' );
            //temp
            $this->Cell( 12, 8, 'ACT', 1, 0, 'C' );
            //temp

            $this->Cell( 10, 8, 'MIN', 1, 0, 'C' );
            //curr
            $this->Cell( 10, 8, 'MAX', 1, 0, 'C' );
            //curr
            $this->Cell( 11, 8, 'ACT', 1, 0, 'C' );
            //curr

            $this->Cell( 11, 8, 'MIN', 1, 0, 'C' );
            //hum
            $this->Cell( 11, 8, 'MAX', 1, 0, 'C' );
            //hum
            $this->Cell( 12, 8, 'ACT', 1, 0, 'C' );
            //hum

            $this->Cell( 11, 8, 'MIN', 1, 0, 'C' );
            //pres
            $this->Cell( 11, 8, 'MAX', 1, 0, 'C' );
            //pres
            $this->Cell( 14, 8, 'ACT', 1, 0, 'C' );
            //pres

            $this->Cell( 11, 8, 'MIN', 1, 0, 'C' );
            //dust
            $this->Cell( 11, 8, 'MAX', 1, 0, 'C' );
            //dust
            $this->Cell( 12, 8, 'ACT', 1, 0, 'C' );
            //dust

            $this->Cell( 10, 8, 'MIN', 1, 0, 'C' );
            //lux
            $this->Cell( 15, 8, 'MAX', 1, 0, 'C' );
            //lux
            $this->Cell( 16, 8, 'ACT', 1, 0, 'C' );
            //lux

            $this->Cell( 11, 8, 'MIN', 1, 0, 'C' );
            //pt
            $this->Cell( 11, 8, 'MAX', 1, 0, 'C' );
            //pt
            $this->Cell( 12, 8, 'ACT', 1, 0, 'C' );
            //pt

            $this->Cell( 11, 8, 'MIN', 1, 0, 'C' );
            //ktype
            $this->Cell( 11, 8, 'MAX', 1, 0, 'C' );
            //ktype
            $this->Cell( 12, 8, 'ACT', 1, 0, 'C' );
            //ktype

            $this->Cell( 11, 8, 'MIN', 1, 0, 'C' );
            //ktype1
            $this->Cell( 11, 8, 'MAX', 1, 0, 'C' );
            //ktype1
            $this->Cell( 12, 8, 'ACT', 1, 0, 'C' );
            //ktype1

            $this->Cell( 11, 8, 'MIN', 1, 0, 'C' );
            //pres1
            $this->Cell( 11, 8, 'MAX', 1, 0, 'C' );
            //pres1
            $this->Cell( 14, 8, 'ACT', 1, 0, 'C' );
            //pres1
            $this->Cell( 37, 8, '', 1, 0, 'C' );
            $this->Ln();
        }
    }
    // Page footer

    function Footer()
 {
        // Position at 1.5 cm from bottom
        $this->SetY( -15 );
        $this->SetFont( 'Arial', 'I', 8 );
        $this->Cell( 0, 10, 'Page ' . $this->PageNo(), 0, 0, 'C' );
    }
}

$pdf = new PDF();
$pdf->header = 0;
$pdf->AddPage( 'L', 'a3' );

$pdf->SetFont( 'Arial', 'B', 18 );
$pdf->Cell( 30, 12, '', 0 );
$pdf->Cell( 100, 12, 'R. STAHL GROUP', 0, '', '', false, 'https://www.rstahl.com/' );
$pdf->Cell( 30, 12, $pdf->Image( '../images/rstahl_logo.png', 342, 10, 35, 20 ), 0, 0, 'C' );
$pdf->Ln( 20 );

if ( $stmt->rowCount() > 0 ) {

    $pdf->SetFont( 'Arial', '', 10 );
    $pdf->Cell( 30, 12, '', 0 );
    $pdf->Cell( 290, 12, $reportDate, 0 );
    $pdf->Cell( 30, 12, 'REPORT TIME: ' . $reportTime, 0 );
    $pdf->Ln();

    $pdf->SetFont( 'Arial', 'B', 9 );
    // $pdf->Cell( 15, 12, 'SNO', 1, 0, 'C' );
    // $pdf->Cell( 34, 12, 'TEMPERATURE ', 1, 0, 'C' );
    // $pdf->Cell( 31, 12, 'CURRENT ', 1, 0, 'C' );
    // $pdf->Cell( 34, 12, 'HUMIDITY ', 1, 0, 'C' );
    // $pdf->Cell( 36, 12, 'PRESSURE ', 1, 0, 'C' );
    // $pdf->Cell( 34, 12, 'DUST ', 1, 0, 'C' );
    // $pdf->Cell( 41, 12, 'LUX ', 1, 0, 'C' );
    // $pdf->Cell( 34, 12, 'PT 100 ', 1, 0, 'C' );
    // $pdf->Cell( 34, 12, 'K TYPE ', 1, 0, 'C' );
    // $pdf->Cell( 34, 12, 'K TYPE 1 ', 1, 0, 'C' );
    // $pdf->Cell( 36, 12, 'PRESSURE 1 ', 1, 0, 'C' );
    // $pdf->Cell( 37, 12, 'TIME', 1, 0, 'C' );
    $pdf->SetXY( 10, 50 );
    $pdf->MultiCell( 15, 12, '   SNO        ', 1, 'C' );
    $pdf->SetXY( 25, 50 );
    $pdf->MultiCell( 34, 12, 'TEMPERATURE   '.$tempSign, 1, 'C' );
    $pdf->SetXY( 59, 50 );
    $pdf->MultiCell( 31, 12, 'CURRENT '.$currSign, 1, 'C' );
    $pdf->SetXY( 90, 50 );
    $pdf->MultiCell( 34, 12, '         HUMIDITY           '.$humSign, 1, 'C' );
    $pdf->SetXY( 124, 50 );
    $pdf->MultiCell( 36, 12, '         PRESSURE            '.$presSign, 1, 'C' );
    $pdf->SetXY( 160, 50 );
    $pdf->MultiCell( 34, 12, '            DUST             '.$dustSign, 1, 'C' );
    $pdf->SetXY( 194, 50 );
    $pdf->MultiCell( 41, 12, '               LUX                 '.$luxSign, 1, 'C' );
    $pdf->SetXY( 235, 50 );
    $pdf->MultiCell( 34, 12, '            PT 100               '.$tempSign, 1, 'C' );
    $pdf->SetXY( 269, 50 );
    $pdf->MultiCell( 34, 12, '             K TYPE               '.$tempSign, 1, 'C' );
    $pdf->SetXY( 303, 50 );
    $pdf->MultiCell( 34, 12, '            K TYPE 1              '.$tempSign, 1, 'C' );
    $pdf->SetXY( 337, 50 );
    $pdf->MultiCell( 36, 12, '       PRESSURE 1          '.$presSign, 1, 'C' );
    $pdf->SetXY( 373, 50 );
    $pdf->MultiCell( 37, 12, '            TIME                    '.$timeSign, 1, 'C' );
    // $pdf->Ln();
    // $pdf->Cell( 15, 8, '', 1, 0, 'C' );
    // $pdf->Cell( 34, 8, $tempSign, 1, 0, 'C' );
    // $pdf->Cell( 31, 8, $currSign, 1, 0, 'C' );
    // $pdf->Cell( 34, 8, $humSign, 1, 0, 'C' );
    // $pdf->Cell( 36, 8, $presSign, 1, 0, 'C' );
    // $pdf->Cell( 34, 8, $dustSign, 1, 0, 'C' );
    // $pdf->Cell( 41, 8, $luxSign, 1, 0, 'C' );
    // $pdf->Cell( 34, 8, $tempSign, 1, 0, 'C' );
    // $pdf->Cell( 34, 8, $tempSign, 1, 0, 'C' );
    // $pdf->Cell( 34, 8, $tempSign, 1, 0, 'C' );
    // $pdf->Cell( 36, 8, $presSign, 1, 0, 'C' );
    // $pdf->Cell( 37, 8, '', 1, 0, 'C' );
    // $pdf->Ln();

    $pdf->SetFont( 'Arial', 'B', 8 );

    $pdf->Cell( 15, 8, '', 1, 0, 'C' );

    $pdf->Cell( 11, 8, 'MIN', 1, 0, 'C' );
    //temp
    $pdf->Cell( 11, 8, 'MAX', 1, 0, 'C' );
    //temp
    $pdf->Cell( 12, 8, 'ACT', 1, 0, 'C' );
    //temp

    $pdf->Cell( 10, 8, 'MIN', 1, 0, 'C' );
    //curr
    $pdf->Cell( 10, 8, 'MAX', 1, 0, 'C' );
    //curr
    $pdf->Cell( 11, 8, 'ACT', 1, 0, 'C' );
    //curr

    $pdf->Cell( 11, 8, 'MIN', 1, 0, 'C' );
    //hum
    $pdf->Cell( 11, 8, 'MAX', 1, 0, 'C' );
    //hum
    $pdf->Cell( 12, 8, 'ACT', 1, 0, 'C' );
    //hum

    $pdf->Cell( 11, 8, 'MIN', 1, 0, 'C' );
    //pres
    $pdf->Cell( 11, 8, 'MAX', 1, 0, 'C' );
    //pres
    $pdf->Cell( 14, 8, 'ACT', 1, 0, 'C' );
    //pres

    $pdf->Cell( 11, 8, 'MIN', 1, 0, 'C' );
    //dust
    $pdf->Cell( 11, 8, 'MAX', 1, 0, 'C' );
    //dust
    $pdf->Cell( 12, 8, 'ACT', 1, 0, 'C' );
    //dust

    $pdf->Cell( 10, 8, 'MIN', 1, 0, 'C' );
    //lux
    $pdf->Cell( 15, 8, 'MAX', 1, 0, 'C' );
    //lux
    $pdf->Cell( 16, 8, 'ACT', 1, 0, 'C' );
    //lux

    $pdf->Cell( 11, 8, 'MIN', 1, 0, 'C' );
    //pt
    $pdf->Cell( 11, 8, 'MAX', 1, 0, 'C' );
    //pt
    $pdf->Cell( 12, 8, 'ACT', 1, 0, 'C' );
    //pt

    $pdf->Cell( 11, 8, 'MIN', 1, 0, 'C' );
    //ktype
    $pdf->Cell( 11, 8, 'MAX', 1, 0, 'C' );
    //ktype
    $pdf->Cell( 12, 8, 'ACT', 1, 0, 'C' );
    //ktype

    $pdf->Cell( 11, 8, 'MIN', 1, 0, 'C' );
    //ktype1
    $pdf->Cell( 11, 8, 'MAX', 1, 0, 'C' );
    //ktype1
    $pdf->Cell( 12, 8, 'ACT', 1, 0, 'C' );
    //ktype1

    $pdf->Cell( 11, 8, 'MIN', 1, 0, 'C' );
    //pres1
    $pdf->Cell( 11, 8, 'MAX', 1, 0, 'C' );
    //pres1
    $pdf->Cell( 14, 8, 'ACT', 1, 0, 'C' );
    //pres1
    $pdf->Cell( 37, 8, '', 1, 0, 'C' );

    $sno = 1;

    foreach ( $result as $row ) {
        $pdf->SetFont( 'Arial', '', 8 );
        $pdf->Ln();
        $c = 1;
        $pdf->Cell( 15, 8, $sno++, 1, 0, 'C' );

        foreach ( $row as $column ) {

            $pdf->SetFont( 'Arial', '', 8 );

            if ( $c == 1 ) {
                $pdf->Cell( 11, 8, ' ' . $resultLow[0]['temp'], 1, 0, 'C' );
                $pdf->Cell( 11, 8, ' ' . $resultEx[0]['temp'], 1, 0, 'C' );
                if ( $column > $resultEx[0]['temp'] || $column < $resultLow[0]['temp'] && $column != 0 ) {
                    $pdf->SetFont( 'Arial', 'BIU', 8 );
                }
                $pdf->Cell( 12, 8, ' ' . $column, 1, 0, 'C' );
            } else if ( $c == 2 ) {
                $pdf->Cell( 10, 8, ' ' . $resultLow[0]['curr'], 1, 0, 'C' );
                $pdf->Cell( 10, 8, ' ' . $resultEx[0]['curr'], 1, 0, 'C' );
                if ( $column > $resultEx[0]['curr'] || $column < $resultLow[0]['curr'] && $column != 0 ) {
                    $pdf->SetFont( 'Arial', 'BIU', 8 );
                }
                $pdf->Cell( 11, 8, ' ' . $column, 1, 0, 'C' );
            } else if ( $c == 3 ) {
                $pdf->Cell( 11, 8, ' ' . $resultLow[0]['hum'], 1, 0, 'C' );
                $pdf->Cell( 11, 8, ' ' . $resultEx[0]['hum'], 1, 0, 'C' );
                if ( $column > $resultEx[0]['hum'] || $column < $resultLow[0]['hum'] && $column != 0 ) {
                    $pdf->SetFont( 'Arial', 'BIU', 8 );
                }
                $pdf->Cell( 12, 8, ' ' . $column, 1, 0, 'C' );
            } else if ( $c == 4 ) {
                $pdf->Cell( 11, 8, ' ' . $resultLow[0]['pres'], 1, 0, 'C' );
                $pdf->Cell( 11, 8, ' ' . $resultEx[0]['pres'], 1, 0, 'C' );
                if ( $column > $resultEx[0]['pres'] || $column < $resultLow[0]['pres'] && $column != 0 ) {
                    $pdf->SetFont( 'Arial', 'BIU', 8 );
                }
                $pdf->Cell( 14, 8, ' ' . $column, 1, 0, 'C' );
            } else if ( $c == 5 ) {
                $pdf->Cell( 11, 8, ' ' . $resultLow[0]['dust'], 1, 0, 'C' );
                $pdf->Cell( 11, 8, ' ' . $resultEx[0]['dust'], 1, 0, 'C' );
                if ( $column > $resultEx[0]['dust'] || $column < $resultLow[0]['dust'] && $column != 0 ) {
                    $pdf->SetFont( 'Arial', 'BIU', 8 );
                }
                $pdf->Cell( 12, 8, ' ' . $column, 1, 0, 'C' );
            } else if ( $c == 6 ) {
                $pdf->Cell( 10, 8, ' ' . $resultLow[0]['lux'], 1, 0, 'C' );
                $pdf->Cell( 15, 8, ' ' . $resultEx[0]['lux'], 1, 0, 'C' );
                if ( $column > $resultEx[0]['lux'] || $column < $resultLow[0]['lux'] && $column != 0 ) {
                    $pdf->SetFont( 'Arial', 'BIU', 8 );
                }
                $pdf->Cell( 16, 8, ' ' . $column, 1, 0, 'C' );
            } else if ( $c == 7 ) {
                $pdf->Cell( 11, 8, ' ' . $resultLow[0]['pt100'], 1, 0, 'C' );
                $pdf->Cell( 11, 8, ' ' . $resultEx[0]['pt100'], 1, 0, 'C' );
                if ( $column > $resultEx[0]['pt100'] || $column < $resultLow[0]['pt100'] && $column != 0 ) {
                    $pdf->SetFont( 'Arial', 'BIU', 8 );
                }
                $pdf->Cell( 12, 8, ' ' . $column, 1, 0, 'C' );
            } else if ( $c == 8 ) {
                $pdf->Cell( 11, 8, ' ' . $resultLow[0]['ktype'], 1, 0, 'C' );
                $pdf->Cell( 11, 8, ' ' . $resultEx[0]['ktype'], 1, 0, 'C' );
                if ( $column > $resultEx[0]['ktype'] || $column < $resultLow[0]['ktype'] && $column != 0 ) {
                    $pdf->SetFont( 'Arial', 'BIU', 8 );
                }
                $pdf->Cell( 12, 8, ' ' . $column, 1, 0, 'C' );
            } else if ( $c == 9 ) {
                $pdf->Cell( 11, 8, ' ' . $resultLow[0]['ktype1'], 1, 0, 'C' );
                $pdf->Cell( 11, 8, ' ' . $resultEx[0]['ktype1'], 1, 0, 'C' );
                if ( $column > $resultEx[0]['ktype1'] || $column < $resultLow[0]['ktype1'] && $column != 0 ) {
                    $pdf->SetFont( 'Arial', 'BIU', 8 );
                }
                $pdf->Cell( 12, 8, ' ' . $column, 1, 0, 'C' );
            } else if ( $c == 10 ) {
                $pdf->Cell( 11, 8, ' ' . $resultLow[0]['pres1'], 1, 0, 'C' );
                $pdf->Cell( 11, 8, ' ' . $resultEx[0]['pressure1'], 1, 0, 'C' );
                if ( $column > $resultEx[0]['pressure1'] || $column < $resultLow[0]['pres1'] && $column != 0 ) {
                    $pdf->SetFont( 'Arial', 'BIU', 8 );
                }
                $pdf->Cell( 14, 8, ' ' . $column, 1, 0, 'C' );
            } else if ( $column == trim( $column ) && strpos( $column, ' ' ) !== false && $c == 11 ) {
                $pdf->Cell( 37, 8, ' ' . $column, 1, 0, 'C' );
            }
            $c++;
        }
    }
} else {
    $pdf->SetFont( 'Arial', '', 10 );
    $pdf->Cell( 30, 12, 'NO DATA TO ' . $reportDate, 0 );
    $pdf->Ln();
}
$pdf->Output( '', 'rstahl_data_export_' . strtolower( $reportDate ) . '.pdf', true );
