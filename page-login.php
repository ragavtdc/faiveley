<?php

require 'routes/includes/functions.php';

session_start();
session_destroy();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='shortcut icon' href='images/favicon-wabtec.ico' type='image/x-icon'>
    <!-- Main CSS-->
    <link rel='stylesheet' type='text/css' href='<?php echo $path ?>css/main.css'>
    <!-- Font-icon css-->
    <link rel='stylesheet' type='text/css' href='<?php echo $path ?>js/font-awesome/4.7.0/css/font-awesome.min.css'>
    <title>Login</title>
    <script>
        function disableBackButton() {
            window.history.forward();
        }
        setTimeout('disableBackButton()', 0);
    </script>
</head>

<body onload='disableBackButton()'>
    <section class='material-half-bg'>
        <div class='cover' id='logo'>
            <h1></h1>
            <!-- <img src = '../blog/vali-admin/rstahl_logo.png' height = '15%' width = '8%'> -->
        </div>
    </section>
    <section class='login-content'>
        <div class='login-box'>


            <form class='login-form' method='post' action='<?php echo $path ?>validation/login.php'>
                <!-- <h3 class='login-head'><i class='fa fa-lg fa-fw fa-user'></i>SIGN IN</h3> -->
                <center>
                    <img style="width: 100%;" class="pb-2" src='<?php echo $path ?>images/faiveley.jpg'>
                </center>
                <div class='form-group'>
                    <label class='control-label'>USERNAME</label>
                    <input class='form-control' type='text' data-toggle='tooltip' title='Username' name='username' autocomplete='off' required autofocus>
                </div>
                <div class='form-group'>
                    <label class='control-label'>PASSWORD</label>
                    <input class='form-control' type='password' data-toggle='tooltip' title='Password' name='password' required>
                </div>
                <div class='form-group'>
                    <p class='semibold-text mb-2'><a href='#' data-toggle='flip'>Forgot Password ?</a></p>
                </div>
                <div class='form-group btn-container'>
                    <button class='btn btn-primary btn-block'><i class='fa fa-sign-in fa-lg fa-fw'></i>SIGN IN</button>
                </div>

            </form>

            <form class='forget-form' action='validation/forgot-password/check-email.php' method='post'>

                <h3 class='login-head'><i class='fa fa-lg fa-fw fa-lock'></i>Forgot Password ?</h3>
                <div class='form-group'>
                    <label class='control-label'>EMAIL</label>
                    <input class='form-control' type='email' name='usermail' data-toggle='tooltip' title='User mail' placeholder='Email' required autocomplete='off'>
                </div>
                <div class='form-group btn-container'>
                    <button class='btn btn-primary btn-block'><i class='fa fa-unlock fa-lg fa-fw'></i>RESET</button>
                </div>
                <div class='form-group mt-3'>
                    <p class='semibold-text mb-0'><a href='#' data-toggle='flip'><i class='fa fa-angle-left fa-fw'></i>
                            Back to Login</a></p>
                </div>
            </form>
        </div>
        <div style='height:30px;'></div>
        <div class='form-group btn-container'>
            <button class='btn btn-primary btn-guest-signin btn-block' onclick='guestSignin();'><i class='fa fa-user-circle fa-lg fa-fw'></i>SIGN
                IN AS GUEST</button>
        </div>
    </section>
    <?php foot();
    ?>
    <script src='js/plugins/bootstrap-notify.min.js'></script>
    <script type='text/javascript' src='js/plugins/sweetalert.min.js'></script>

    <?php if (isset($_SESSION['error']) && $_SESSION['error'] == 'error') {
    ?>
        <script type='text/javascript'>
            $.notify({
                title: ' Login error : ',
                message: 'Invalid username or password',
                icon: 'fa fa-exclamation-triangle'
            }, {
                type: 'danger'
            });
        </script>
    <?php }
    ?>

    <?php if (isset($_SESSION['close']) && $_SESSION['close'] == 'close') {
    ?>
        <script type='text/javascript'>
            $.notify({
                title: ' Logged out : ',
                message: 'Unauthorised entry',
                icon: 'fa fa-exclamation-triangle'
            }, {
                type: 'danger'
            });
        </script>
    <?php }
    ?>

    <?php
    if (isset($_SESSION['mail']) && $_SESSION['mail'] == 'sent') {
    ?>
        <script>
            swal('Mail sent',
                'Reset Password link has been sent to your e-mail.\nVerify your e-mail and reset your password.\nLink will expire in 1 hour.',
                'success');
        </script>
    <?php }
    ?>

    <?php if (isset($_SESSION['changePaswd']) && $_SESSION['changePaswd'] == 'notChanged') {
    ?>
        <script type='text/javascript'>
            swal('Error!',
                'Password does not changed,\nPlease try again.', 'error');
        </script>
    <?php }
    ?>

    <?php if (isset($_SESSION['changePaswd']) && $_SESSION['changePaswd'] == 'changed') {
    ?>
        <script type='text/javascript'>
            swal('Updated!', 'Password changed successfully\nLogin with new credentials.', 'success');
        </script>
    <?php }
    ?>

    <?php
    if (isset($_SESSION['check']) && $_SESSION['check'] == 'incorrect') {
    ?>
        <script>
            swal('Incorrect', 'E-mail address does not exist.\nKindly provide a valid e-mail address.', 'error');
        </script>
    <?php }
    ?>

    <?php
    if (isset($_SESSION['link-expiration']) && $_SESSION['link-expiration'] == 'expired') {
    ?>
        <script>
            swal('Link expired', 'Please try again.\nThe password reset link e-mail was expired.', 'error');
        </script>
    <?php }
    ?>

    <script type='text/javascript'>
        // Login Page Flipbox control
        $('.login-content [data-toggle="flip"]').click(function() {
            $('.login-box').toggleClass('flipped');
            return false;
        });

        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });

        function guestSignin() {
            $(document).ready(function() {
                window.location = 'validation/guestSign.php?usertype=Guest';
            })
        }
    </script>

</body>

</html>
<?php
$_SESSION['error'] = '';
$_SESSION['close'] = '';
$_SESSION['changePaswd'] = '';
$_SESSION['mail'] = '';
$_SESSION['check'] = '';
$_SESSION['link-expiration'] = '';
?>